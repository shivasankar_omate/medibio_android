﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Enumeration;
using Xamarin.Forms;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration.Pnp;
using System.Diagnostics;
using Medibio.WinPhone.BluetoothLE;
using Medibio.BluetoothLE.Interfaces;
using Medibio.BluetoothLE;
using Medibio.BluetoothLE.Model;
using Medibio.Modules.Model;

[assembly: Xamarin.Forms.Dependency(typeof(BluetoothAdapterImplementation))]
namespace Medibio.WinPhone.BluetoothLE
{
    class BluetoothAdapterImplementation : IBluetoothAdapter
    {
        public string Name
        {
            get;
            private set;
        }
        
        public string Address
        {
            get;
            private set;
        }

        public IBluetoothDevice ConnectedDevice
        {
            get;
            set;
        }

        public List<IBluetoothDevice> DiscoveredDeviceList
        {
            get;
            set;
        }

        public bool IsConnected
        {
            get;
            set;
        }

        private DeviceState _DeviceState;
        public DeviceState DeviceCurrentState
        {
            private set {
                if (ConnectedDevice != null &&  _DeviceState != value) {
                    _DeviceState = value;
                    OnDeviceStateChanged?.Invoke(this, EventArgs.Empty);
                } 
            }
            get {
                return _DeviceState;
            }
        }

        public bool IsDiscoverable
        {
            get { return true; }
        }

        public bool IsEnabled
        {
            get { return true; }
        }

        public bool IsScanning
        {
            get;
            set;
        }


        public BluetoothType TargetType
        {
            get;
            set;
        }

        public event EventHandler<BluetoothDeviceFoundEventArgs> OnDeviceFound;
        public event EventHandler<BLEDataReceivedEventArgs> OnDataReceived;
        public event EventHandler<BLEDeviceDisconnectedEventArgs> OnDeviceDisconnected;
        public event EventHandler<BLEDeviceConnectedEventArgs> OnDeviceConnected;
        public event EventHandler<EventArgs> OnDeviceStateChanged;

        public BluetoothAdapterImplementation()
        {
            this.DiscoveredDeviceList = new List<IBluetoothDevice> ();
        }

        private List<DeviceInformation> _DiscoveredDeviceInfo = new List<DeviceInformation>();

        public async Task StartScanAsync(int timeoutInMilliSeconds)
        {
            IsScanning = true;

            this._DiscoveredDeviceInfo.Clear();

            //Set Device info service
            if (!DeviceInfoService.Instance.HasDeviceInfo)
            {
                var results = await DeviceInformation.FindAllAsync(
                GattDeviceService.GetDeviceSelectorFromUuid(GattServiceUuids.DeviceInformation),
                new string[] { "System.Devices.ContainerId" });
                if(results.Count > 0 && await BluetoothDeviceImplementation.IsConnected(results[0]))
                {
                    await DeviceInfoService.Instance.InitializeDeviceInfoAsync(results[0]);
                }
            }

            //Set Battery service
            if (!BatteryLevelService.Instance.HasBatteryData)
            {
                var results = await DeviceInformation.FindAllAsync(
                GattDeviceService.GetDeviceSelectorFromUuid(GattServiceUuids.Battery),
                new string[] { "System.Devices.ContainerId" });
                if (results.Count > 0 && await BluetoothDeviceImplementation.IsConnected(results[0]))
                {
                    await BatteryLevelService.Instance.InitializeBatteryLevelInfoAsync(results[0]);
                }
            }

            // Custom Service support
            if (!ZephyrCustomServcie.Instance.IsServiceInitialized)
            {
                Guid customService;
                Guid.TryParse(CustomBleServices.kHxM2CustomServiceUuidString, out customService);
                if (null != customService)
                {
                    var results = await DeviceInformation.FindAllAsync(
                    GattDeviceService.GetDeviceSelectorFromUuid(customService),
                    new string[] { "System.Devices.ContainerId" });
                    
                    
                    if (results.Count > 0 && await BluetoothDeviceImplementation.IsConnected(results[0]))
                    {
                        Debug.WriteLine("Devcie Is enable" + results[0].IsEnabled);
                        BluetoothLEDevice device = await BluetoothLEDevice.FromIdAsync(results[0].Id);
                        Debug.WriteLine("Device Connection status" + device.ConnectionStatus.ToString());
                        bool IsCombinedCharacteristicSupported = await ZephyrCustomServcie.Instance.HasCombinedDataCharacteristic(results[0]);
                        if (IsCombinedCharacteristicSupported)
                        {
                            _DiscoveredDeviceInfo.Add(results[0]);
                            DevcieFoundWithHrSerice(results[0], customService);
                        }
                        else
                        {
                            ZephyrCustomServcie.Instance.InitializeCustomServiceAsync(results[0]);
                        }                    }
                }
            }

            if (!HeartRateService.Instance.IsServiceInitialized && _DiscoveredDeviceInfo.Count == 0)
            {
                var devices = await DeviceInformation.FindAllAsync(
               GattDeviceService.GetDeviceSelectorFromUuid(GattServiceUuids.HeartRate),
               new string[] { "System.Devices.ContainerId" });
                if (devices.Count > 0 && await BluetoothDeviceImplementation.IsConnected(devices[0]))
                {
                    _DiscoveredDeviceInfo.Add(devices[0]);
                    DevcieFoundWithHrSerice(devices[0], GattServiceUuids.HeartRate);
                }
            }

            //TODO Error message will shown when no HR service found.
            if (_DiscoveredDeviceInfo.Count == 0)
            {
                //Should throw error message
                Debug.WriteLine("Could not find any Heart Rate devices. Please make sure your device is paired " +
                     "and powered on!");
            }
            //IsScanning = false;
        }

        private async Task DevcieFoundWithHrSerice(DeviceInformation dInfo, Guid serviceType)
        {
            DeviceInformation deviceInfo = dInfo;
            if (deviceInfo != null)
            {
                BluetoothLEDevice device = await BluetoothLEDevice.FromIdAsync(deviceInfo.Id);
                BluetoothDeviceImplementation LeDevcie = new BluetoothDeviceImplementation(device, deviceInfo);
                LeDevcie.GattService = serviceType;
                DiscoveredDeviceList.Add(LeDevcie);

                Name = device.Name;
                Address = device.BluetoothAddress.ToString();
                device.ConnectionStatusChanged += Device_ConnectionStatusChanged;

                OnDeviceFound?.Invoke(this, new BluetoothDeviceFoundEventArgs(LeDevcie));
            }
        }

        private void Device_ConnectionStatusChanged(BluetoothLEDevice sender, object args)
        {
            if(sender.ConnectionStatus == BluetoothConnectionStatus.Connected)
            {
                Debug.WriteLine("Connected");
                this.IsConnected = true;
                DeviceCurrentState = DeviceState.Connected;
                
            }
            else
            {
                Debug.WriteLine("Disconnected");
                this.IsConnected = false;
                DeviceCurrentState = DeviceState.Disconnected;
            }

        }

        public async Task StopScan() {
            //Nothing to return
            IsScanning = false;
        }

        public async Task ConnectDevice(IBluetoothDevice BleDevice)
        {
            BluetoothDeviceImplementation bleDeviceImplementation = ((BluetoothDeviceImplementation)BleDevice);
            if (GattServiceUuids.HeartRate.Equals (bleDeviceImplementation.GattService))
            {
                await HeartRateService.Instance.InitializeServiceAsync(bleDeviceImplementation.DeviceInfo);
            }
            else 
            {
                await ZephyrCustomServcie.Instance.InitializeCustomServiceAsync(bleDeviceImplementation.DeviceInfo);
            }
            //if(bleDeviceImplementation.GattService.ToString().ToUpper().Equals(CustomBleServices.kHxM2CustomServiceUuidString))
            try
            {
                // Check if the device is initially connected, and display the appropriate message to the user
                var deviceObject = await PnpObject.CreateFromIdAsync(PnpObjectType.DeviceContainer,
                    ((BluetoothDeviceImplementation)BleDevice).DeviceInfo.Properties["System.Devices.ContainerId"].ToString(),
                    new string[] { "System.Devices.Connected" });

                bool isConnected;
                if (Boolean.TryParse(deviceObject.Properties["System.Devices.Connected"].ToString(), out isConnected))
                {
                    ConnectedDevice = BleDevice;
                    IsConnected = true;
                    if (GattServiceUuids.HeartRate.Equals(bleDeviceImplementation.GattService))
                    {
                        HeartRateService.Instance.ValueChangeCompleted += Instance_ValueChangeCompleted;
                    }
                    else
                    {
                        ZephyrCustomServcie.Instance.ValueChangeCompleted += Instance_ValueChangeCompleted;
                    }
                    OnDeviceConnected?.Invoke(this, new BLEDeviceConnectedEventArgs(BleDevice));
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Retrieving device properties failed with message: " + e.Message);
            }
        }

        /*
        private void OnDeviceConnectionUpdated(bool isConnected)
        {
            if (isConnected)
            {
                HeartRateService.Instance.ValueChangeCompleted += Instance_ValueChangeCompleted;
                this.IsConnected = true;
                DeviceCurrentState = DeviceState.Connected;
            }
            else 
            {
                this.IsConnected = false;
                DeviceCurrentState = DeviceState.Disconnected;
            }
        }*/

        void Instance_ValueChangeCompleted(HRData heartRateMeasurementValue)
        {
            OnDataReceived?.Invoke(this, new BLEDataReceivedEventArgs(heartRateMeasurementValue));
        }

        public async Task DisconnectDevice(IBluetoothDevice device) {

            //HeartRateService.Instance
            DiscoveredDeviceList.Clear();
            ConnectedDevice = null;
            
            HeartRateService.Instance.Dispose();
            ZephyrCustomServcie.Instance.Dispose();
            DeviceInfoService.Instance.Dispose();
            BatteryLevelService.Instance.Dispose();
            IsConnected = false;
            
       }
           
    }
}
