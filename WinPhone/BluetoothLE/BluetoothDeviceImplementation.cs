﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.BluetoothLE;
using Medibio.BluetoothLE.Interfaces;
using Windows.Devices.Bluetooth;
using Windows.Devices.Enumeration;

namespace Medibio.WinPhone.BluetoothLE
{
    class BluetoothDeviceImplementation : IBluetoothDevice
    {
        private BluetoothLEDevice _device;

        public string Address
        {
            get { return _device.BluetoothAddress.ToString();  }
        }

        public Guid GattService
        {
            set;
            get;
        }

        public bool CreateSocket(Action action)
        {
            throw new NotImplementedException();
        }

        public string Name
        {
            get { return _device.Name; }
        }

        public event EventHandler<Medibio.BluetoothLE.Model.DeviceConnectedEventArgs> OnConnected;

        public event EventHandler<Medibio.BluetoothLE.Model.DeviceConnectedEventArgs> OnLost;

        public bool ReadData(Action action)
        {
            throw new NotImplementedException();
        }

        public DeviceState State
        {
            get {
                if (_device.ConnectionStatus == BluetoothConnectionStatus.Connected)
                    return DeviceState.Connected;
                else
                    return DeviceState.Disconnected;
            }
        }
        public DeviceInformation DeviceInfo{get; set;}

        public BluetoothDeviceImplementation(BluetoothLEDevice device, DeviceInformation deviceInfo) {
            _device = device;
            DeviceInfo = deviceInfo;
        }

        public static async Task<bool> IsConnected(DeviceInformation devInfo)
        {
            BluetoothLEDevice device = await BluetoothLEDevice.FromIdAsync(devInfo.Id);
            if(device.ConnectionStatus == BluetoothConnectionStatus.Connected)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
