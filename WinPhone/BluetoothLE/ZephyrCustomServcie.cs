﻿using Medibio.Modules.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;
using Windows.Devices.Enumeration.Pnp;
using Windows.Storage.Streams;

namespace Medibio.WinPhone.BluetoothLE
{
    public static class CustomBleServices
    {
        public static string kHxM2CustomServiceUuidString = "BEFDFF10-C979-11E1-9B21-0800200C9A66";
        public static string kHxM2ActivityCharacteristicUuidString = "BEFDFF11-C979-11E1-9B21-0800200C9A66";
        public static string kHxM2ECGCharacteristicUuidString = "BEFDFF13-C979-11E1-9B21-0800200C9A66";
        public static string kHxM2CombinedDataCharacteristicUuidString = "BEFDFF14-C979-11E1-9B21-0800200C9A66";
    }


    public class ZephyrCustomServcie : IDisposable
    {
        private const GattClientCharacteristicConfigurationDescriptorValue CHARACTERISTIC_NOTIFICATION_TYPE =
            GattClientCharacteristicConfigurationDescriptorValue.Notify;
        
        private static ZephyrCustomServcie instance;
        private GattDeviceService service;
        private IReadOnlyList<GattCharacteristic> characteristics;
        private PnpObjectWatcher watcher;
        private String deviceContainerId;

        public event ValueChangeCompletedHandler ValueChangeCompleted;
        public event DeviceConnectionUpdatedHandler DeviceConnectionUpdated;

        public static ZephyrCustomServcie Instance
        {
            //get { return instance; }
            get
            {
                return instance ?? (instance = new ZephyrCustomServcie());
            }
        }

        public bool IsServiceInitialized { get; set; }

        public GattDeviceService Service
        {
            get { return service; }
        }

        private ZephyrCustomServcie()
        {
        }

        public async Task InitializeCustomServiceAsync(DeviceInformation device)
        {
            try
            {
                deviceContainerId = "{" + device.Properties["System.Devices.ContainerId"] + "}";
                service = await GattDeviceService.FromIdAsync(device.Id);
                //GattDeviceService.
                if (service != null)
                {
                    IsServiceInitialized = true;
                    await ConfigureServiceForNotificationsAsync();
                }
                else
                {
                    // rootPage.NotifyUser("Access to the device is denied, because the application was not granted access, " "or the device is currently in use by another application.",NotifyType.StatusMessage);
                }
            }
            catch (Exception e)
            {

                Debug.WriteLine("ERROR: Accessing your device failed." + Environment.NewLine + e.Message);
            }
        }

        /// <summary>
        /// Configure the Bluetooth device to send notifications whenever the Characteristic value changes
        /// </summary>
        private async Task ConfigureServiceForNotificationsAsync()
        {
            try
            {
                // Obtain the characteristic for which notifications are to be received
                Guid customService;
                Guid.TryParse(CustomBleServices.kHxM2CustomServiceUuidString, out customService);
                if (null != customService)
                {
                    characteristics = service.GetAllCharacteristics();
                    if(characteristics.Count > 0)
                    {
                        foreach(var characteristic in characteristics)
                        {
                            Debug.WriteLine("Custom charav>>" + characteristic.Uuid.ToString());
                            // While encryption is not required by all devices, if encryption is supported by the device,
                            // it can be enabled by setting the ProtectionLevel property of the Characteristic object.
                            // All subsequent operations on the characteristic will work over an encrypted link.
                            //characteristic.ProtectionLevel = GattProtectionLevel.EncryptionRequired;

                            // Register the event handler for receiving notifications
                            characteristic.ValueChanged += Characteristic_ValueChanged;
                           
                            // In order to avoid unnecessary communication with the device, determine if the device is already 
                            // correctly configured to send notifications.
                            // By default ReadClientCharacteristicConfigurationDescriptorAsync will attempt to get the current
                            // value from the system cache and communication with the device is not typically required.
                            var currentDescriptorValue = await characteristic.ReadClientCharacteristicConfigurationDescriptorAsync();

                            if ((currentDescriptorValue.Status != GattCommunicationStatus.Success) ||
                                (currentDescriptorValue.ClientCharacteristicConfigurationDescriptor != CHARACTERISTIC_NOTIFICATION_TYPE))
                            {
                                // Set the Client Characteristic Configuration Descriptor to enable the device to send notifications
                                // when the Characteristic value changes
                                GattCommunicationStatus status =
                                    await characteristic.WriteClientCharacteristicConfigurationDescriptorAsync(
                                    CHARACTERISTIC_NOTIFICATION_TYPE);

                                if (status == GattCommunicationStatus.Unreachable)
                                {
                                    // Register a PnpObjectWatcher to detect when a connection to the device is established,
                                    // such that the application can retry device configuration.
                                    StartDeviceConnectionWatcher();
                                }
                            }
                        }
                    }
                }
                
                
            }
            catch (Exception e)
            {
                Debug.WriteLine("On Initialize Hr characteristics" + e.Message);
                //rootPage.NotifyUser("ERROR: Accessing your device failed." + Environment.NewLine + e.Message, NotifyType.ErrorMessage);
            }
        }

        public async Task<bool> HasCombinedDataCharacteristic(DeviceInformation devInfo)
        {
            try
            {
                // Obtain the characteristic for which notifications are to be received
                Guid CombinedDataCharacteristicUuid;
                Guid.TryParse(CustomBleServices.kHxM2CombinedDataCharacteristicUuidString, out CombinedDataCharacteristicUuid);
                if (null != CombinedDataCharacteristicUuid)
                {
                    var service = await GattDeviceService.FromIdAsync(devInfo.Id);
                    characteristics = service.GetCharacteristics(CombinedDataCharacteristicUuid);
                    if (characteristics.Count > 0)
                    {
                        if (characteristics[0].Uuid.ToString().Equals(CustomBleServices.kHxM2CombinedDataCharacteristicUuidString))
                        {
                            return true;
                        }
                    }
                    return false;
                }
                else
                {
                    return false;
                }

            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Register to be notified when a connection is established to the Bluetooth device
        /// </summary>
        private void StartDeviceConnectionWatcher()
        {
            watcher = PnpObject.CreateWatcher(PnpObjectType.DeviceContainer,
                new string[] { "System.Devices.Connected" }, String.Empty);

            watcher.Updated += DeviceConnection_Updated;
            watcher.Start();
        }

        /// <summary>
        /// Invoked when a connection is established to the Bluetooth device
        /// </summary>
        /// <param name="sender">The watcher object that sent the notification</param>
        /// <param name="args">The updated device object properties</param>
        private async void DeviceConnection_Updated(PnpObjectWatcher sender, PnpObjectUpdate args)
        {
            var connectedProperty = args.Properties["System.Devices.Connected"];
            bool isConnected = false;
            if ((deviceContainerId == args.Id) && Boolean.TryParse(connectedProperty.ToString(), out isConnected) &&
                isConnected)
            {
                foreach(var characteristic in characteristics) {
                    var status = await characteristic.WriteClientCharacteristicConfigurationDescriptorAsync(
                    CHARACTERISTIC_NOTIFICATION_TYPE);

                    if (status == GattCommunicationStatus.Success)
                    {
                        IsServiceInitialized = true;

                        // Once the Client Characteristic Configuration Descriptor is set, the watcher is no longer required
                        watcher?.Stop();
                        watcher = null;
                    }
                }

                // Notifying subscribers of connection state updates
                if (DeviceConnectionUpdated != null)
                {
                    DeviceConnectionUpdated(isConnected);
                }
            }
        }

        /// <summary>
        /// Invoked when Windows receives data from your Bluetooth device.
        /// </summary>
        /// <param name="sender">The GattCharacteristic object whose value is received.</param>
        /// <param name="args">The new characteristic value sent by the device.</param>
        private void Characteristic_ValueChanged(
            GattCharacteristic sender,
            GattValueChangedEventArgs args)
        {
            var data = new byte[args.CharacteristicValue.Length];

            DataReader.FromBuffer(args.CharacteristicValue).ReadBytes(data);

            // Process the raw data received from the device.
            HRData value = null;
            if (sender.Uuid.ToString().ToUpper() == CustomBleServices.kHxM2CombinedDataCharacteristicUuidString)
            {
                value = ProcessCombinedData(data);
            }
            else if (sender.Uuid.ToString().ToUpper() == CustomBleServices.kHxM2ActivityCharacteristicUuidString)
            {
                ProcessActivityData(data);
            }
            else if (sender.Uuid.ToString().ToUpper() == CustomBleServices.kHxM2ECGCharacteristicUuidString)
            {
                //TODO need to handle
            } 

            //value.Timestamp = args.Timestamp;

            /*
            lock (datapoints)
            {
                datapoints.Add(value);
            }
            */
            if (ValueChangeCompleted != null)
            {
                ValueChangeCompleted.Invoke(value);
            }
        }

        /// <summary>
        ///Process combined data from byte array
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static HRData ProcessCombinedData(byte[] bytes)
        {
            // The packet structure we use is based on version 1. Abort if it's any
            // other
            uint version = (uint)(bytes[0] & 0x0f);
            if (version != 0x01)
            {
                return null;
            }
            HRData hrData = new HRData();

            sbyte contactBit = (sbyte)(bytes[0] >> 7);

            if (contactBit == 1)
            {
                hrData.Contact = 3;
            }
            else
            {
                hrData.Contact = 0;
            }
            //BLEHelper.LogMessage(" Contact: " + BLEHelper.DataPacket.Contact);

            // Byte 4 is heart rate. But we will continue to read it from 0x180D.
            // This will ensure compatibility with other devices.
            sbyte hr = (sbyte)bytes[4];
            //BLEHelper.LogMessage(" hr: " + hr);
            hrData.Bpm = (int)hr;

            // Activity is byte 5 and 4 LSB bits from byte 6
            Int16 activityUnscaled = (Int16)(((bytes[6] & 0x0f) << 8) | bytes[5]);
            double activity = activityUnscaled * 0.01;
            hrData.Activity = activity;

            // Posture is 4 HSB bits in byte 6 and all of byte 7
            Int16 posture = (Int16)((bytes[6] >> 4) | (bytes[7] << 4));
            // get Posture in 1 degree units
            posture = (Int16)((posture << 20) >> 20);
            // Sign-extend from 12-bit to 32-bit
            hrData.Posture = posture;

            int count = bytes.Count();

            //ScanSession scanSession = ScanSession.Instance;

            // Decode up to 4 RR values. Values are stored 10-bit packed in 4ms resolution, e.g. 250 = 1000ms
            if (count > 9)
            {
                int val = (int)(((bytes[8] >> 0) | ((bytes[9] & 0x3) << 8)) * 4);
                //BLEHelper.LogMessage(" ibi: " + val);
                hrData.RrIntervals.Add(val);
                //scanSession.newRRInterval(val);
            }
            if (count > 10)
            {
                int val = (int)(((bytes[9] >> 2) | ((bytes[10] & 0xf) << 6)) * 4);
                //BLEHelper.LogMessage(" ibi: " + val);
                hrData.RrIntervals.Add(val);
                //scanSession.newRRInterval(val);
            }
            if (count > 11)
            {
                int val = (int)(((bytes[10] >> 4) | ((bytes[11] & 0x3f) << 4)) * 4);
                //BLEHelper.LogMessage(" ibi: " + val);
                hrData.RrIntervals.Add(val);
                //scanSession.newRRInterval(val);
            }
            if (count > 12)
            {
                int val = (int)(((bytes[11] >> 6) | (bytes[12] << 2)) * 4);
                //BLEHelper.LogMessage(" ibi: " + val);
                hrData.RrIntervals.Add(val);
                //scanSession.newRRInterval(val);
            }

            // Save data into monitor
            //LogMessage (" return");
            return hrData;

        }
        /// <summary>
        /// Read Activity charactristic
        /// </summary>
        /// <param name="data"></param>
        public static void ProcessActivityData(byte[] data)
        {

            ushort activityUnscaled = (ushort)((data[2] << 8) + data[1]);
            ushort peakActivity = (ushort)((data[4] << 8) + data[3]);

            MonitorDevice.Instance.activity = activityUnscaled * 0.01;
            MonitorDevice.Instance.peakActivity = peakActivity * 0.01;

            return;

        }

        /// <summary>
        /// Process the raw data read from the device into an application usable string, according to the Bluetooth
        /// Specification.
        /// </summary>
        /// <param name="bodySensorLocationData">Raw data read from the heart rate monitor.</param>
        /// <returns>The textual representation of the Body Sensor Location.</returns>
        public string ProcessBodySensorLocationData(byte[] bodySensorLocationData)
        {
            // The Bluetooth Heart Rate Profile specifies that the Body Sensor Location characteristic value has
            // a single byte of data
            byte bodySensorLocationValue = bodySensorLocationData[0];
            string retval;

            retval = "";
            switch (bodySensorLocationValue)
            {
                case 0:
                    retval += "Other";
                    break;
                case 1:
                    retval += "Chest";
                    break;
                case 2:
                    retval += "Wrist";
                    break;
                case 3:
                    retval += "Finger";
                    break;
                case 4:
                    retval += "Hand";
                    break;
                case 5:
                    retval += "Ear Lobe";
                    break;
                case 6:
                    retval += "Foot";
                    break;
                default:
                    retval = "";
                    break;
            }
            return retval;
        }

        public void Dispose()
        {
            service?.Dispose();
            if(characteristics != null)
            {
                foreach (var characteristic in characteristics)
                {
                    characteristic.ValueChanged -= Characteristic_ValueChanged;
                }
            }
           
            characteristics = null;
            watcher?.Stop();
            watcher = null;
            deviceContainerId = null;
            ZephyrCustomServcie.instance = null;

        }
    }
}
