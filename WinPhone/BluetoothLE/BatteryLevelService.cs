﻿using System.Threading.Tasks;
using System;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;
using Medibio.Modules.Model;
using Windows.Storage.Streams;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;


namespace Medibio.WinPhone.BluetoothLE
{
    public class BatteryLevelService
    {
        GattDeviceService service;
        GattCharacteristic batteryCharacteristic;

        bool hasBatteryData;
        public bool HasBatteryData
        {
            get
            {
                return hasBatteryData;
            }
            set
            {
                hasBatteryData = value;
            }
        }

        static BatteryLevelService instance;
        public static BatteryLevelService Instance
        {
            get
            {
                return instance ?? (instance = new BatteryLevelService());
            }
        }

        BatteryLevelService()
        {

        }

        /// <summary>
        /// Get battery service.  Read battrey level and set notification for battery level change.
        /// </summary>
        /// <param name="deviceInfo"></param>
        /// <returns></returns>
        public async Task InitializeBatteryLevelInfoAsync(DeviceInformation deviceInfo)
        {
            try
            {
                service = await GattDeviceService.FromIdAsync(deviceInfo.Id);
                if(service != null)
                {
                    await GetBatteryLevel();
                }
            }
            catch 
            {

            }
        } 

        async Task GetBatteryLevel()
        {
            batteryCharacteristic = service.GetCharacteristics(GattCharacteristicUuids.BatteryLevel)[0];
            var result = await batteryCharacteristic.ReadValueAsync();
            sbyte batLevel = ProcessBatteryData( result.Value.ToArray());
            MonitorDevice.Instance.battery = batLevel;
            ///Debug.WriteLine("<<Battery level>>" + batLevel);

            batteryCharacteristic.ValueChanged += Battery_ValueChanged;

            var currentDescriptorValue = await batteryCharacteristic.ReadClientCharacteristicConfigurationDescriptorAsync();

            if ((currentDescriptorValue.Status != GattCommunicationStatus.Success) ||
                (currentDescriptorValue.ClientCharacteristicConfigurationDescriptor != GattClientCharacteristicConfigurationDescriptorValue.Notify))
            {
                GattCommunicationStatus status =
                    await batteryCharacteristic.WriteClientCharacteristicConfigurationDescriptorAsync(
                    GattClientCharacteristicConfigurationDescriptorValue.Notify);

                if (status == GattCommunicationStatus.Unreachable)
                {
                    // Register a PnpObjectWatcher to detect when a connection to the device is established,
                    // such that the application can retry device configuration.
                    //StartDeviceConnectionWatcher();
                }
            }
        }

        void Battery_ValueChanged(GattCharacteristic sender, GattValueChangedEventArgs args)
        {
            var result = args.CharacteristicValue;
            MonitorDevice.Instance.battery = ProcessBatteryData(result.ToArray());
            Debug.WriteLine("Battery value change"+ ProcessBatteryData(result.ToArray()));
        }

        /// <summary>
        /// Get Battery status data.
        /// </summary>
        /// <param name="data"></param>
        sbyte ProcessBatteryData(byte[] data)
        {
            sbyte percent = (sbyte)data[0];
            return percent;
        }

        public void Dispose()
        {
            service.Dispose();
            if(batteryCharacteristic != null)
            {
                batteryCharacteristic.ValueChanged -= Battery_ValueChanged;
                batteryCharacteristic = null;
            }
            BatteryLevelService.instance = null;
        }
    }
    
}