﻿using Medibio.Modules.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;
using Windows.Storage.Streams;

namespace Medibio.WinPhone.BluetoothLE
{
    public class DeviceInfoService : IDisposable
    {
        GattDeviceService service;
        const string DeviceSerialNumberUuid = "00002A25-0000-1000-8000-00805f9b34fb";
        const string DeviceFirmwareVersionUuid = "00002A28-0000-1000-8000-00805f9b34fb";

        bool hasDeviceInfo;
        public bool HasDeviceInfo
        {
            get
            {
                return hasDeviceInfo;
            }
            set
            {
                hasDeviceInfo = value;
            }
        }

        static DeviceInfoService instance;
        public static DeviceInfoService Instance
        {
            get
            {
                return instance ?? (instance = new DeviceInfoService());
            }
        }

        DeviceInfoService()
        {

        }

        public async Task InitializeDeviceInfoAsync(DeviceInformation deviceInfo)
        {
            try
            {
                //deviceContainerId = "{" + device.Properties["System.Devices.ContainerId"] + "}";
                service = await GattDeviceService.FromIdAsync(deviceInfo.Id);
                //GattDeviceService.
                if (service != null)
                {
                    //IsServiceInitialized = true;
                    await GetDeviceInfo();
                }
                else
                {
                    // rootPage.NotifyUser("Access to the device is denied, because the application was not granted access, " "or the device is currently in use by another application.",NotifyType.StatusMessage);
                }
            }
            catch (Exception e)
            {

                Debug.WriteLine("ERROR: Accessing your device failed." + Environment.NewLine + e.Message);
            }
        }

        async Task GetDeviceInfo()
        {
            var characteristics = service.GetAllCharacteristics();

            foreach (var characteristic in characteristics)
            {
                var result = await characteristic.ReadValueAsync();
                if (characteristic.Uuid == GattCharacteristicUuids.FirmwareRevisionString)
                {
                    MonitorDevice.Instance.firmwareVersion = DataReader.FromBuffer(result.Value).ReadString(result.Value.Length);
                    Debug.WriteLine("<<Firmware>>" + MonitorDevice.Instance.firmwareDate);
                }
                else if (characteristic.Uuid == GattCharacteristicUuids.ModelNumberString)
                {
                    MonitorDevice.Instance.modelNumber = DataReader.FromBuffer(result.Value).ReadString(result.Value.Length);
                    Debug.WriteLine("<<model number>>" + MonitorDevice.Instance.modelNumber);
                }
                else if (characteristic.Uuid == GattCharacteristicUuids.ManufacturerNameString)
                {
                    MonitorDevice.Instance.manufacturer = DataReader.FromBuffer(result.Value).ReadString(result.Value.Length);
                    Debug.WriteLine("<<manufacturer>>" + MonitorDevice.Instance.manufacturer);
                }
                else if (characteristic.Uuid == GattCharacteristicUuids.HardwareRevisionString)
                {
                    MonitorDevice.Instance.hardwareRevision = DataReader.FromBuffer(result.Value).ReadString(result.Value.Length);
                    Debug.WriteLine("<<hardwareRevision>>" + MonitorDevice.Instance.hardwareRevision);
                }
                else if (characteristic.Uuid.ToString().ToUpper().Equals(DeviceFirmwareVersionUuid))
                {
                    MonitorDevice.Instance.firmwareVersion = DataReader.FromBuffer(result.Value).ReadString(result.Value.Length);
                    Debug.WriteLine("<<hardwareRevision>>" + MonitorDevice.Instance.hardwareRevision);
                }
                else if (characteristic.Uuid.ToString().ToUpper().Equals(DeviceSerialNumberUuid))
                {
                    MonitorDevice.Instance.serialNumber = DataReader.FromBuffer(result.Value).ReadString(result.Value.Length);
                    Debug.WriteLine("<<hardwareRevision>>" + MonitorDevice.Instance.hardwareRevision);
                }
                else
                {
                    Debug.WriteLine("Device info unhandled characteristics" + characteristic.Uuid.ToString());
                }
            }

            hasDeviceInfo = true;
        }

        public void Dispose()
        {
            service?.Dispose();
            service = null;
            DeviceInfoService.instance = null;
        }
    }
}
