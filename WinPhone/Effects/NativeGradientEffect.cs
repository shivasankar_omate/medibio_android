﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.Effects;
using Medibio.WinPhone.Effects;
using Windows.UI.Xaml.Media;
using Xamarin.Forms;
using Xamarin.Forms.Platform.WinRT;

[assembly:ResolutionGroupName("Medibio")]
[assembly:ExportEffect(typeof(NativeGradientEffect),"GradientEffect")]
namespace Medibio.WinPhone.Effects
{
    class NativeGradientEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            try
            {
                View view = (View)this.Element;
                var effects = new List<Effect>(Element.Effects);
                var effect = (GradientEffect)effects.Find(e => e is GradientEffect);
                if (view != null)
                {

                    LinearGradientBrush gradientBrush = new LinearGradientBrush()
                    {
                        SpreadMethod = GradientSpreadMethod.Pad
                    };
                    int i = 0;
                    foreach (var color in effect.Colors)
                    {
                        byte alpha = (byte)(color.A * 255);
                        byte red = (byte)(color.R * 255);
                        byte green = (byte)(color.G * 255);
                        byte blue = (byte)(color.B * 255);
                        
                        gradientBrush.GradientStops.Add(new GradientStop() { Color = Windows.UI.Color.FromArgb(alpha, red, green, blue), Offset = effect.StartPoints[i]});

                        i++;

                    }
                    //gradientBrush.GradientStops.Add(new GradientStop() { Color = Windows.UI.Color.FromArgb(100,255,0,0), Offset = 0 });
                    //gradientBrush.GradientStops.Add(new GradientStop() { Color = Windows.UI.Color.FromArgb(100, 0, 255, 0), Offset = 0.5 });
                    //gradientBrush.GradientStops.Add(new GradientStop() { Color = Windows.UI.Color.FromArgb(100, 0, 0, 255), Offset = 1 });

                    //Setting border if it is given.
                    if (effect.BorderRadious != 0)
                    {
                        
                    }
                    //Check whether it is view or contianer.
                    if (Control == null && Container != null)
                    {
                        (Container as Windows.UI.Xaml.Controls.Panel).Background = gradientBrush;
                        (Container as Windows.UI.Xaml.Controls.Panel).Background = gradientBrush;
                        (Container as Windows.UI.Xaml.Controls.Panel).InvalidateMeasure();
                        //(Container as Windows.UI.Xaml.Controls.Panel).Background = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 255, 0));
                    }
                    else
                    {
                        (Control as Windows.UI.Xaml.Controls.Control).Background = gradientBrush;
                        (Control as Windows.UI.Xaml.Controls.Control).BorderThickness = new Windows.UI.Xaml.Thickness(0d);
                        //(Control as Windows.UI.Xaml.Controls.Control).
                    }

                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception in Gradient Effect android:>>" + e.Message);
            }
        }

        protected override void OnDetached()
        {
            //Nothig to do with it.
        }
    }
}
