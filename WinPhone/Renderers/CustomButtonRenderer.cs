﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.WinPhone.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.WinRT;
using System.Diagnostics;

[assembly:ExportRenderer(typeof(Xamarin.Forms.Button), typeof(CustomButtonRenderer))]
namespace Medibio.WinPhone.Renderers
{
    public class CustomButtonRenderer: ButtonRenderer
    {
        public CustomButtonRenderer()
        {
            //Even Custructor also not getting called
            Debug.WriteLine("This is Button Renderer");
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            //I set the breakpoint over here which never caught.
            base.OnElementChanged(e);
            if(Control != null)
            {
                Control.BorderThickness = new Windows.UI.Xaml.Thickness(0d);
            }
        }
    }
}
