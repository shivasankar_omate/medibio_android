﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.WinPhone.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.WinRT;

[assembly: ExportRenderer(typeof(Xamarin.Forms.Label), typeof(CustomLabelRenderer))]
namespace Medibio.WinPhone.Renderers
{
    public class CustomLabelRenderer: LabelRenderer
    {
        public CustomLabelRenderer()
        {
            Debug.WriteLine("this is label renderer");
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
        }
    }
}
