﻿using Medibio.FileHelper;
using Medibio.WinPhone.FileHelper;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileHelperImplementation))]
namespace Medibio.WinPhone.FileHelper
{
    class FileHelperImplementation : IFileHelper
    {
        public async Task WriteAsync(string filePath, string text)
        {
            try
            {

                var file = await StorageFile.GetFileFromPathAsync(filePath);
                lock (file)
                {
                    FileIO.AppendTextAsync(file, text);
                }
                /*
            

            Byte[] textArray = System.Text.Encoding.UTF8.GetBytes(text);
            using (var stream = await file.OpenStreamForWriteAsync())
            {
                stream.Seek(0, SeekOrigin.End);
                await stream.WriteAsync(textArray, 0, textArray.Length);
            }

            file = null;
            */
                //

            }
            catch (Exception e)
            {
                Debug.WriteLine("on file write" + e.Message);
            }
        }
    }
}
