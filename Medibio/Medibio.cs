﻿using System;
using System.Diagnostics;
using Acr.Settings;
using Medibio.APIHandler;
using Medibio.Modules;
using Medibio.Modules.APIHandler;
using Medibio.Modules.Common;
using Medibio.Modules.Scan.Model;
using Medibio.Modules.Scan.View;
using Medibio.Modules.Scan.ViewModel;
using Xamarin.Forms;
using Medibio.Modules.LogHandler;
using Medibio.Modules.Result.View;
using Xamarin.Forms.Xaml;
using Medibio.Modules.Result.ViewModel;
using System.Collections.Generic;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Medibio
{
	public class App : Application
	{
		public static string AuthToken;

		private static string _userId;
		public static string UserId
		{
			get
			{
				return _userId;
			}
			set
			{
				_userId = value;
			}
		}

		private static string _token;
		public static string Token
		{
			get
			{
				return _token;
			}
			set
			{
				_token = value;
			}
		}

		public App()
		{

			//Set the Application environment.
			AppEnvironment.CurrentEnvironment = EnvironmentType.Scratch;

			List<ScanResult> scanList = new List<ScanResult>();
			scanList.Add(new ScanResult
			{
				Id = "573af169ca842cbe31738211",
				TimeStamp = "2016-05-17 15:51:53.000 GMT+05:30",
				Viewed = true,
				Reviewed = true,
				Duration = "00:10",
				Result = "-4"
			});
			scanList.Add(new ScanResult
			{
				Id = "573af169ca842cbe31738211",
				TimeStamp = "2016-05-17 10:28:13.000 GMT+05:30",
				Viewed = true,
				Reviewed = true,
				Duration = "06:10",
				Result = "-2"
			});
			scanList.Add(new ScanResult
			{
				Id = "573af169ca842cbe31738211",
				TimeStamp = "2016-05-19 22:16:01.000 GMT+05:30",
				Viewed = true,
				Reviewed = true,
				Duration = "08:10",
				Result = "3"
			});
			scanList.Add(new ScanResult
			{
				Id = "573af169ca842cbe31738211",
				TimeStamp = "2016-05-19 22:41:32.000 GMT+05:30",
				Viewed = true,
				Reviewed = true,
				Duration = "09:10",
				Result = "4"
			});
			scanList.Add(new ScanResult
			{
				Id = "573af169ca842cbe31738211",
				TimeStamp = "2016-05-19 22:16:01.000 GMT+05:30",
				Viewed = true,
				Reviewed = true,
				Duration = "12:10",
				Result = "5"
			});
			/*
            NavigationHandler.GlobalNavigator.Navigation.PushAsync(new ScanResultsPage()
            {
                BindingContext = new ScanResultsViewModel(scanList)
            });
            
            MainPage = NavigationHandler.GlobalNavigator;
            */
			/*MainPage = new ContentPage()
            {
                BackgroundColor = Color.Blue
            };*/
			//Retrive App settings


			var scanDetails = Settings.Local.Get<Scan>(AppConstants.CURRENT_SCANNING, null);
			if (null == scanDetails)
			{
				Debug.WriteLine("No scan record is detected.");
				// The root page of your application
				LaunchMainPage();
			}
			else
			{
				LaunchScanInProcessPage();
				Debug.WriteLine("Scan record found");
				Debug.WriteLine("Scan file>>>" + scanDetails.ScanFileName);
				TryToRestartConnection(scanDetails);
			}

			// initialise
			//GetProgramCode();
			//LaunchMainPage();

			Debug.WriteLine("********** On Construction of APP class ***********");
		}

		protected override void OnStart()
		{
			// Handle when your app starts
			Debug.WriteLine(" ********** On start ***********");
			Logger.Handler.WriteLog("app started");
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
			Debug.WriteLine(" ********** On Sleep ***********");
			if (ScanManager.Manager.IsScanning)
			{
				Settings.Local.Set<Scan>(AppConstants.CURRENT_SCANNING, ScanManager.Manager.ScanDetail);
			}
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
			Debug.WriteLine(" ********** On Resume ***********");
			if (ScanManager.Manager.IsScanning)
			{
				Settings.Local.Set<Scan>(AppConstants.CURRENT_SCANNING, null);
			}
		}

		void LaunchMainPage()
		{
			var page = new HomePage();
			NavigationHandler.GlobalNavigator.Navigation.PushAsync(page);
			MainPage = NavigationHandler.GlobalNavigator;
		}

		void LaunchScanInProcessPage()
		{
			ContentPage page = new ScanInProgressPage();
			//page.BindingContext = new ScanInProgressViewModel(scanDetails);
			NavigationHandler.GlobalNavigator.Navigation.PushAsync(page);
			MainPage = NavigationHandler.GlobalNavigator;
		}

		private async void TryToRestartConnection(Scan scanDetails)
		{
			ScanManager.Manager.StartConnection(scanDetails, () =>
		   {
			   Device.BeginInvokeOnMainThread(() =>
			   {
				   NavigationHandler.GlobalNavigator.CurrentPage.BindingContext = new ScanInProgressViewModel();
			   });

		   }, (errorCode) =>
		   {
			   if (BluetoothErrorCode.NOT_ENABLED.Equals(errorCode))
			   {
				   NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, AppConstants.BLE_SETTINGS_ERROR, AppConstants.OK_TEXT);
			   }
			   else if (BluetoothErrorCode.DEVICE_NOT_FOUND.Equals(errorCode))
			   {
				   NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, AppConstants.CANNT_FIND_DEVICE, AppConstants.OK_TEXT);
			   }

		   });
		}

		async void GetProgramCode()
		{
			var page = new ProgramCodePage();
			await NavigationHandler.GlobalNavigator.Navigation.PushAsync(page);
		}

		async void GetLogin()
		{
			var page = new LoginPage(string.Empty);
			await NavigationHandler.GlobalNavigator.Navigation.PushAsync(page);

			//LoginHandler.CheckProgramCode("TST000001", null,
			//    response =>
			//    {
			//        Debug.WriteLine("Program code success");

			//        LoginHandler.Login("test.medibio@gmail.com", "12345", response.GroupInfo.Id,
			//            loginResponse =>
			//            {
			//                Debug.WriteLine("Login sucessfull.");
			//                Token = loginResponse.Token;
			//                UserId = loginResponse.User.Id;
			//            },
			//            errorResponse =>
			//            {
			//                Debug.WriteLine("Login Failed");
			//            });
			//    },
			//    errorResponse =>
			//    {
			//        Debug.WriteLine("Program code error");
			//    });
		}


	}
}

