﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.BluetoothLE.Interfaces;
using Xamarin.Forms;

namespace Medibio.BluetoothLE
{
    public static class Adapter

    {
        private static IBluetoothAdapter _Current;

        public static IBluetoothAdapter Current
        {
            get
            {
                return _Current ?? (_Current = DependencyService.Get<IBluetoothAdapter>());
            }
        }
    }
}
