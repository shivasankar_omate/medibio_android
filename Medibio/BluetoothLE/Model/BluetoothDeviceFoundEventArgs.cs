﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.BluetoothLE.Interfaces;

namespace Medibio.BluetoothLE.Model
{
    public class BluetoothDeviceFoundEventArgs: EventArgs
    {
        public IBluetoothDevice Device { private set; get; }
        public BluetoothDeviceFoundEventArgs(IBluetoothDevice device)
        {
            Device = device;
        }


    }
}
