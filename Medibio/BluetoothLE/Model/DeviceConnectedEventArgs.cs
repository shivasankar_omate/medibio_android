﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.BluetoothLE.Interfaces;

namespace Medibio.BluetoothLE.Model
{
    public class DeviceConnectedEventArgs: EventArgs
    {
        public IBluetoothDevice Devcie;

        public DeviceConnectedEventArgs(IBluetoothDevice device) {
            Devcie = device; 
        }
    }
}
