﻿
using System;
using Medibio.BluetoothLE.Interfaces;

namespace Medibio.BluetoothLE.Model
{
    public class BLEDeviceDisconnectedEventArgs: EventArgs
    {
        public IBluetoothDevice Device { private set; get; }
        public BLEDeviceDisconnectedEventArgs(IBluetoothDevice device)
        {
            Device = device;
        }
    }
}
