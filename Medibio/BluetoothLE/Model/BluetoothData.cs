﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medibio.BluetoothLE.Model
{
    public class BluetoothData
    {
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string HardwareRevision { get; set; }
        public string FirmwareVersion { get; set; }
        public string FirmwareDate { get; set; }
        public string IbiResolution { get; set; }
        public string StartTime { get; set; }
        public string TimeStamp
        {
            get
            {
                return DateTime.Now.ToString();
            }
        }

        public string Contact { get; set; } = "0";
        public int[] Ibi { get; set; }
        public int HeartRate { get; set; } = 0;
        public float Activity { get; set; } = 0.0000f;
        public float Respiration { get; set; } = 0.0f;
        public float Temperature { get; set; } = 0.0f;
        public GeoLocation Location { get; set; }
        public string iPhoneAccel { get; set; }
        public int BatteryPercent { get; set; }
        public float Posture { get; set; }
        public float Hrv { get; set; }
        public float PeakActivity { get; set; }

        public string GetDeviceInfo()
        {
            return string.Format("Monitor Manufacturer: {0}Monitor Model: {1}Monitor Serial Number: {2}Monitor Hardware Revision: {3}Monitor Firmware Version: {4}Monitor Firmware Date: {5}Monitor IBI Resolution: {6}Start Time:{7}", Manufacturer, Model, SerialNumber, HardwareRevision, FirmwareVersion, FirmwareDate, IbiResolution, StartTime);
        }

        public string GetAllData()
        {
            return string.Format("{0},{1}.{2},{3},{4:4},{5:1},{6:1},{7},{8},{9},{10},{11:2},{12:2},{13:2}", TimeStamp,Contact,Ibi,HeartRate,Activity,Respiration,Temperature, Location, iPhoneAccel, BatteryPercent, Posture, Hrv, PeakActivity);
        }

        public BluetoothData Clone()
        {
            return new BluetoothData
            {
                Manufacturer = this.Manufacturer,
                Model = this.Model,
                SerialNumber = this.SerialNumber,
                HardwareRevision = this.HardwareRevision,
                FirmwareVersion = this.FirmwareVersion,
                FirmwareDate = this.FirmwareDate,
                IbiResolution = this.IbiResolution,
                StartTime = this.StartTime,

                Contact = this.Contact,
                Ibi = this.Ibi,
                HeartRate = this.HeartRate,
                Activity = this.Activity,
                Respiration = this.Respiration,
                Temperature = this.Temperature,
                Location = this.Location,
                iPhoneAccel = this.iPhoneAccel,
                BatteryPercent = this.BatteryPercent,
                Posture = this.Posture,
                Hrv = this.Hrv,
                PeakActivity = this.PeakActivity
            };
        }
    }

    public class GeoLocation
    {
        public double Lat;
        public double Long;

        public override string ToString()
        {
            return string.Format("{{0};{1}}",Lat,Long);
        }
    }

}
