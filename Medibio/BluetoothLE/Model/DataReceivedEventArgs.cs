﻿using System;
using Medibio.Modules.Model;

namespace Medibio.BluetoothLE.Model
{
	public class BLEDataReceivedEventArgs: EventArgs
	{
		public HRData Data{ get; set;}
		public BLEDataReceivedEventArgs (HRData data)
		{
			Data = data;
		}

	}
}

