﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.BluetoothLE.Interfaces;

namespace Medibio.BluetoothLE.Model
{
    public class BLEDeviceConnectedEventArgs: EventArgs
    {
        public IBluetoothDevice Device { private set; get; }
        public BLEDeviceConnectedEventArgs(IBluetoothDevice device)
        {
            Device = device;
        }
    }
}
