﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.BluetoothLE.Model;

namespace Medibio.BluetoothLE.Interfaces
{
    public interface IBluetoothDevice
    {
        string Name { get; }
        string Address { get; }
        DeviceState State { get; }
        event EventHandler<DeviceConnectedEventArgs> OnConnected;// { get; set; }
        event EventHandler<DeviceConnectedEventArgs> OnLost;// { get; set; }
        bool CreateSocket(Action action); //TODO: Action to be generic type
        bool ReadData(Action action); //TODO: Action to be generic type

    }
}
