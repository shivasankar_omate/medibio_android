﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.BluetoothLE.Model;

namespace Medibio.BluetoothLE.Interfaces
{
    public interface IBluetoothAdapter
    {
        string Name { get; }
        string Address { get; }
        bool IsEnabled { get; }
        bool IsDiscoverable { get; }
        bool IsConnected { get; }
        DeviceState DeviceCurrentState { get; }
        bool IsScanning { get; set; }
        BluetoothType TargetType { get; set; }
        List<IBluetoothDevice> DiscoveredDeviceList { get; set; }
        IBluetoothDevice ConnectedDevice { get; set; }
        event EventHandler<BluetoothDeviceFoundEventArgs> OnDeviceFound;
        event EventHandler<BLEDataReceivedEventArgs> OnDataReceived;
        event EventHandler<BLEDeviceDisconnectedEventArgs> OnDeviceDisconnected;
        event EventHandler<BLEDeviceConnectedEventArgs> OnDeviceConnected;
        event EventHandler<EventArgs> OnDeviceStateChanged;
        Task StartScanAsync(int timeoutInMilliSeconds);
        Task StopScan();
        Task ConnectDevice(IBluetoothDevice device);
        Task DisconnectDevice(IBluetoothDevice device);

    }
}
