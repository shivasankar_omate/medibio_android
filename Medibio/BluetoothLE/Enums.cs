﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medibio.BluetoothLE
{
    public enum BluetoothType { BluetoothLE, Bluetooth}
    public enum DeviceState { Disconnected, Connecting, Connected }
}
