﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medibio.FileHelper
{
    public interface IFileHelper
    {
        Task WriteAsync(string filePath, string Text);
    }
}
