﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.BluetoothLE.Interfaces;
using Medibio.BluetoothLE.Model;
using Medibio.Modules.LogHandler;
using Xamarin.Forms;
using Medibio.Modules.Scan.Model;
using Medibio.APIHandler;
using Plugin.Connectivity;
using System.Diagnostics;

namespace Medibio.Modules
{
    public class ScanManager
    {
        const int scanTimeoutInSeconds = 20;
        Action successCallback;
        Action<string> errorCallback;

        public Scan.Model.Scan ScanDetail;
        public IBluetoothAdapter adapter = BluetoothLE.Adapter.Current;
        public event EventHandler<BLEDataReceivedEventArgs> OnDataReceived;
        public event EventHandler<EventArgs> OnConnectionStateChanged;

        //Singleton object for ScanManager class
        static ScanManager manager;
        public static ScanManager Manager
        {
            get
            {
                return manager ?? (manager = new ScanManager());
            }
        }

        //ToCheck whether the scan is running or not.
        bool isScanning;
        public bool IsScanning
        {
            get
            {
                return isScanning;
            }
            set
            {
                isScanning = value;
            }
        }

        public bool IsBluetoothEnabled
        {
            get
            {
                return adapter.IsEnabled;
            }
        }

        bool hasScanDetails;
        public bool HasScanDetails
        {
            get
            {
                return (ScanDetail == null) ? false : true;
            }
        }

        /// <summary>
        /// Empty private Constructor
        /// </summary>
        private ScanManager()
        {
            //Empty
        }

        public async void StartConnection(Scan.Model.Scan scanDetails, Action successCallback, Action<string> errorCallback)
        {
            ScanDetail = scanDetails;
            StartConnection(successCallback, errorCallback);
        }

        /// <summary>
        /// This method will search for bluetoothLE devices that has capability of HRM serivce.
        /// </summary>
        /// 
        public async void StartConnection(Action successCallback, Action<string> errorCallback)
        {
            this.successCallback = successCallback;
            this.errorCallback = errorCallback;

            await Logger.Handler.CreateNewLogFile();
            Logger.Handler.WriteLog("Trying to Find BLE devices.");

            if (adapter.IsEnabled)
            {
                adapter.OnDeviceFound += Adapter_OnDevcieFound;
                Logger.Handler.WriteLog("Scan starts for LE devcies.");
                adapter.StartScanAsync(0);

                Device.StartTimer(TimeSpan.FromSeconds(scanTimeoutInSeconds), () =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (adapter.IsScanning)
                        {
                            adapter.StopScan();

                            if (adapter.DiscoveredDeviceList.Count == 0)
                            {
                                adapter.OnDeviceFound -= Adapter_OnDevcieFound;
                                errorCallback(BluetoothErrorCode.DEVICE_NOT_FOUND);
                            }
                        }
                    });

                    return false;
                });

            }
            else
            {
                errorCallback?.Invoke(BluetoothErrorCode.NOT_ENABLED);
                Logger.Handler.WriteLog("Connection failed. Bluetooth settings not supported");
            }

        }

        private void Adapter_OnDevcieFound(object sender, BluetoothDeviceFoundEventArgs e)
        {
            if (adapter.DiscoveredDeviceList.Count == 1)
            {
                adapter.OnDeviceFound -= Adapter_OnDevcieFound;
                adapter.OnDeviceConnected += Adapter_OnDeviceConnected;
                Logger.Handler.WriteLog("Trying to connect device: " + adapter.DiscoveredDeviceList[0].Name);
                adapter.ConnectDevice(adapter.DiscoveredDeviceList[0]);
            }
        }


        private void Adapter_OnDeviceConnected(object sender, BLEDeviceConnectedEventArgs e)
        {
            Logger.Handler.WriteLog("Device Connected " + Environment.NewLine + " Device Name:" + e.Device.Name + Environment.NewLine + " Device Address: " + e.Device.Address);
            /*if ((null == ScanDetail.Id) && CrossConnectivity.Current.IsConnected)
            {

                Task.Run(() =>
                {
                    ScanHandler.NewScan(
                        ScanDetail.StartTime,
                        ScanDetail.ScanFileName,
                        (response) =>
                        {
                            ScanDetail.Id = response.Id;
                        },
                        (errorResponse) =>
                        {
                            //TODO Check with requirement 
                            Debug.WriteLine("New Scan API hit failed.");
                        });
                });
            }*/
            adapter.OnDeviceConnected -= Adapter_OnDeviceConnected;
            adapter.OnDataReceived += Adapter_OnDataReceived;
            adapter.OnDeviceStateChanged += Adapter_OnDeviceStateChanged;
            adapter.OnDeviceDisconnected += Adapter_OnDeviceDisconnected;
        }

        private void Adapter_OnDeviceDisconnected(object sender, BLEDeviceDisconnectedEventArgs e)
        {
            Logger.Handler.WriteLog("Device disconnected");
        }

        private void Adapter_OnDeviceStateChanged(object sender, EventArgs e)
        {
            Logger.Handler.WriteLog("Device state changed:" + adapter.DeviceCurrentState);
            if (adapter.DeviceCurrentState == BluetoothLE.DeviceState.Disconnected)
            {
                Logger.Handler.WriteLog("Devcie Disconnected");
            }
            else if (adapter.DeviceCurrentState == BluetoothLE.DeviceState.Connected)
            {
                Logger.Handler.WriteLog("Device Reconnected");
            }
            OnConnectionStateChanged?.Invoke(sender, e);
        }

        private void Adapter_OnDataReceived(object sender, BLEDataReceivedEventArgs e)
        {
            if (!IsScanning)
            {
                successCallback?.Invoke();
                IsScanning = true;
                Logger.Handler.WriteLog("Test scan successfull.");
                return;
            }
            if (OnDataReceived != null)
            {
                //Logger.Handler.WriteLog("Start receiving data.");
                OnDataReceived?.Invoke(sender, e);
            }
        }

        public async Task CompleteScan()
        {
            if (adapter.IsConnected)
            {
                adapter.OnDataReceived -= Adapter_OnDataReceived;
                await adapter.DisconnectDevice(adapter.ConnectedDevice);
                adapter.OnDeviceStateChanged -= Adapter_OnDeviceStateChanged;
                adapter.OnDeviceDisconnected -= Adapter_OnDeviceDisconnected;
                ScanDetail = null;
                IsScanning = false;
                /* if(TargetPlatform.Windows == Device.OS || TargetPlatform.WinPhone == Device.OS)
                 {
                     adapter.con
                 }
                 */
                Logger.Handler.WriteLog("Scan Completed");
                Task.Run(() =>
                {
                    ScanHandler.UpdateScanEvent(ScanDetail, ScanEvent.COMPLETE,
                        (response) =>
                        {
                            Debug.WriteLine("Scan completed event success");
                        },
                        (errorResponse) =>
                        {
                            Debug.WriteLine("Scan event update failed.");
                        });
                });
            }
        }
    }

    public static class BluetoothErrorCode
    {
        public static readonly string NOT_ENABLED = "100";
        public static readonly string DEVICE_NOT_FOUND = "101";
        public static readonly string SERVICE_NOT_FOUND = "102";
        public static readonly string SCAN_TIMEOUT = "103";
    }
}
