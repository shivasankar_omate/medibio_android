﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Medibio.Resourses
{
	public static class AppResources
	{
		public static readonly Color PrimaryDarkColor = Color.FromHex("#343533");
		public static readonly Color SecondayDarkColor = Color.FromHex("#343533");
		public static readonly Color PrimaryLiteColor = Color.FromHex("#FFFFFF");
		public static readonly Color SecondaryLiteColor = Color.FromHex("#343533");
		public static readonly Color Orange = Color.FromHex("#DB7744");
		public static readonly Color PrimaryFontColor = Color.FromHex("#9d3467");
		public static readonly Color SecondaryFontColor = Color.FromHex("#000000");
		public static readonly Color PrimaryFont = Color.Accent;
	}
}
