﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medibio.Modules.Common
{
    public static class ErrorCodes
    {
        public static class Login
        {
            public const string E_GRP = "E_GRP";
            public const string E_NOACCOUNT = "E_NOACCOUNT";
            public const string E_INVALID = "E_INVALID";
            public const string E_VALIDATION = "E_VALIDATION";
            public const string E_ERROR = "E_ERROR";
        }

        public static class ProgramCode
        {
            public const string E_VALIDATION = "E_VALIDATION";
        }

		public static class Register
		{
			public const string E_GRP = "E_GRP";
			public const string E_EMAIL = "E_EMAIL";
			public const string E_VALIDATION = "E_VALIDATION";
			public const string E_ERROR = "E_ERROR";
		}

		public static class Reset
		{
			public const string E_SENT = "E_SENT";
			public const string E_EMAIL = "E_EMAIL";
			public const string E_VALIDATION = "E_VALIDATION";
			public const string E_ERROR = "E_ERROR";
		}
    }



    public static class ErrorMessages
    {
        public static class Login
        {
            public static readonly string E_GRP = "The email and password are correct but the group doesn't match.";
            public static readonly string E_NOACCOUNT = "The email address doesn't exist in our DB.";
            public static readonly string E_INVALID = "The email and password are not valid.";
            public static readonly string E_VALIDATION = "The following validation errors occurred:";
            public static readonly string E_ERROR = "An unknown error has occurred.";
        }

		public static class Register
		{
			public static readonly string E_GRP = "The group specified is invalid";
			public static readonly string E_EMAIL = "The email address is already in use";
			public static readonly string E_VALIDATION = "Vaidation errors, details are passed with errors key";
			public static readonly string E_ERROR = "An unknown error has occurred.";
		}

        public static class ProgramCode
        {
            public static readonly string E_VALIDATION = "The following validation errors occurred:";
            public static readonly string E_INVALID = "The program code you have entered is not valid.";
            public static readonly string E_ERROR = "An unknown error has occurred.";            
        }

		public static class Reset
		{
			public static readonly string E_SENT = "The email wasn't sent successfully due to the error passed back";
			public static readonly string E_EMAIL = "The email address sent wasn't found in the DB";
			public static readonly string E_VALIDATION = "Vaidation errors, details are passed with errors key";
			public static readonly string E_ERROR = "An unknown error has occurred.";
		}
    }
}
