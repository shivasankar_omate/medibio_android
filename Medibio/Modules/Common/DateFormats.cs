﻿using System;

namespace Medibio
{
	public class DateFormats
	{
		public static string FilenameTimestampFormat = "yyyyMMddHHmmssFFF";
		public static string StartTimeFormat = "yyyy-MM-dd HH:mm:ss 'GMT'zzz";
		public static string TimestampFormat = "yyyy-MM-dd HH:mm:ss";
        public static string ScanResultDateFormat = "MMM dd,yyyy h:mm tt";
        public static string DateTimeParseFormat = "yyyy-MM-dd HH:mm:ss.FFF 'GMT'zzz";


        public DateFormats ()
		{

		}
	}
}
