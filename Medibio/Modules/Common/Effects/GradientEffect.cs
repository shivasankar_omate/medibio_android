﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Medibio.Effects
{
	public enum GradientOrientation { Horizontal, Vertical }
	public class GradientEffect : RoutingEffect
	{
		public static Color[] DefaultColors = new Color[] {
				Color.FromHex("#983b57"),
				Color.FromHex("#b85158"), //203, 102, 77),
                Color.FromHex("#b85158"),
				Color.FromHex("#bb5457"),
				Color.FromHex("#ca654d"),
				Color.FromHex("#a94260"),
		};

		public static float[] DefaultStartPoints = new float[] {
				0, 0.20f, 0.35f, 0.50f, 0.75f, 1f };
		public Color[] Colors { get; set; } = DefaultColors;

		public float[] StartPoints { get; set; } = DefaultStartPoints;

		public int BorderRadious { get; set; } = 0;

		public int Angle { get; set; } = 0;

		public GradientOrientation Orientation { get; set; } = GradientOrientation.Horizontal;

		public GradientEffect() : base("Medibio.GradientEffect")
		{

		}

	}
}
