﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medibio.Modules.Common
{
    public static class AppConstants
    {
        //public static string SERVER_BASE_URL = @"https:\//app.medibio.com.au/api/v1";
        //Scratch : https:\//scratch.medibio.com.au:443/api/v1/  https:\//scratch.medibio.com.au:443/api/v1
        //public static readonly string SERVER_BASE_URL = @"https:\//scratch.medibio.com.au:443/";

        public static readonly string IS_SCAN_RUNNING_KEY = "IsScanRunning";
        public static readonly string CURRENT_SCANNING = "RunningScanObject";
        public static readonly string TEMP_DEVICE_ID = "WinPhone_Id";

		public static readonly string APP_NAME = "Medibio";
        public static readonly string APP_VERSION = "0.1";
        public static readonly string OK_TEXT = "OK";
        public static readonly string CANNT_FIND_DEVICE = "Can't find any BLE devices";
        public static readonly string BLE_SETTINGS_ERROR = "Check your bluetooth settings";
        public static readonly string UPLOAD_SUCCESSFUL = "Files uploaded successfully";
        public static readonly string CANNT_UPLOAD_FILE = "Can't upload files.";
        public static readonly string WANNA_COMPLETE = "Are you sure?";
        public static readonly string NETWORK_ERROR = "Check your network connection";
        public static readonly string CANCEL_TEXT = "Cancel";
        public static readonly string YES = "Yes";
        public static readonly string CONFIRM = "Confirm";
		public static string WARNING = "Warning";

		public static readonly string PASSWORD_EMAIL_SENT = "Password Reset";
		public static readonly string PASSWORD_RESET = "Please check your email for a link to reset your password and try again";
		public static string PASSWORD_EMAIL_SENT_FAILED = "Reset Request Failed. Please try again later";

        public static readonly string CANT_GET_DATA = "Can't able to get data from server.";
    }
}
