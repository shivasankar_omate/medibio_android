﻿using System;
using RestSharp.Portable;
using System.Diagnostics;
using RestSharp.Portable.HttpClient;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Medibio.APIHandler;
using Medibio.Modules.Common;
using Medibio.Modules.APIHandler;

namespace Medibio
{
    public class APIServiceProvider
    {
        private APIServiceProvider()
        {
        }

        private static APIServiceProvider _ServiceProvider;
        public static APIServiceProvider ServiceProvider
        {
            get
            {
                return _ServiceProvider ?? (_ServiceProvider = new APIServiceProvider());
            }
        }

        public readonly string AuthToken;

        public async Task Execute<T>(RestRequest request, Action<T> successCallback, Action<ErrorResponse> errorCallback) where T : new()
        {
            try
            {
                request.AddParameter("version", "1.5", ParameterType.HttpHeader);//.AddHeader("version", "1.5");
                //request.AddHeader("version", "1.5");
                using (IRestClient client = new RestClient(AppEnvironment.BaseUrl))
                {
                    //Debug.WriteLine(JsonConvert.SerializeObject(request));
                    IRestResponse<T> response = await client.Execute<T>(request);
                    Debug.WriteLine(JsonConvert.SerializeObject(response.Data));
                    successCallback?.Invoke(response.Data);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error Occured on API hit :" + ex.Message);
                //TODO: call error call back with valid error data
                errorCallback?.Invoke(new ErrorResponse());
            }
        }

        public async Task<bool> Execute(RestRequest request)
        {
            try
            {
                using (IRestClient client = new RestClient(AppEnvironment.BaseUrl))
                {
                    IRestResponse response = await client.Execute(request);
                    if (response.IsSuccess)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                
                //Must call API error handler
                Debug.WriteLine("Error Occured on API hit :" + ex.Message);
               
                return false;
            }
        }



    }
}

