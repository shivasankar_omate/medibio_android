﻿using System;
using System.Threading.Tasks;
using Medibio.Modules.Scan.Model;
using RestSharp.Portable;
namespace Medibio.APIHandler
{
    public static class ScanHandler

    {
        public static async Task NewScan(string timeStamp, string fileName, Action<ScanResponse> successCallback, Action<ErrorResponse> errorCallback)
        {
            RestRequest request = new RestRequest("api/v1/scan", Method.POST);
            request.AddHeader("token", App.AuthToken);
            request.AddBody(new
            {
                timestamp = timeStamp,
                filename = fileName
            });

            await APIServiceProvider.ServiceProvider.Execute<ScanResponse>(request, successCallback, errorCallback);
        }

        public static async Task UpdateScanEvent(Scan scan, string scanEvent, Action<ScanEventResponse> successCallback, Action<ErrorResponse> errorCallback) {
            RestRequest request = new RestRequest("api/v1/scan/{timeStamp}/event", Method.POST);
            request.AddHeader("token", App.AuthToken);
            request.AddUrlSegment("timeStamp",scan.StartTime);
            ScanDataModel requestData = new ScanDataModel()
            {
                EventType = scanEvent,
                Params = new ScanParams
                {
                    FileName = scan.ScanFileName,
                    Error = scan.ErrorMessage
                }
            };
            request.AddBody(requestData);

            await APIServiceProvider.ServiceProvider.Execute<ScanEventResponse>(request, successCallback, errorCallback);
        }

        public static async Task GetScanResults(int startIndex, int limitCount, Action<ScanResultResponse> successCallback, Action<ErrorResponse> errorCallback)
        {
            RestRequest request = new RestRequest("api/v1/scan_result?start={0}&limit={1}", Method.GET);
            request.AddHeader("token", App.AuthToken);
            request.AddUrlSegment("0",startIndex);
            request.AddUrlSegment("1",limitCount);

            await APIServiceProvider.ServiceProvider.Execute<ScanResultResponse>(request, successCallback, errorCallback);
        }

        public static async Task GetDetailedScanResult(string id, Action<ScanDetailedResult> successCallback, Action<ErrorResponse> errorCallback)
        {
            RestRequest request = new RestRequest("api/v1/scan_result/{id}", Method.GET);
            request.AddHeader("token", App.AuthToken);
            request.AddUrlSegment("id", id);

            await APIServiceProvider.ServiceProvider.Execute<ScanDetailedResult>(request, successCallback, errorCallback);
        }
    }

}
