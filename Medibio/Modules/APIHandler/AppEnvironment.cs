﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Xamarin.Forms;

namespace Medibio.Modules.APIHandler
{
    public enum EnvironmentType { Scratch, Test, Pilot }
    public static class AppEnvironment
    {
        public static EnvironmentType CurrentEnvironment = EnvironmentType.Scratch;

        public static readonly string CognitoIdentityPoolId = "us-east-1:e61c24eb-5a29-4588-a723-1c20e4172abb";

        public static readonly string ApplicationArn = ApplicationArnDev;

        public static readonly string ApplicationArnProd = "arn:aws:sns:us-east-1:523426821968:app/APNS/StressApp";

        public static readonly string ApplicationArnDev = "arn:aws:sns:us-east-1:523426821968:app/APNS_SANDBOX/StressApp";

        public static RegionEndpoint CongnitotRegion
        {
            get
            {
                switch (CurrentEnvironment)
                {
                    case EnvironmentType.Pilot:
                        {
                            return RegionEndpoint.USEast1;
                        }
                    case EnvironmentType.Test:
                        {
                            return RegionEndpoint.USEast1;
                        }
                    case EnvironmentType.Scratch:
                        {
                            return RegionEndpoint.USEast1;
                        }
                    default:
                        {
                            //It will not get executed 
                            return RegionEndpoint.USEast1;
                        }
                }
            }
        }

        public static RegionEndpoint DataRegion
        {
            get
            {
                switch (CurrentEnvironment)
                {
                    case EnvironmentType.Pilot:
                        {
                            return RegionEndpoint.APSoutheast2;
                        }
                    case EnvironmentType.Test:
                        {
                            return RegionEndpoint.APSoutheast2;
                        }
                    case EnvironmentType.Scratch:
                        {
                            return RegionEndpoint.USEast1;
                        }
                    default:
                        {
                            //It will not get executed 
                            return RegionEndpoint.USEast1;
                        }
                }
            }
        }

        public static RegionEndpoint LogRegion
        {
            get
            {
                switch (CurrentEnvironment)
                {
                    case EnvironmentType.Pilot:
                        {
                            return RegionEndpoint.USEast1;
                        }
                    case EnvironmentType.Test:
                        {
                            return RegionEndpoint.USEast1;
                        }
                    case EnvironmentType.Scratch:
                        {
                            return RegionEndpoint.USEast1;
                        }
                    default:
                        {
                            //It will not get executed 
                            return RegionEndpoint.USEast1;
                        }
                }
            }
        }
        /// <summary>
        /// Location for uploading the scan data file
        /// </summary>
        /// <value>The data bucket.</value>
        public static string DataBucket
        {
            get
            {
                switch (CurrentEnvironment)
                {
                    case EnvironmentType.Pilot:
                        {
                            //Need to change once we get the setting for pilot
                            if (Device.OS == TargetPlatform.Android)
							    return "mebdev-scratch-android-data"; 
                            else
							    return "mebdev-scratch-windows-data";
                        }
                    case EnvironmentType.Test:
                        {
                            //Need to change once we get the setting for test
                            if (Device.OS == TargetPlatform.Android)
							    return "mebdev-scratch-android-data";
                            else
							    return "mebdev-scratch-windows-data";
                        }
                    case EnvironmentType.Scratch:
                        {
                            if (Device.OS == TargetPlatform.Android)
                                return "meb-scratch-mobile-data";
                            else
							    return "meb-scratch-mobile-data";
                        }
                    default:
                        {
                            //It will not get executed 
						    return "mebdev-scratch-android-data";
                        }
                }
            }
        }

        
		/// <summary>
		/// Location for uploading application debug logs
		/// </summary>
		/// <value>The log bucket.</value>
        public static string LogBucket
        {
            get
            {
                switch (CurrentEnvironment)
                {
                    //Need to change once we get the setting for pilot
                    case EnvironmentType.Pilot:
                        {
						return "mebdev-scratch-android-logs";
                        }
                    //Need to change once we get the setting for test
                    case EnvironmentType.Test:
                        {
						return "mebdev-scratch-android-logs";
                        }
                    //Need to change once we get the setting for scratch
                    case EnvironmentType.Scratch:
                        {
						if (Device.OS == TargetPlatform.Android)
							return "meb-scratch-mobile-logs";
						else 
							return "meb-scratch-mobile-logs";
                        }
                    default:
                        {
                            //It will not get executed 
						return "mebdev-scratch-android-logs";
                        }
                }
            }
        }

        public static string BaseUrl
        {
            get
            {
                switch (CurrentEnvironment)
                {
                    case EnvironmentType.Pilot:
                        {
						return "https://scratch.medibio.com.au:443/";
                        }
                    case EnvironmentType.Test:
                        {
						return "https://scratch.medibio.com.au:443/";
                        }
                    case EnvironmentType.Scratch:
                        {
                            return "https://scratch.medibio.com.au:443/";
                        }
                    default:
                        {
                            //It will not get executed 
                            return "https://scratch.medibio.com.au:443/";
                        }
                }
            }
        }
    }
}
