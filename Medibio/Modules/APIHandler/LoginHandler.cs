﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Medibio.Modules.Common;
using RestSharp.Portable;

namespace Medibio.APIHandler
{
    public static class LoginHandler
    {
        public static async Task CheckProgramCode(string programCode, string email = null, Action<ProgramCodeResponse> successCallback = null, Action<ErrorResponse> errorCallback = null) {

            RestRequest request = new RestRequest(@"api/v1/auth/studycode", Method.POST);
            ProgramCodeRequest requestObject = new ProgramCodeRequest()
            {
                Code = programCode,
                Email = email            
            };

            request.AddBody(requestObject);

            await APIServiceProvider.ServiceProvider.Execute(request, successCallback, errorCallback);
        }

        public static async Task GetUserRegister(string email, string password, string groupId, Action<RegisterResponse> successCallback, Action<ErrorResponse> errorCallback) {

            RestRequest request = new RestRequest("api/v1/auth/register", Method.POST);
            RegisterRequest requestData = new RegisterRequest()
            {
                Email = email,
                Password = password,
                GroupId = groupId
            };
            request.AddJsonBody(requestData);

            await APIServiceProvider.ServiceProvider.Execute<RegisterResponse>(request, successCallback, errorCallback);
        }

        public static async Task ForgetPassword(string email, Action<ForgetPasswordResponse> successCallback, Action<ErrorResponse> errorCallback) {
            RestRequest request = new RestRequest("api/v1/auth/forgotpassword", Method.POST);
            request.AddBody(new
            {
                email = email
            });

            await APIServiceProvider.ServiceProvider.Execute<ForgetPasswordResponse>(request, successCallback, errorCallback);
        }

        public static async Task Login(string emailId, string password, string groupId, Action<LoginResponse> successCallback, Action<ErrorResponse> errorCallback)
        {
            RestRequest request = new RestRequest("api/v1/auth/login", Method.POST);
            LoginRequest requestData = new LoginRequest
            {
                Email = emailId,
                Password = password,
                GroupId = groupId
            };
            request.AddBody(requestData);

            await APIServiceProvider.ServiceProvider.Execute<LoginResponse>(request, successCallback, errorCallback);
        }

    }

    public class SampleApp
    {
        public string status;
    }
}
