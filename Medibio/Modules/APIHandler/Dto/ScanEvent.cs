﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medibio.APIHandler
{
    public class ScanEventRequest
    {
        [JsonProperty(PropertyName ="timestamp", NullValueHandling = NullValueHandling.Ignore)]
        public string event_type { get; set; }
        [JsonProperty(PropertyName ="filename", NullValueHandling = NullValueHandling.Ignore)]
        public EventParams Params { get; set; }
    }

    public class EventParams
    {
        [JsonProperty(PropertyName ="filename", NullValueHandling = NullValueHandling.Ignore)]
        public string filename { get; set; }
        [JsonProperty(PropertyName ="error", NullValueHandling = NullValueHandling.Ignore)]
        public string error { get; set; }
    }

    public class ScanEventResponse
    {
        [JsonProperty (PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }
        [JsonProperty (PropertyName = "timestamp", NullValueHandling = NullValueHandling.Ignore)]
        public string TimeStamp { get; set; }
        [JsonProperty(PropertyName = "filename", NullValueHandling =NullValueHandling.Ignore)]
        public string FileName { get; set; }
    }

    public class ScanInfo
    {
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "timestamp", NullValueHandling = NullValueHandling.Ignore)]
        public string TimeStamp { get; set; }
    }

    public class EventInfo
    {
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "created", NullValueHandling = NullValueHandling.Ignore)]
        public string Created { get; set; }
    }

    public class RootObject
    {
        [JsonProperty(PropertyName = "scan", NullValueHandling = NullValueHandling.Ignore)]
        public ScanInfo scan { get; set; }
        [JsonProperty(PropertyName = "event", NullValueHandling = NullValueHandling.Ignore)]
        public EventInfo Event { get; set; }
    }
}
