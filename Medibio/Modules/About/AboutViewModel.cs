﻿using System;
using Xamarin.Forms;

namespace Medibio
{
	public class AboutViewModel
	{
		public string About {
			get;
			set;
		} = "Lorem ipsum dolor sit amet, veri minim debet eam te. Affert dolorum at usu, at amet indoctum vim. Eum id tale facete. Cum id graece ignota, " +
			"sit at appellantur intellegebat, clita fastidii probatus ea qui. Sed no solum choro, mel id modus saperet inermis." +
			"Cum at labore temporibus. Elitr exerci accusata sed cu, his dolor congue ne. Eum propriae maluisset ad, simul dicunt voluptatibus id sit, " +
			"rebum offendit vulputate sea in. Mazim accusamus per at, ut nobis audire suscipit mea." +
			"His ne nostrud voluptatibus, eu primis copiosae sit. Ut numquam neglegentur instructior mei. Eos novum eligendi salutandi cu, vide laoreet " +
			"sententiae mea ea. Meliore deleniti in ius, dicat aeque deterruisset cum eu, nisl numquam per an. Eu clita labore consulatu quo. Quas numquam " +
			"philosophia nec te, pro te unum invenire scribentur, vix ne suas dicunt ceteros. Ei amet nibh elit duo, eam ut inermis mnesarchum. Usu ne nihil " +
			"omnesque apeirian. Mazim assueverit ius in. Nostrud docendi tractatos eu mei. In vim utroque iracundia, dictas scriptorem et mea.";

		public string Version {
			get;
			set;
		} = "Welcome to Medibio\r\n" +
			"Objective Digital\r\n" +
			"V1.0";

		public AboutViewModel ()
		{
		}

		private Command _BackCmd;
		public Command BackCmd
		{
			get
			{
				return _BackCmd ?? (_BackCmd = new Command(() =>
					{
						NavigationHandler.GlobalNavigator.Navigation.PopAsync();
					}));
			}
		}
	}
}

