﻿using System.Collections.Generic;

using Xamarin.Forms;

namespace Medibio.Modules.About
{
	public partial class AboutPage : ContentPage
	{
		public AboutPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			this.BindingContext = new AboutViewModel();
		}
	}
}

