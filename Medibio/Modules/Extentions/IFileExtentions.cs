﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.FileHelper;
using PCLStorage;
using Xamarin.Forms;

namespace Medibio.Extentions
{
    public static class IFileExtentions
    {
        public static async Task WriteAsync(this IFile file, string text)
        {
            IFileHelper fileHelper = DependencyService.Get<IFileHelper>();
            await fileHelper.WriteAsync(file.Path, text);
        }
    }
}


