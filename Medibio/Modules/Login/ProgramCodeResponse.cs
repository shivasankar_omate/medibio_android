﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medibio.APIHandler
{
    public class Partner
    {
        [JsonProperty (PropertyName ="name",NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "url", NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }
    }

    public class Group
    {
        [JsonProperty(PropertyName = "name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }
    }

    public class ProgramCodeResponse
    {
        [JsonProperty(PropertyName = "valid", NullValueHandling = NullValueHandling.Ignore)]
        public bool IsValid { get; set; }
        [JsonProperty(PropertyName = "partner", NullValueHandling = NullValueHandling.Ignore)]
        public Partner PartnerInfo { get; set; }
        [JsonProperty(PropertyName = "group", NullValueHandling = NullValueHandling.Ignore)]
        public Group GroupInfo { get; set; }
        [JsonProperty(PropertyName = "email_in_use", NullValueHandling = NullValueHandling.Ignore)]
        public bool EmailInUse { get; set; }
    }
}
