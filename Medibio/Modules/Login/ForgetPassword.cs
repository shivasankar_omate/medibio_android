﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Medibio.Modules.Login
{
    public class ForgetPasswordResponse
    {
        [JsonProperty(PropertyName = "email_sent", NullValueHandling = NullValueHandling.Ignore)]
        public bool EmailSent { get; set; }
    }
}
