﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.S3.Model;
using Medibio.APIHandler;
using Medibio.Extentions;
using Medibio.Modules.APIHandler;
using PCLStorage;
using Plugin.DeviceInfo;
using Xamarin.Forms;
using Medibio.Modules.Common;

namespace Medibio.Modules.LogHandler
{
    public class Logger
    {
        private IFile _CurrentLogFile;

        public readonly static string DefaultDeviceId = AppConstants.TEMP_DEVICE_ID;

        private static Logger _Handler;
        public static Logger Handler
        {
            get
            {
                return _Handler ?? (_Handler = new Logger()); 
            }
        }

        private Logger()
        {

        }

        public async Task WriteLog(string log)
        {
            if (_CurrentLogFile == null)
                await CreateNewLogFile();
			_CurrentLogFile.WriteAsync(DateTime.Now.ToString(DateFormats.TimestampFormat) + ": " + log + Environment.NewLine);
            /*lock (_CurrentLogFile)
                
            {
                _CurrentLogFile.WriteAsync(DateTime.Now.ToString(DateFormats.TimestampFormat)+ ": " + log +  Environment.NewLine);
            }*/
        }

        public async Task CreateNewLogFile()
        {
            try
            {
                IFolder rootFolder = FileSystem.Current.LocalStorage;
                IFolder folder = await rootFolder.CreateFolderAsync("MedibioLogs",
                    CreationCollisionOption.OpenIfExists);
                string timeStamp;
                timeStamp = DateTime.Now.ToString(DateFormats.FilenameTimestampFormat);
                //var deviceId = CrossDeviceInfo.Current.Id.Replace("=", "");
                string deviceId;
                if(TargetPlatform.Android == Device.OS)
                {
                    deviceId = CrossDeviceInfo.Current.Id;
                }
                else
                {
                    deviceId = Logger.DefaultDeviceId;
                }
                // + CrossDeviceInfo.Current.Id + "_"
                _CurrentLogFile = await folder.CreateFileAsync("MedibioScanLog_" + deviceId + "_" + timeStamp + ".log",
                    CreationCollisionOption.OpenIfExists);
                Debug.WriteLine("testing");
            }
            catch(Exception e)
            {
                Debug.WriteLine("exception on create file" + e.Message+">>>>"+e.StackTrace);
            }
            
        }

        public async Task UploadLogFiles(Action successCallback, Action ErrorCallback)
        {
            bool IsSuccess = true;
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder folder = await rootFolder.CreateFolderAsync("MedibioLogs",
                CreationCollisionOption.OpenIfExists);
            IList<IFile> files = await folder.GetFilesAsync();
            /*
            if(files != null )
            {
                foreach (IFile file in files)
                {
                    Debug.WriteLine(await file.ReadAllTextAsync());
                    PutObjectRequest request = new PutObjectRequest();
                    request.BucketName = AppEnvironment.LogBucket;
                    request.FilePath = file.Path;
                    if(TargetPlatform.Android == Device.OS)
                    {
                        request.Key = file.Name;
                    }
                    else
                    {
                        request.Key = file.Name.Replace(Logger.DefaultDeviceId, CrossDeviceInfo.Current.Id);
                    }
                    
                    request.ContentType = "text/plain";

                    await S3Handler.UploadFile(request, AppEnvironment.DataRegion, 
                        () =>
                        {
                            Debug.WriteLine("File upload success :" + file.Name);
                        },
                        () =>
                        {
                            Debug.WriteLine("File upload failed :" + file.Name);
                            IsSuccess = false;
                        }
                     );
                }
            }
            */

            folder = await rootFolder.CreateFolderAsync("MedibioScanLogs",
               CreationCollisionOption.OpenIfExists);
            files = await folder.GetFilesAsync();
            if (files != null)
            {
                foreach (IFile file in files)
                {
                    Debug.WriteLine(await file.ReadAllTextAsync());
                    PutObjectRequest request = new PutObjectRequest();
                    //request.BucketName = AppEnvironment.DataBucket;
                    request.BucketName = "medibio-dev";
                    request.FilePath = file.Path;
                    if (TargetPlatform.Android == Device.OS)
                    {
                        request.Key = file.Name;
                    }
                    else
                    {
                        request.Key = file.Name.Replace(Logger.DefaultDeviceId, CrossDeviceInfo.Current.Id);
                    }
                    request.ContentType = "text/plain";

                    await S3Handler.UploadFile(request, AppEnvironment.DataRegion,
                        () =>
                        {
                            Debug.WriteLine("File upload success :" + file.Name);
                        },
                        () =>
                        {
                            Debug.WriteLine("File upload failed :" + file.Name);
                            IsSuccess = false;
                        }
                     );
                }
            }

            if (IsSuccess)
            {
                successCallback?.Invoke();
            }else
            {
                ErrorCallback?.Invoke();
            }

        }

        public async Task DeleteLogFiles()
        {
            
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder folder = await rootFolder.CreateFolderAsync("MedibioLogs",
                CreationCollisionOption.OpenIfExists);
            IList<IFile> files = await folder.GetFilesAsync();
            if (files != null)
            {
                foreach (IFile file in files)
                {
                    file.DeleteAsync();
                }
            }

            folder = await rootFolder.CreateFolderAsync("MedibioScanLogs",
               CreationCollisionOption.OpenIfExists);
            files = await folder.GetFilesAsync();
            if (files != null)
            {
                foreach (IFile file in files)
                {
                    file.DeleteAsync();
                }
            }
            
        }
    }
}
