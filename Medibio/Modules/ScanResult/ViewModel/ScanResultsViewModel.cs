﻿using Medibio.APIHandler;
using Medibio.Modules.Common;
using Medibio.Modules.Result.Model;
using Medibio.Modules.Result.View;
using Plugin.Connectivity;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Medibio.Modules.Scan.Custom.ViewModel;

namespace Medibio.Modules.Result.ViewModel
{
	class ScanResultsViewModel : ViewModelBase
	{
		bool isBusy;
		public bool IsBusy
		{
			set
			{
				SetProperty("IsBusy", ref this.isBusy, value);
			}
			get
			{
				return isBusy;
			}
		}

		private ObservableCollection<ScanResultItemViewModel> scanResults;
		public ObservableCollection<ScanResultItemViewModel> ScanResults
		{
			set
			{
				SetProperty("ScanResults", ref this.scanResults, value);
			}
			get
			{
				return scanResults;
			}
		}

		ScanResultItemViewModel selectedResult;
		public ScanResultItemViewModel SelectedResult
		{
			set
			{
				//todo:- Refactor how this works, it looks wrong
				selectedResult = value;
				OnPropertyChanged("SelectedResult");
				if (null != selectedResult)
				{
					OnResultSelected();
				}
			}
			get
			{
				return selectedResult;
			}
		}

		Command menuCommand;
		public Command MenuCommand
		{
			get
			{
				return menuCommand ?? (menuCommand = new Command(
					() =>
					{
						//TODO Show and hide slider.
					}
					));
			}
		}


		public ScanResultsViewModel(List<ScanResult> results)
		{
			IsBusy = true;
			ScanResults = new ObservableCollection<ScanResultItemViewModel>();
			//GetScanResults();
			GetViewModelCollection(results);
		}

		private async void GetScanResults()
		{
			if (CrossConnectivity.Current.IsConnected)
			{
				ScanHandler.GetScanResults(0, 30,
				(response) =>
				{
					GetViewModelCollection(response.Scans);
				},
				(errorResponse) =>
				{
					IsBusy = false;
					NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, AppConstants.CANT_GET_DATA, AppConstants.OK_TEXT);
				});
			}
			else
			{
				IsBusy = false;
				NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, AppConstants.NETWORK_ERROR, AppConstants.OK_TEXT);
			}

		}

		private async void GetViewModelCollection(List<ScanResult> results)
		{
			ObservableCollection<ScanResultItemViewModel> temp = new ObservableCollection<ScanResultItemViewModel>();
			foreach (var result in results)
			{
				ScanResultItemViewModel scanVM = new ScanResultItemViewModel(result);
				temp.Add(scanVM);
			}

			ScanResults = temp;
			IsBusy = false;
		}

		private void OnResultSelected()
		{
			ScanResultDetailViewModel viewModel = new ScanResultDetailViewModel(SelectedResult.ScanData);
			ContentPage page = null;
			if (ScanResultStatus.Mild == SelectedResult.ScanData.Result)
			{
				page = new MildResultPage();
			}
			else if (ScanResultStatus.Moderate == SelectedResult.ScanData.Result)
			{
				page = new ModerateResultPage();
			}
			else if (ScanResultStatus.Incomplete == SelectedResult.ScanData.Result)
			{
				page = new InCompleteResultPage();
			}
			else if (ScanResultStatus.Test == SelectedResult.ScanData.Result)
			{
				page = new TestScanResultPage();
			}
			else if (ScanResultStatus.Severe == SelectedResult.ScanData.Result)
			{
				page = new SeriousResultPage();
			}

			if (null != page)
				NavigationHandler.GlobalNavigator.Navigation.PushAsync(page);

			page.BindingContext = viewModel;

			SelectedResult = null;
		}
	}
}
