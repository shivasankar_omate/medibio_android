﻿using System;
using System.Linq;
using Medibio.APIHandler;
using Xamarin.Forms;
using Medibio.Modules.Result.Model;
using System.Globalization;
using Medibio.Modules.Scan.Custom.ViewModel;

namespace Medibio.Modules.Result.ViewModel
{

	public class ScanResultItemViewModel : ViewModelBase
	{
		static string InComplete = "InComplete";
		static string Test = "Test OK";
		static string Mild = "Mild";
		static string Moderate = "Moderate";
		static string Serious = "Serious";

		Medibio.APIHandler.ScanResult scanResult;

		public Medibio.APIHandler.ScanResult ScanData
		{
			get
			{
				return scanResult;
			}
		}

		public string DurationText
		{
			get
			{
				//TODO
				if (null != ScanData.Duration)
				{
					var durationPart = ScanData.Duration.Split(':');
					if (durationPart.Count() == 2)
					{
						return string.Format("{0} hrs {1} minutes", durationPart[0], durationPart[1]);
					}
				}

				return "--";
			}
		}

		DateTime scanTimeStamp;
		public string DateTimeText
		{
			get
			{
				if (DateTime.TryParseExact(ScanData.TimeStamp, DateFormats.DateTimeParseFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out scanTimeStamp))
				{
					return (null == ScanData) ? string.Empty : scanTimeStamp.ToString(DateFormats.ScanResultDateFormat);
				}
				else
				{
					return "--";
				}

			}
		}

		public string Result
		{
			get
			{
				if (ScanResultStatus.Test == ScanData.Result)
				{
					return Test;
				}
				else if (ScanResultStatus.Mild == ScanData.Result)
				{
					return Mild;
				}
				else if (ScanResultStatus.Moderate == ScanData.Result)
				{
					return Moderate;
				}
				else if (ScanResultStatus.Severe == ScanData.Result)
				{
					return Serious;
				}
				else if (ScanResultStatus.Incomplete == ScanData.Result)
				{
					return InComplete;
				}
				else
				{
					return string.Empty;
				}
			}
		}

		public Color ResultColor
		{
			get
			{
				return GetResultColor();
			}
		}

		public ScanResultItemViewModel(ScanResult result)
		{
			scanResult = result;
		}

		private Color GetResultColor()
		{
			if (ScanResultStatus.Test == ScanData.Result)
			{
				return ScanResultColor.TEST_SCAN;
			}
			else if (ScanResultStatus.Mild == ScanData.Result)
			{
				return ScanResultColor.MILD;
			}
			else if (ScanResultStatus.Moderate == ScanData.Result)
			{
				return ScanResultColor.MODERATE;
			}
			else if (ScanResultStatus.Severe == ScanData.Result)
			{
				return ScanResultColor.SERIOUS;
			}
			else if (ScanResultStatus.Incomplete == ScanData.Result)
			{
				return ScanResultColor.InComplete;
			}
			else
			{
				return default(Color);
			}

		}
	}

	public static class ScanResultColor
	{
		public static readonly Color InComplete = Color.FromHex("#999999");
		public static readonly Color TEST_SCAN = Color.FromHex("#9d3467");
		public static readonly Color MODERATE = Color.FromHex("#dc7844");
		public static readonly Color MILD = Color.FromHex("#94c04a");
		public static readonly Color SERIOUS = Color.FromHex("#c62d2d");
	}
}
