﻿using Medibio.APIHandler;
using Medibio.Modules.Common;
using Medibio.Modules.Result.Model;
using Plugin.Connectivity;
using System;
using System.Globalization;
using System.Linq;
using Xamarin.Forms;
using Medibio.Modules.Scan.Custom.ViewModel;

namespace Medibio.Modules.Result.ViewModel
{
	public class ScanResultDetailViewModel : ViewModelBase
	{
		static string InComplete = "InComplete";
		static string Test = "Test OK";
		static string Mild = "Mild";
		static string Moderate = "Moderate";
		static string Serious = "Serious";

		ScanDetailedResult scanDetailResult;

		bool isBusy;
		public bool IsBusy
		{
			get
			{
				return isBusy;
			}
			set
			{
				SetProperty("IsBusy", ref this.isBusy, value);
			}
		}


		public string DurationText
		{
			get
			{
				//TODO
				if (null != scanDetailResult.Duration)
				{
					var durationPart = scanDetailResult.Duration.Split(':');
					if (durationPart.Count() == 2)
					{
						return string.Format("{0} hrs {1} minutes", durationPart[0], durationPart[1]);
					}
				}

				return "--";
			}
		}

		DateTime scanTimeStamp;
		public string DateTimeText
		{
			get
			{
				if (DateTime.TryParseExact(scanDetailResult.TimeStamp, DateFormats.DateTimeParseFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out scanTimeStamp))
				{
					return (null == scanDetailResult) ? string.Empty : string.Format("{0} {1}", "Scanned on", scanTimeStamp.ToString(DateFormats.ScanResultDateFormat));
				}
				else
				{
					return "--";
				}

			}
		}

		public string Result
		{
			get
			{
				if (ScanResultStatus.Test == scanDetailResult.Result)
				{
					return Test;
				}
				else if (ScanResultStatus.Mild == scanDetailResult.Result)
				{
					return Mild;
				}
				else if (ScanResultStatus.Moderate == scanDetailResult.Result)
				{
					return Moderate;
				}
				else if (ScanResultStatus.Severe == scanDetailResult.Result)
				{
					return Serious;
				}
				else if (ScanResultStatus.Incomplete == scanDetailResult.Result)
				{
					return InComplete;
				}
				else
				{
					return string.Empty;
				}
			}
		}

		public Color ResultColor
		{
			get
			{
				return GetResultColor();
			}
		}

		public string AverageHeartRate
		{
			get
			{
				return string.Format("{0} (awake) {1} (asleep)", scanDetailResult.HrAwakeMean, scanDetailResult.HrSleepMean);
			}
		}

		Command backCommand;
		public Command BackCommand
		{
			get
			{
				return backCommand ?? (backCommand = new Command(
					() =>
					{
						NavigationHandler.GlobalNavigator.Navigation.PopAsync();
					}
					));
			}
		}

		Command moreInfoCommand;
		public Command MoreInfoCommand
		{
			get
			{
				return moreInfoCommand ?? (moreInfoCommand = new Command(
					() =>
					{
						GetMoreInfo();
					}
					));
			}
		}

		Command startNewScanCommand;
		public Command StartNewScanCommand
		{
			get
			{
				return startNewScanCommand ?? (startNewScanCommand = new Command(
					() =>
					{
						LaunchNewScan();
					}
					));
			}
		}

		public ScanResultDetailViewModel(ScanResult scanResult)
		{
			IsBusy = false;
			scanDetailResult = new ScanDetailedResult(scanResult);
			//GetScanDetail(scanResult.Id);
		}

		private async void GetScanDetail(string scanResultId)
		{
			if (CrossConnectivity.Current.IsConnected)
			{
				await ScanHandler.GetDetailedScanResult(scanResultId,
					(response) =>
					{
						scanDetailResult = response;
						RaiseProperties();
						IsBusy = false;
					},
					(errorResponse) =>
					{
						NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, AppConstants.CANT_GET_DATA, AppConstants.OK_TEXT);
						IsBusy = false;
					}
				);
			}
			else
			{
				IsBusy = false;
				NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, AppConstants.NETWORK_ERROR, AppConstants.OK_TEXT);
			}

		}

		private void LaunchNewScan()
		{
			//TODO Launch new scan from here

		}

		private void GetMoreInfo()
		{
			//TODO get more info
		}

		private Color GetResultColor()
		{
			if (ScanResultStatus.Incomplete == scanDetailResult.Result)
			{
				return ScanResultColor.InComplete;
			}
			else if (ScanResultStatus.Mild == scanDetailResult.Result)
			{
				return ScanResultColor.MILD;
			}
			else if (ScanResultStatus.Moderate == scanDetailResult.Result)
			{
				return ScanResultColor.MODERATE;
			}
			else if (ScanResultStatus.Severe == scanDetailResult.Result)
			{
				return ScanResultColor.SERIOUS;
			}
			else if (ScanResultStatus.Test == scanDetailResult.Result)
			{
				return ScanResultColor.TEST_SCAN;
			}
			else
			{
				return default(Color);
			}

		}

		private void RaiseProperties()
		{
			OnPropertyChanged("DurationText");
			OnPropertyChanged("DateTimeText");
			OnPropertyChanged("Result");
			OnPropertyChanged("ResultColor");
			OnPropertyChanged("HrSleepMean");
			OnPropertyChanged("HrAwakeMean");
			OnPropertyChanged("HrMean");
		}
	}
}
