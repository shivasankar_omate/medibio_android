﻿using Medibio.APIHandler;
using Medibio.Modules.Result.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Medibio.Modules.Result.View
{
    public partial class ScanResultsPage : ContentPage
    {
        public ScanResultsPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            //BindingContext = new ScanResultsViewModel(TestData());
            //scanResults.ItemsSource = new string[]{"tamil" ,"sela","fsdfjasoij","adfh","kldsajf","fasjf" };

        }

        private List<ScanResult> TestData()
        {
            List<ScanResult> results = new List<ScanResult>();

            results.Add(new ScanResult {
                TimeStamp = "Jan 31, 2016, 7:09 PM",
                Duration = "12 hours, 29 minutes",
                Result = "mild",
                Reviewed = true,
                Viewed = true
            });

            results.Add(new ScanResult
            {
                TimeStamp = "Jan 26, 2016, 9:29 PM",
                Duration = "12 hours, 19 minutes",
                Result = "moderate",
                Reviewed = true,
                Viewed = true
            });

            results.Add(new ScanResult
            {
                TimeStamp = "Jan 22, 2016, 7:09 PM",
                Duration = "12 hours, 12 minutes",
                Result = "serious",
                Reviewed = true,
                Viewed = true
            });

            results.Add(new ScanResult
            {
                TimeStamp = "Jan 9, 2016, 9:09 PM",
                Duration = "12 hours, 12 minutes",
                Result = "moderate",
                Reviewed = true,
                Viewed = true
            });

            results.Add(new ScanResult
            {
                TimeStamp = "Jan 20, 2016, 10:03 PM",
                Duration = "Jan 9, 2016, 9:09 PM",
                Result = "mild",
                Reviewed = true,
                Viewed = true
            });

            results.Add(new ScanResult
            {
                TimeStamp = "Jan 9, 2016, 9:09 PM",
                Duration = "12 hours, 12 minutes",
                Result = "moderate",
                Reviewed = true,
                Viewed = true
            });

            results.Add(new ScanResult
            {
                TimeStamp = "Jan 9, 2016, 9:09 PM",
                Duration = "12 hours, 12 minutes",
                Result = "serious",
                Reviewed = true,
                Viewed = true
            });

            return results;
        }

    }
}
