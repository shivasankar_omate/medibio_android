﻿using Medibio.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Medibio.Modules.Result.View
{
	public partial class TestScanResultPage : ContentPage
	{
		public TestScanResultPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			GradientBlock.Effects.Add(new GradientEffect()
			{
				BorderRadious = 0,
				Colors = new Color[] { Color.FromHex("#cd833f"), Color.FromHex("#9d3467") },
				StartPoints = new float[] { 0f, 1f },
				Orientation = GradientOrientation.Vertical
			});
		}
	}
}
