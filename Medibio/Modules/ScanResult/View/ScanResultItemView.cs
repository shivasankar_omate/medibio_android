﻿
using System;
using Xamarin.Forms;

namespace Medibio.Result.View
{
    public class ScanListItemView : ViewCell
    {
        public BoxView indicatorBoxView;
        public Label dateTimeLabel;
        public Label durationLabel;
        public Button shortResultBtn;

        public ScanListItemView()
        {
            CreateView();
        }

        public static readonly BindableProperty DateTimeTextProperty = BindableProperty.Create(
            "DateTimeText", 
            typeof(string),
            typeof(ScanListItemView),
            string.Empty, 
            BindingMode.TwoWay,
            propertyChanged: ( view,  oldValue,  newValue) => 
            {
                if (null == newValue)
                    return;

                ((ScanListItemView)view).dateTimeLabel.Text = newValue.ToString();
            });

        public static readonly BindableProperty ResultProperty = BindableProperty.Create(
           "Result",
           typeof(string),
           typeof(ScanListItemView),
           string.Empty,
           BindingMode.TwoWay,
           propertyChanged: (view, oldValue, newValue) =>
           {
               if (null == newValue)
                   return;

               ((ScanListItemView)view).shortResultBtn.Text = newValue.ToString();
           });

        public static readonly BindableProperty ResultColorProperty = BindableProperty.Create(
            "ResultColor",
            typeof(Color),
            typeof(ScanListItemView),
            default(Color),
            BindingMode.TwoWay,
            propertyChanged: (view, oldValue, newValue) =>
            {
                if (null == newValue)
                    return;

                ((ScanListItemView)view).indicatorBoxView.BackgroundColor = (Color)newValue;
                ((ScanListItemView)view).shortResultBtn.BackgroundColor = (Color)newValue;
            });

        public static readonly BindableProperty DurationTextProperty = BindableProperty.Create(
            "DurationText",
            typeof(string),
            typeof(ScanListItemView),
            string.Empty,
            BindingMode.TwoWay,
            propertyChanged: (view, oldValue, newValue) => 
            {
                if (null == newValue)
                    return;

                ((ScanListItemView)view).durationLabel.Text = newValue.ToString();
            });

        private void CreateView()
        {
            indicatorBoxView = new BoxView()
            {
                WidthRequest = 3
            };

            indicatorBoxView.BackgroundColor = Color.FromHex("#94C04A");

            dateTimeLabel = new Label()
            {
                Text = "Jan 31, 2016, 7:09 PM",
                FontSize = 15,
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.FromHex("#E69D3467")
            };

            Image timerImage = new Image()
            {
                HeightRequest = 11,
                WidthRequest = 12,
                Aspect = Aspect.AspectFit
            };

            durationLabel = new Label()
            {
                Text = "12 hours, 29 minutes",
                FontSize = 12,
                TextColor = Color.FromHex("#E6353535")
            };

            StackLayout durationLayout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal
            };
            durationLayout.Children.Add(timerImage);
            durationLayout.Children.Add(durationLabel);

            StackLayout TextLayout = new StackLayout()
            {
                Padding = new Thickness(10, 0, 0, 0),
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Center
            };

            TextLayout.Children.Add(dateTimeLabel);
            TextLayout.Children.Add(durationLayout);

            shortResultBtn = new Button()
            {
                BackgroundColor = Color.FromHex("#94C04A"),
                HeightRequest = 15,
                WidthRequest = 70,
                FontSize = 11,
                TextColor = Color.White,
                FontAttributes = FontAttributes.Bold,
                VerticalOptions = LayoutOptions.Center,
                BorderRadius = 3
            };

            Image viewMoreImage = new Image()
            {
                HeightRequest = 19,
                WidthRequest = 10,
                Aspect = Aspect.AspectFit,
                HorizontalOptions = LayoutOptions.End,

            };

            StackLayout buttonStack = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Spacing = 19,
                Padding = new Thickness(0, 0, 19, 0)
            };

            buttonStack.Children.Add(shortResultBtn);
            buttonStack.Children.Add(viewMoreImage);

            if (TargetPlatform.Android == Device.OS)
            {
                timerImage.Source = "timer.png";
                viewMoreImage.Source = "viewmore.png";
            }
            else
            {
                timerImage.Source = "Assets/timer.png";
                viewMoreImage.Source = "Assets/viewmore.png";
            }


            StackLayout viewContainter = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.Center,
                HeightRequest = 73,
                Spacing = 0,
                BackgroundColor = Color.White
            };

            viewContainter.Children.Add(indicatorBoxView);
            viewContainter.Children.Add(TextLayout);
            viewContainter.Children.Add(buttonStack);

            this.View = viewContainter;

        }
    }
}
