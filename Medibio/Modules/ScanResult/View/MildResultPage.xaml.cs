﻿using Medibio.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Medibio.Modules.Result.View
{
	public partial class MildResultPage : ContentPage
	{
		public MildResultPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			GradientBlock.Effects.Add(new GradientEffect()
			{
				BorderRadious = 0,
				Colors = new Color[] { Color.FromHex("#94c04a"), Color.FromHex("#587b1c") },
				StartPoints = new float[] { 0f, 1f },
				Orientation = GradientOrientation.Vertical
			});
		}
	}
}
