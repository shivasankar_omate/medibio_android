﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medibio.Modules.Result.Model
{
    public static class ScanResultStatus
    {
        public static string Unknown = "unknown";
        public static string Test = "-4";
        public static string Short = "-3";
        public static string Incomplete = "-2";
        public static string Pending = "-1";
        public static string None = "1";
        public static string Slight = "2";
        public static string Mild = "3";
        public static string Moderate = "4";
        public static string Severe = "5";
        public static string VerySevere = "6";
    }
}
