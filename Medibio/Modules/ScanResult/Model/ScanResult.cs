﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Medibio.Modules.ScanResult.Model
{
    public class ScanResultResponse
    {
        [JsonProperty(PropertyName = "scans", NullValueHandling = NullValueHandling.Ignore)]
        public List<ScanResult> Scans{ get; set; }
    }

    public class ScanResult {
        [JsonProperty(PropertyName = "timestamp", NullValueHandling = NullValueHandling.Ignore)]
        public string TimeStamp{ get; set; }
        [JsonProperty(PropertyName = "result", NullValueHandling = NullValueHandling.Ignore)]
        public string Result { get; set; }
        [JsonProperty(PropertyName = "duration", NullValueHandling = NullValueHandling.Ignore)]
        public string Duration { get; set; }
        [JsonProperty(PropertyName = "viewed", NullValueHandling = NullValueHandling.Ignore)]
        public bool Viewed { get; set; }
        [JsonProperty(PropertyName = "reviewed", NullValueHandling = NullValueHandling.Ignore)]
        public bool Reviewed { get; set; }

    }

    public class ScanDetailedResult
    {
        [JsonProperty(PropertyName = "timestamp", NullValueHandling = NullValueHandling.Ignore)]
        public string TimeStamp { get; set; }
        [JsonProperty(PropertyName = "result", NullValueHandling = NullValueHandling.Ignore)]
        public string Result { get; set; }
        [JsonProperty(PropertyName = "duration", NullValueHandling = NullValueHandling.Ignore)]
        public string Duration { get; set; }
        [JsonProperty(PropertyName = "hr_sleep_mean", NullValueHandling = NullValueHandling.Ignore)]
        public string HrSleepMean { get; set; }
        [JsonProperty(PropertyName = "hr_awake_mean", NullValueHandling = NullValueHandling.Ignore)]
        public string HrAwakeMean { get; set; }
        [JsonProperty(PropertyName = "hr_mean", NullValueHandling = NullValueHandling.Ignore)]
        public string HrMean { get; set; }
        [JsonProperty(PropertyName = "device_info", NullValueHandling = NullValueHandling.Ignore)]
        public string DeviceInfo { get; set; }
        [JsonProperty(PropertyName = "viewed", NullValueHandling = NullValueHandling.Ignore)]
        public bool Viewed { get; set; }
        [JsonProperty(PropertyName = "reviewed", NullValueHandling = NullValueHandling.Ignore)]
        public bool Reviewed { get; set; }
    }
}
