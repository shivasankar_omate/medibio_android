﻿using System;

namespace Medibio.Modules.Model
{
	public sealed class MonitorDevice
	{
		private static MonitorDevice instance = null;
		private static readonly object padlock = new object();

		private MonitorDevice() {

		}

		public static MonitorDevice Instance
		{
			get
			{
				lock (padlock)
				{
					if (instance == null)
					{
						instance = new MonitorDevice();
					}
					return instance;
				}
			}
		}

		public string manufacturer = "";
		public string modelNumber = "";
		public string serialNumber = "";
		public string hardwareRevision = "";
		public string firmwareVersion = "";
		public string firmwareDate = "";
		public string deviceDescription = "";
		public string deviceVersion = "";
        public string ibiResolutionVersion = "1.4.11.102";

        
		
		
		public string contact = "";

		public bool isScanning = false;

		public bool isConnected = false;

	}
}

