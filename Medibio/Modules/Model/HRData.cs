﻿using System;
using System.Collections.Generic;

namespace Medibio.Modules.Model
{
	public class HRData
	{
		private DateTime Timestamp;
		public int SequenceNo;
		public int Contact;
		public int Bpm;
		public List<int> RrIntervals = new List<int>();
		public double Activity;
		public double Posture;
        public double PeakActivity;
		public List<int> EcgData = new List<int>();
        public int BatteryPercent;
        public string Temperature = "0.0";
        public string Location = "{-;-}";
        public string PhoneAcceleration = "{0.0,0.0,0.0}";
        public string Respiration = "0.0";
        public string Hrv = "0.00";

        public HRData ()
		{
			Timestamp = DateTime.Now;
		}

		public static string headerString(){
			return string.Format (@"{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}", "Timestamp", "Contact", "IBI(s)", "Heartrate(bpm)", "Activity(G)","Respiration(/min)", "Temperature(degC)","Location","PhoneAccel","BattPercent(%)", "Posture(deg)", "HRV(ms)", "PeakActivity(G)");
		}
		public string toString(){
			return string.Format (@"{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}", this.timestampString(), this.Contact, this.rrAsString(), this.Bpm, this.Activity,this.Respiration, this.Temperature, this.Location, this.PhoneAcceleration,this.BatteryPercent, this.Posture, this.Hrv, this.PeakActivity);
		}
		public string hrString(){
			return string.Format (@"BPM: {0}  Cont: {1}   Bat: {2}", this.Bpm, this.Contact, this.BatteryPercent);
		}
		public string rrString(){
			return string.Format (@"RR: {0}", this.rrAsString());
		}
		public string activityString(){
			return string.Format (@"Activity: {0}", this.Activity);
		}
		public string postureString(){
			return string.Format (@"Posture: {0}", this.Posture);
		}
		public string toShortString(){
			return string.Format (@"BPM: {0}\nRR: {1}\nActivity: {2}\nPosture: {3}", this.Bpm, this.rrAsString(), this.Activity, this.Posture);
		}
		private string timestampString(){
			string fmt = "yyyy-MM-dd hh:mm:ss.fff";
			return this.Timestamp.ToString (fmt);
		}
		public string rrAsString(){
			string rrString = "{";
			int count = 0;

			foreach (int rr in this.RrIntervals) {
				if (count == 0) {
					rrString = string.Format (@"{0}{1}", rrString, rr);
				} else {
					rrString = string.Format (@"{0};{1}", rrString, rr);
				}
				count++;
			}
			rrString = string.Format (@"{0}{1}", rrString, "}");

			return rrString;
		}
		public string rrAsList(){
			string rrString = "";
			int count = 0;

			foreach (int rr in this.RrIntervals) {
				if (count == 0) {
					rrString = string.Format (@"{0}{1}", rrString, rr);
				} else {
					rrString = string.Format (@"{0}\n{1}", rrString, rr);
				}
				count++;
			}
			rrString = string.Format (@"{0}{1}", rrString, "");

			return rrString;
		}
		public string ecgAsString(){
			string ecgString = "{";
			int count = 0;

			foreach (int ecg in this.EcgData) {
				if (count == 0) {
					ecgString = string.Format (@"{0}{1}", ecgString, ecg);
				} else {
					ecgString = string.Format (@"{0};{1}", ecgString, ecg);
				}
				count++;
			}
			ecgString = string.Format (@"{0}{1}", ecgString, "}");

			return ecgString;
		}

        public HRData Clone()
        {
            /*
            public int sequenceNo = 0;
        public int Contact = 0;
        public int Bpm = 0;
        public List<int> RrIntervals = new List<int>();
        public double Activity = 0;
        public double Posture = 0;
        public List<int> EcgData = new List<int>();
        */
            HRData cloneObj = new HRData();
            cloneObj.Contact = this.Contact;
            cloneObj.Bpm = this.Bpm;
            cloneObj.RrIntervals = this.RrIntervals;
            cloneObj.Activity = this.Activity;
            cloneObj.Posture = this.Posture;
            cloneObj.EcgData = this.EcgData;
            cloneObj.PeakActivity = this.PeakActivity;
            cloneObj.Location = this.Location;
            cloneObj.Temperature = this.Temperature;
            cloneObj.PhoneAcceleration = this.PhoneAcceleration;
            cloneObj.BatteryPercent = this.BatteryPercent;
            cloneObj.Respiration = this.Respiration;
            cloneObj.Hrv = this.Hrv;

            return cloneObj;

        }
	}
}

