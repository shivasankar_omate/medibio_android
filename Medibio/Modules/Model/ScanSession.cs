﻿using System;
using System.Text;
using Medibio.Modules.Common;
using Plugin.DeviceInfo;
using Xamarin.Forms;

namespace Medibio.Modules.Model
{
	public sealed class ScanSession
	{
		private static ScanSession instance = null;
		private static readonly object padlock = new object();

		public bool isScanning = false;
		public bool isPaused = false;

		private ScanSession() {
			this.startTime = DateTime.Now;
			this.lostTime = new TimeSpan(0);
		}

		public static ScanSession Instance
		{
			get
			{
				lock (padlock)
				{
					if (instance == null)
					{
						instance = new ScanSession();
					}
					return instance;
				}
			}
		}

		DateTime startTime;
		DateTime endTime;
		TimeSpan lostTime = new TimeSpan(0);

		DateTime pauseStartTime;

		public int eventCount = 0;
		public int disconnectCount = 0;
		int rrErrorCount;
		int rrCount;

		static int hrMin = 30; // BPM
		static int hrMax = 200; // BPM

		public string rrErrorPercent {
			get {
				double error = ((double)rrErrorCount / (double)rrCount) * 100.0;

				return string.Format (@"{0:0.0}", error);
			}
		}
		public string rrErrorCountString {
			get {

				return string.Format (@"{0}/{1}", rrErrorCount, rrCount);
			}
		}
		TimeSpan elapsedTime {
			get {
				return DateTime.Now.Subtract (startTime);
			}
		}
		public string elapsedTimeString {
			get {
				TimeSpan time = this.elapsedTime;
				return string.Format (@"Time: {0:00}:{1:00}:{2:00}  Lost: {3:00}:{4:00}:{5:00} {6}", time.Hours, time.Minutes, time.Seconds,
					lostTime.Hours, lostTime.Minutes, lostTime.Seconds, disconnectCount);
			}
		}
		public bool Start(){
			if (this.isScanning) {
				return false;
			} else {
				this.Reset ();
				this.isScanning = true;
				return true;
			}
		}
		public bool Stop(){
			if (!this.isScanning) {
				return false;
			} else {
				this.isScanning = false;
				return true;
			}
		}
		public void Pause(){
			isPaused = true;
			this.pauseStartTime = DateTime.Now;
			disconnectCount++;
		}
		public void Resume(){
			if (isPaused) {
				TimeSpan time = DateTime.Now.Subtract (pauseStartTime);
				lostTime += time;
			}
			isPaused = false;
		}
		public void Reset(){
			rrErrorCount = 0;
			rrCount = 0;
			eventCount = 0;
			disconnectCount = 0;
			this.startTime = DateTime.Now;
		}

		/// <summary>
		/// Process new RR intervals to calculate statistics.
		/// </summary>
		/// <param name="rrInterval">Rr interval.</param>
		public void newRRInterval(int rrInterval){

			rrCount++;

			if (rrInterval < (hrMin * 1000 / 60) || rrInterval > (hrMax * 1000 / 60)) {
				rrErrorCount++;
			}

		}
		public string startTimeString{
			get {
				return startTime.ToString ("yyyy-MM-dd hh:mm:ss");
			}
		}
		public string WriteHeaderData()
		{
			StringBuilder _string = new StringBuilder();

			_string.Append(formatHeaderLine(ScanDataFields.AmazonID, ""));
			_string.Append(formatHeaderLine(ScanDataFields.UserID, ""));
			_string.Append(formatHeaderLine(ScanDataFields.PushNotificationToken, ""));
			_string.Append(formatHeaderLine(ScanDataFields.CaptureDevice, CrossDeviceInfo.Current.Platform.ToString()));

            if(Device.Idiom == TargetIdiom.Phone)
			    _string.Append(formatHeaderLine(ScanDataFields.CaptureDeviceType, "Phone"));
            else if(Device.Idiom == TargetIdiom.Tablet)
                _string.Append(formatHeaderLine(ScanDataFields.CaptureDeviceType, "Tablet"));

            if(Device.OS == TargetPlatform.Android)
                _string.Append(formatHeaderLine(ScanDataFields.CaptureDeviceOS, "Android"));
            else if(Device.OS == TargetPlatform.WinPhone)
                _string.Append(formatHeaderLine(ScanDataFields.CaptureDeviceOS, "Windows"));

            _string.Append(formatHeaderLine(ScanDataFields.CaptureDeviceOSVersion, CrossDeviceInfo.Current.VersionNumber.ToString()));
			_string.Append(formatHeaderLine(ScanDataFields.CaptureDeviceUUID, CrossDeviceInfo.Current.Id));
			_string.Append(formatHeaderLine(ScanDataFields.CaptureApplication, AppConstants.APP_NAME));
			_string.Append(formatHeaderLine(ScanDataFields.CaptureApplicationVersion, AppConstants.APP_VERSION));
			_string.Append(formatHeaderLine(ScanDataFields.MonitorManufacturer, MonitorDevice.Instance.manufacturer));
			_string.Append(formatHeaderLine(ScanDataFields.MonitorModel, MonitorDevice.Instance.modelNumber));
			_string.Append(formatHeaderLine(ScanDataFields.MonitorSerialNumber, MonitorDevice.Instance.serialNumber));
			_string.Append(formatHeaderLine(ScanDataFields.MonitorHardwareRevision, MonitorDevice.Instance.hardwareRevision));
			_string.Append(formatHeaderLine(ScanDataFields.MonitorFirmwareVersion, MonitorDevice.Instance.firmwareVersion));
			_string.Append(formatHeaderLine(ScanDataFields.MonitorFirmwareDate, MonitorDevice.Instance.firmwareDate));
			_string.Append(formatHeaderLine(ScanDataFields.MonitorIBIResolution, MonitorDevice.Instance.ibiResolutionVersion));
			_string.Append(formatHeaderLine(ScanDataFields.StartTime, startTimeString));
			_string.Append(formatLine(ScanDataFields.DATABEGIN));
			_string.Append(formatLine(HRData.headerString()));
			return _string.ToString();

		}
		public string WriteEndData()
		{
			StringBuilder _string = new StringBuilder();

			_string.Append(formatLine(ScanDataFields.DATAEND));

			return _string.ToString();

		}
		private string formatHeaderLine(string field, string value){
			return string.Format (@"{0}: {1}{2}", field, value, Environment.NewLine);
		}
		private string formatLine(string line){
			return string.Format (@"{0}{1}", line, Environment.NewLine);
		}
	}
}

