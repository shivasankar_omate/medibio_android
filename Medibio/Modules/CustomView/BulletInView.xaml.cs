﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Medibio.CustomView
{
    public partial class BulletInView : ContentView
    {
        public BulletInView()
        {
            InitializeComponent();
        }

        public string Text
        {
            get
            {
                return TextLbl.Text;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    TextLbl.Text = string.Empty;
                else
                    TextLbl.Text = value;
            }
        }

        public Color BolletColor
        {
            get
            {
                return BulletBtn.BackgroundColor;
            }
            set
            {
                BulletBtn.BackgroundColor = value;
            }
        }

        public Color TextColor
        {
            get
            {
                return TextLbl.TextColor;
            }
            set
            {
                TextLbl.TextColor = value;
            }
        }
    }
}
