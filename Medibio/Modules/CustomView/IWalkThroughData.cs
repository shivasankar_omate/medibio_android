﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Medibio.CustomView
{
    public interface IWalkThroughData
    {
        ImageSource Source { get; set; }
        string Heading { get; set; }
        string Description { get; set; }
    }
}
