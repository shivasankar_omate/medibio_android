﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Medibio.CustomView
{
    public class PagerIndicatorDots : StackLayout
    {
        int dotCount = 1;
        int _selectedIndex;

        public Color DotColor { get; set; }
        public Color SelectedDotColor { get; set; }
        public double DotSize { get; set; }

        public PagerIndicatorDots()
        {
            HorizontalOptions = LayoutOptions.CenterAndExpand;
            VerticalOptions = LayoutOptions.Center;
            Orientation = StackOrientation.Horizontal;
            DotColor = Color.FromHex("#8e8e8e");
            DotSize = 6d;
            Spacing = 10;
            if (Device.OS == TargetPlatform.WinPhone)
            {
                DotSize = 12d;
                Spacing = 20;
                HeightRequest = 20;
                WidthRequest = 200;
                BackgroundColor = Color.Aqua;
                HorizontalOptions = LayoutOptions.FillAndExpand;

            }            
        }

        void CreateDot()
        {
            //Make one button and add it to the dotLayout
            var dot = new Button
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                BorderRadius = Convert.ToInt32(DotSize / 2),
                HeightRequest = DotSize,
                WidthRequest = DotSize,
                BackgroundColor = DotColor
            };
            Children.Add(dot);
        }

        public static BindableProperty ItemsSourceProperty =
            BindableProperty.Create<PagerIndicatorDots, IList>(
                pi => pi.ItemsSource,
                null,
                BindingMode.TwoWay,
                propertyChanging: (bindable, oldValue, newValue) => {
                    ((PagerIndicatorDots)bindable).ItemsSourceChanging();
                },
                propertyChanged: (bindable, oldValue, newValue) => {
                    ((PagerIndicatorDots)bindable).ItemsSourceChanged();
                }
            );

        public IList ItemsSource
        {
            get
            {
                return (IList)GetValue(ItemsSourceProperty);
            }
            set
            {
                SetValue(ItemsSourceProperty, value);
            }
        }

        public static BindableProperty SelectedItemProperty =
            BindableProperty.Create<PagerIndicatorDots, IWalkThroughData>(
                pi => pi.SelectedItem,
                null,
                BindingMode.OneWay,
                propertyChanged: (bindable, oldValue, newValue) => {
                    ((PagerIndicatorDots)bindable).SelectedItemChanged();
                });

        public IWalkThroughData SelectedItem
        {
            get
            {
                return (IWalkThroughData)GetValue(SelectedItemProperty);
            }
            set
            {
                SetValue(SelectedItemProperty, value);
            }
        }

        void ItemsSourceChanging()
        {
            if (ItemsSource != null)
                _selectedIndex = ItemsSource.IndexOf(SelectedItem);
        }

        void ItemsSourceChanged()
        {
            if (ItemsSource == null) return;

            // Dots *************************************
            var countDelta = ItemsSource.Count - Children.Count;

            if (countDelta > 0)
            {
                for (var i = 0; i < countDelta; i++)
                {
                    CreateDot();
                }
            }
            else if (countDelta < 0)
            {
                for (var i = 0; i < -countDelta; i++)
                {
                    Children.RemoveAt(0);
                }
            }
            //*******************************************
        }

        void SelectedItemChanged()
        {
            var selectedIndex = ItemsSource.IndexOf(SelectedItem);
            var pagerIndicators = Children.Cast<Button>().ToList();

            foreach (var pi in pagerIndicators)
            {
                UnselectDot(pi);
            }

            if (selectedIndex > -1)
            {
                SelectDot(pagerIndicators[selectedIndex]);
            }
        }

        static void UnselectDot(Button dot)
        {
            dot.Opacity = 0.5;
            //dot.WidthRequest = 8;
            //Rectangle bound = dot.Bounds;
            //if(bound.Width == 14)//&& bound.Width != 0
            //{
            //    bound.X = bound.X + 4;
            //    bound.Width = 6;
            //}
            //bound.Height = 6;
            
            //dot.TranslateTo(bound.X, bound.Y, 100, Easing.BounceIn);
            //dot.LayoutTo(bound, 100, Easing.BounceIn);
            dot.BackgroundColor = Color.FromHex("#8e8e8e");
            
        }

        static void SelectDot(Button dot)
        {
            
            dot.Opacity = 1.0;
            //dot.WidthRequest = 15;
            
            //Rectangle bound = dot.Bounds;
            //bound.Width = 14;
            //bound.Height = 6;
            //bound.X = bound.X-4;

            //dot.LayoutTo(bound, 100, Easing.BounceOut);
            //dot.TranslationX = bound.X;
            
            dot.BackgroundColor = Color.FromHex("#9d3467");
            
        }
    }
}
