﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Medibio.CustomView
{
	public partial class ImageTextView : ContentView
	{

		public ImageTextView()
		{
			InitializeComponent();

		}
		public static readonly BindableProperty SpacingProperty = BindableProperty.Create<ImageTextView,double> (ctrl => ctrl.Spacing, default(double),
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanging: (bindable, oldValue, newValue) => {
				var ctrl = (ImageTextView)bindable;
				ctrl.Spacing = newValue;
			});
		public double Spacing
		{
			set
			{
				SetValue (SpacingProperty,value);
				this.stack.Spacing = value;
			}
			get
			{
				return (double)GetValue (SpacingProperty);
			}
		}

//		public static readonly BindableProperty ThicknessProperty = BindableProperty.Create<ImageTextView,Thickness> (ctrl => ctrl.Padding, defaultValue: new Thickness(0),
//			defaultBindingMode: BindingMode.TwoWay,
//			propertyChanging: (bindable, oldValue, newValue) => {
//				var ctrl = (ImageTextView)bindable;
//				ctrl.Padding = newValue;
//			});
//
//		public Thickness Padding
//		{
//			set
//			{
//				SetValue (ThicknessProperty,value);
//				stack.Padding = value;
//			}
//			get
//			{
//				return (Thickness) GetValue (ThicknessProperty);
//			}
//		}

		public static BindableProperty SourceProperty = 
			BindableProperty.Create<ImageTextView, ImageSource>(ctrl => ctrl.Source,
				defaultValue: null,
				defaultBindingMode: BindingMode.TwoWay,
				propertyChanging: (bindable, oldValue, newValue) => {
					var ctrl = (ImageTextView)bindable;
					ctrl.Source = newValue;
				});

		public ImageSource Source {
			get { return (string)GetValue(SourceProperty); }
			set { 
				SetValue (SourceProperty, value);
				img.Source = value;
			}
		}


		public static readonly BindableProperty TextProperty = BindableProperty.Create<ImageTextView,string> (ctrl => ctrl.Text,
            defaultValue: string.Empty,
            defaultBindingMode: BindingMode.TwoWay,
            propertyChanged: (bindable, oldValue, newValue) => {
				var Ctrl = (ImageTextView)bindable;
				Ctrl.label.Text = newValue;
			});
		public string Text
		{
			set
			{
				SetValue (TextProperty, value);
			}
			get
			{
				return (string)GetValue (TextProperty);
			}
		}

		public static readonly BindableProperty TextColorProperty = BindableProperty.Create<ImageTextView,Color> (ctrl => ctrl.TextColor, Color.Default,
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanging: (bindable, oldValue, newValue) => {
				var ctrl = (ImageTextView)bindable;
				ctrl.TextColor = newValue;
			});
		public Color TextColor
		{
			set
			{
				SetValue (TextColorProperty,value);
				this.label.TextColor = value;
			}
			get
			{
				return (Color)GetValue (TextColorProperty);
			}
		}

		public static BindableProperty FontSizeProperty = 
			BindableProperty.Create<ImageTextView, double>(ctrl => ctrl.FontSize,
				defaultValue: default(double),
				defaultBindingMode: BindingMode.TwoWay,
				propertyChanged: (bindable, oldValue, newValue) => {
					var ctrl = (ImageTextView)bindable;
					ctrl.FontSize = newValue;
				});

		public double FontSize
		{
			set
			{
				SetValue (FontSizeProperty, value);
				label.FontSize = value;
			}
			get
			{
				return (double)GetValue (FontSizeProperty);
			}
		}

		public static readonly BindableProperty FontFamilyProperty = BindableProperty.Create<ImageTextView,string> (ctrl => ctrl.FontFamily,string.Empty,
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanging: (bindable, oldValue, newValue) => {
				var ctrl = (ImageTextView)bindable;
				ctrl.Source = newValue;
			});
		public string FontFamily
		{
			set
			{
				SetValue (FontFamilyProperty,value);
				label.FontFamily = value;
			}
			get
			{
				return (string)GetValue (FontFamilyProperty);
			}
		}

		public static readonly BindableProperty IsSelectedProperty = BindableProperty.Create<ImageTextView,bool> (ctrl => ctrl.IsSelected, false,
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanging: (bindable, oldValue, newValue) => {
				//var ctrl = (ImageTextView)bindable;
				//ctrl.IsSelected = newValue;
			});
		public bool IsSelected
		{
			set
			{
				SetValue (IsSelectedProperty,value);
			}
			get
			{
				return (bool)GetValue (IsSelectedProperty);
			}
		}

		public static readonly BindableProperty ImageHeightWidthProperty = BindableProperty.Create<ImageTextView,double> (ctrl => ctrl.ImageHeightWidth, 20d,
			defaultBindingMode: BindingMode.TwoWay,
			propertyChanged: (bindable, oldValue, newValue) => {
				var ctrl = (ImageTextView)bindable;
//				ctrl.ImageHeightWidth = newValue;
				ctrl.img.WidthRequest = ctrl.img.HeightRequest = ctrl.ImageHeightWidth;
			});
		public double ImageHeightWidth
		{
			set
			{
				SetValue (ImageHeightWidthProperty,value);
			}
			get
			{
				return (double)GetValue (ImageHeightWidthProperty);
			}
		}

//		public static readonly BindableProperty BackgroundColorProperty = BindableProperty.Create<ImageTextView,Color> (ctrl => ctrl.stack.BackgroundColor, Color.Default,
//			defaultBindingMode: BindingMode.TwoWay,
//			propertyChanging: (bindable, oldValue, newValue) => {
//				var ctrl = (ImageTextView)bindable;
//				ctrl.TextColor = newValue;
//			});
//		
//		public new Color BackgroundColor
//		{
//			set
//			{
//				SetValue (BackgroundColorProperty,value);
//				this.stack.BackgroundColor = value;
//			}
//			get
//			{
//				return (Color)GetValue (BackgroundColorProperty);
//			}
//		}
	}
}

