﻿using System;

namespace Medibio.Modules.Model
{
    public class ScanDataFields
    {
        public static string AmazonID { get; } = "Amazon ID";

        public static string ApplicationArn { get; } = "Application Arn";

        public static string UserID { get; } = "User ID";

        public static string PushNotificationToken { get; } = "Push Notification Token";

        public static string CaptureDevice { get; } = "Capture Device";

        public static string CaptureDeviceType { get; } = "Capture Device Type";

        public static string CaptureDeviceOS { get; } = "Capture Device OS";

        public static string CaptureDeviceOSVersion { get; } = "Capture Device OS Version";

        public static string CaptureDeviceUUID { get; } = "Capture Device UUID";

        public static string CaptureApplication { get; } = "Capture Application";
        public static string CaptureApplicationVersion { get; } = "Capture Application Version";

        public static string MonitorManufacturer { get; } = "Monitor Manufacturer";

        public static string MonitorModel { get; } = "Monitor Model";

        public static string MonitorSerialNumber { get; } = "Monitor Serial Number";

        public static string MonitorHardwareRevision { get; } = "Monitor Hardware Revision";

        public static string MonitorFirmwareVersion { get; } = "Monitor Firmware Version";

        public static string MonitorFirmwareDate { get; } = "Monitor Firmware Date";

        public static string MonitorIBIResolution { get; } = "Monitor IBI Resolution";

        public static string StartTime { get; } = "Start Time";

        public static string DATABEGIN { get; } = "DATA BEGIN";

        public static string DATAEND { get; } = "END OF DATA";

        public static string QUESTIONARE_BEGIN { get; } = "QUESTIONARE BEGIN";
        public static string QUESTIONARE_END { get; } = "QUESTIONARE END";

        public ScanDataFields()
        {
        }
    }
}

