﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Medibio.Modules.Scan.Model
{

    public static class ScanEvent
    {
        public static readonly string CREATE = "create";
        public static readonly string COMPLETE = "complete";
        public static readonly string ERROR = "error";
        public static readonly string UPLOAD = "upload";
    }

    public class ScanDataModel
    {
        [JsonProperty(PropertyName = "event_type", NullValueHandling = NullValueHandling.Ignore)]
        public string EventType { get; set; }
        [JsonProperty(PropertyName = "params", NullValueHandling = NullValueHandling.Ignore)]
        public ScanParams Params { get; set; }

    }

    public class ScanParams
    {
        [JsonProperty(PropertyName = "filename", NullValueHandling = NullValueHandling.Ignore)]
        public string FileName { get; set; }
        [JsonProperty(PropertyName = "error", NullValueHandling = NullValueHandling.Ignore)]
        public string Error { get; set; }
    }

    public class ScanEventResponse
    {
        [JsonProperty(PropertyName = "scan", NullValueHandling = NullValueHandling.Ignore)]
        public ScanDetail ScanDetails { get; set; }
        [JsonProperty(PropertyName = "event", NullValueHandling = NullValueHandling.Ignore)]
        public EventDetail EventDetails { get; set; }
    }

    public class ScanDetail
    {
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "timestamp", NullValueHandling = NullValueHandling.Ignore)]
        public string Timestamp { get; set; }
    }

    public class EventDetail
    {
        [JsonProperty(PropertyName = "id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "created", NullValueHandling = NullValueHandling.Ignore)]
        public string Created { get; set; }
    }
}
