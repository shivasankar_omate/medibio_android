﻿using System;
using System.Collections.Generic;

namespace Medibio.Modules.Model
{
    public class HRData
    {
        private DateTime Timestamp;
        public int SequenceNo;
        public int Contact;
        public int Bpm;
        public List<int> RrIntervals = new List<int>();
        public double Activity;
        public double Posture;
        public double PeakActivity;
        public List<int> EcgData = new List<int>();
        public int BatteryPercent;
        public string Temperature = "0.0";
        public string Location = "{-;-}";
		public string PhoneAcceleration = "{0.0;0.0;0.0}";
        public string Respiration = "0.0";
        public string Hrv = "0.00";

        public HRData()
        {
            Timestamp = DateTime.Now;

			Activity = MonitorDevice.Instance.activity;
			PeakActivity = (int)MonitorDevice.Instance.peakActivity;
			BatteryPercent = (int)MonitorDevice.Instance.battery;
        }

        public static string HeaderString()
        {
            return string.Format(@"{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}", "Timestamp", "Contact", "IBI(s)", "Heartrate(bpm)", "Activity(G)", "Respiration(/min)", "Temperature(degC)", "Location", "PhoneAccel", "BattPercent(%)", "Posture(deg)", "HRV(ms)", "PeakActivity(G)");
        }
        public override string ToString()
        {
            return string.Format(@"{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}", this.TimestampString(), this.Contact, this.RrAsString(), this.Bpm, this.Activity, this.Respiration, this.Temperature, this.Location, this.PhoneAcceleration, this.BatteryPercent, this.Posture, this.Hrv, this.PeakActivity);
        }

        public string HrString()
        {
            return string.Format(@"BPM: {0}  Cont: {1}   Bat: {2}", this.Bpm, this.Contact, this.BatteryPercent);
        }

        public string RrString()
        {
            return string.Format(@"RR: {0}", this.RrAsString());
        }
        public string ActivityString()
        {
            return string.Format(@"Activity: {0}", this.Activity);
        }

        public string PostureString()
        {
            return string.Format(@"Posture: {0}", this.Posture);
        }

        public string ToShortString()
        {
            return string.Format(@"BPM: {0}\nRR: {1}\nActivity: {2}\nPosture: {3}", this.Bpm, this.RrAsString(), this.Activity, this.Posture);
        }

        private string TimestampString()
        {
			return this.Timestamp.ToString(DateFormats.TimestampFormat);
        }

        public string RrAsString()
        {
            string rrString = "{";
            int count = 0;

            foreach (int rr in this.RrIntervals)
            {
                if (count == 0)
                {
                    rrString = string.Format(@"{0}{1}", rrString, rr);
                }
                else
                {
                    rrString = string.Format(@"{0};{1}", rrString, rr);
                }
                count++;
            }
            rrString = string.Format(@"{0}{1}", rrString, "}");

            return rrString;
        }

        public string RrAsList()
        {
            string rrString = "";
            int count = 0;

            foreach (int rr in this.RrIntervals)
            {
                if (count == 0)
                {
                    rrString = string.Format(@"{0}{1}", rrString, rr);
                }
                else
                {
                    rrString = string.Format(@"{0}\n{1}", rrString, rr);
                }
                count++;
            }
            rrString = string.Format(@"{0}{1}", rrString, "");

            return rrString;
        }

        public string EcgAsString()
        {
            string ecgString = "{";
            int count = 0;

            foreach (int ecg in this.EcgData)
            {
                if (count == 0)
                {
                    ecgString = string.Format(@"{0}{1}", ecgString, ecg);
                }
                else
                {
                    ecgString = string.Format(@"{0};{1}", ecgString, ecg);
                }
                count++;
            }
            ecgString = string.Format(@"{0}{1}", ecgString, "}");

            return ecgString;
        }
        
    }
}

