﻿using System;

namespace Medibio.Modules.Model
{
    public sealed class MonitorDevice
    {
        private static MonitorDevice instance = null;
        private static readonly object padlock = new object();

        private MonitorDevice()
        {

        }

        public static MonitorDevice Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new MonitorDevice();
                    }
                    return instance;
                }
            }
        }

        public string manufacturer = string.Empty;
        public string modelNumber = string.Empty;
        public string serialNumber = string.Empty;
        public string hardwareRevision = string.Empty;
        public string firmwareVersion = string.Empty;
        public string firmwareDate = string.Empty;
        public string deviceDescription = string.Empty;
        public string deviceVersion = string.Empty;
        public string ibiResolutionVersion = "1.4.11.102";

		public double activity = 0.0;
		public double peakActivity = 0.0;
		public double battery = 0.0;

		public string ibiResolution {
			get {
				// This is a hack and needs to check the specific version
				if (modelNumber == "HxM2") {
					return "1000.0";
				} else if (modelNumber == "BioHarness 3") {

					// Check the version of the firmware and return the approriate value


					return "1024.0";
				} else {
					return "1024.0";
				}
			}
		}


        public string contact = string.Empty;

        public bool isScanning = false;

        public bool isConnected = false;

    }
}

