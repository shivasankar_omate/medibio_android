﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medibio.Modules.Scan.Model
{
    public enum ScanStatus { Created, Scaning, Completed, Uploading, Updated }
    public class Scan
    {
        public string Id;
        public string StartTime;
        public string ScanFileName;
        public ScanStatus Status;
        public long DurationInSeconds;
        public string ErrorMessage;
    }
}
