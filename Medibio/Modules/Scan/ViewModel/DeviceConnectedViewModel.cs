﻿using Medibio.Modules.Scan.Custom.ViewModel;
using Medibio.Modules.Scan.View;
using Xamarin.Forms;

namespace Medibio.Modules.Scan.ViewModel
{
	class DeviceConnectedViewModel : ViewModelBase
	{
		private bool isBusy;
		public bool IsBusy
		{
			set
			{
				SetProperty("IsBusy", ref this.isBusy, value);
			}
			get
			{
				return isBusy;
			}
		}

		private Command _BackCmd;
		public Command BackCmd
		{
			get
			{
				return _BackCmd ?? (_BackCmd = new Command(async () =>
				{
					await ScanManager.Manager.CompleteScan();
					NavigationHandler.GlobalNavigator.Navigation.PopAsync();
				}));
			}
		}

		private Command _StartScanCmd;
		public Command StartScanCmd
		{
			get
			{
				return _StartScanCmd ?? (_StartScanCmd = new Command(async () =>
				{
					if (IsBusy)
						return;

					IsBusy = true;
					ContentPage page = new ScanInProgressPage();
					page.BindingContext = new ScanInProgressViewModel();
					await NavigationHandler.GlobalNavigator.Navigation.PushAsync(page);
					IsBusy = false;

				}));
			}
		}


	}
}
