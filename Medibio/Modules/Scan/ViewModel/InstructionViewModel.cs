﻿using System.Collections.ObjectModel;
using Medibio.CustomView;
using Medibio.Modules.Scan.Custom.ViewModel;
using Medibio.Modules.Scan.View;
using Xamarin.Forms;

namespace Medibio.Modules.Scan.ViewModel
{
	public class InstructionViewModel : ViewModelBase
	{
		public bool IsBusy { get; set; }
		public InstructionViewModel()
		{
			ObservableCollection<IWalkThroughData> instructions = new ObservableCollection<IWalkThroughData>();
			instructions.Add(new InstructionSlideViewModel()
			{
				Heading = "Power up",
				Source = ImageSource.FromFile(Device.OnPlatform<string>("", "powerup.png", "Assets/powerup.png")),
				Description = "Change your phone"
			});
			instructions.Add(new InstructionSlideViewModel()
			{
				Heading = "When",
				Source = ImageSource.FromFile(Device.OnPlatform<string>("", "when.png", "Assets/when.png")),
				Description = "Start the scan 2 hours before your bet time and do not end the scan until 1 hour after your wake"
			});
			instructions.Add(new InstructionSlideViewModel()
			{
				Heading = "Fit",
				Source = ImageSource.FromFile(Device.OnPlatform<string>("", "fit.png", "Assets/fit.png")),
				Description = "Fit your device snugly around your chest"
			});
			instructions.Add(new InstructionSlideViewModel()
			{
				Heading = "Connect",
				Source = ImageSource.FromFile(Device.OnPlatform<string>("", "bt.png", "Assets/bt.png")),
				Description = "Phone Bluetooth setting: ON"
			});
			instructions.Add(new InstructionSlideViewModel()
			{
				Heading = "Do's and Don'ts",
				Source = ImageSource.FromFile(Device.OnPlatform<string>("", "do_donts.png", "Assets/do_donts.png")),
				Description = "Change your phone"
			});

			Instructions = instructions;
			CurrentInstruction = Instructions[0];
		}

		private ObservableCollection<IWalkThroughData> _Instructions;
		public ObservableCollection<IWalkThroughData> Instructions
		{
			set
			{
				SetProperty("Instructions", ref _Instructions, value);
			}
			get
			{
				return _Instructions;
			}
		}

		private IWalkThroughData _CurrentInstruction;
		public IWalkThroughData CurrentInstruction
		{
			set
			{
				SetProperty("CurrentInstruction", ref _CurrentInstruction, value);
			}
			get
			{
				return _CurrentInstruction;
			}
		}

		private Command _BackCmd;
		public Command BackCmd
		{
			get
			{
				return _BackCmd ?? (_BackCmd = new Command(() =>
				{
					NavigationHandler.GlobalNavigator.Navigation.PopAsync();
				}));
			}
		}

		private Command _ProceedCmd;
		public Command ProceedCmd
		{
			get
			{
				return _ProceedCmd ?? (_ProceedCmd = new Command(async () =>
				{
					if (IsBusy)
						return;
					IsBusy = true;
					ContentPage page = new FitYourDevicePage();
					await NavigationHandler.GlobalNavigator.Navigation.PushAsync(page);
					IsBusy = false;
				}));
			}
		}
	}
}
