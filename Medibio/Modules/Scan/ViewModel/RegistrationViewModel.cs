﻿using System;
using System.Linq;
using Medibio.APIHandler;
using Medibio.Modules.Common;
using Medibio.Modules.Scan.Custom.ViewModel;
using Xamarin.Forms;

namespace Medibio.Modules.Scan.ViewModel
{
	public class RegistrationViewModel : ViewModelBase
	{
		public RegistrationViewModel(string groupId = "")
		{
			GroupId = groupId;
		}

		private string _Email;
		public string Email
		{
			get { return _Email; }
			set { this.SetProperty(nameof(Email), ref _Email, value); }
		}

		private string _ErrorMessage;
		public string ErrorMessage
		{
			get { return _ErrorMessage; }
			set { this.SetProperty(nameof(ErrorMessage), ref _ErrorMessage, value); }
		}

		private string _Password;
		public string Password
		{
			get { return _Password; }
			set { this.SetProperty(nameof(Password), ref _Password, value); }
		}

		private string _GroupId;
		public string GroupId
		{
			get { return _GroupId; }
			set { this.SetProperty(nameof(GroupId), ref _GroupId, value); }
		}

		private Command _BackCmd;
		public Command BackCmd
		{
			get
			{
				return _BackCmd ?? (_BackCmd = new Command(() =>
				{
					NavigationHandler.GlobalNavigator.Navigation.PopAsync();
				}));
			}
		}

		private Command _CreateAccountCommand;
		public Command CreateAccountCommand
		{
			get
			{
				return _CreateAccountCommand ?? (_CreateAccountCommand = new Command(async () =>
				{
					//Pass to API:
					await LoginHandler.GetUserRegister(Email, Password, GroupId, RegisterSuccess, RegisterError);
				}));
			}
		}

		public async void RegisterSuccess(RegisterResponse response)
		{
			if (response.Success)
			{
				//App.Token = response.Token;
				//App.UserId = response.User.Id;

				//TODO - log in the App Settings:
				//response.Token
				//response.User.Email
				//response.User.Id

				await NavigationHandler.GlobalNavigator.Navigation.PopToRootAsync();
			}
			else
			{
				//Generic error occured - API success but no valid user found:                
				await NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, ErrorMessages.Login.E_NOACCOUNT, AppConstants.OK_TEXT);
			}
		}

		public async void RegisterError(ErrorResponse response)
		{
			switch (response.ErrorCode)
			{
				case ErrorCodes.Register.E_GRP: ErrorMessage = ErrorMessages.Register.E_GRP; break;
				case ErrorCodes.Register.E_EMAIL: ErrorMessage = ErrorMessages.Register.E_EMAIL; break;
				case ErrorCodes.Register.E_VALIDATION:
					ErrorMessage = string.Format("{0}{1}", 
					                             ErrorMessages.Register.E_VALIDATION, 
					                             string.Join(Environment.NewLine, response.Errors.Select(s => s.Value.Message)));
					break;
				case ErrorCodes.Register.E_ERROR: ErrorMessage = ErrorMessages.Register.E_ERROR; break;
			}
		}
	}
}

