﻿using System;
using System.Linq;
using Medibio.APIHandler;
using Medibio.Modules.Common;
using Medibio.Modules.Scan.Custom.ViewModel;
using Xamarin.Forms;

namespace Medibio
{
	public class ForgotPasswordViewModel : ViewModelBase
	{
		public ForgotPasswordViewModel()
		{
		}

		private string _Email;
		public string Email
		{
			get { return _Email; }
			set { this.SetProperty(nameof(Email), ref _Email, value); }
		}

		private Command _BackCmd;
		public Command BackCmd
		{
			get
			{
				return _BackCmd ?? (_BackCmd = new Command(() =>
				{
					NavigationHandler.GlobalNavigator.Navigation.PopAsync();
				}));
			}
		}

		private Command _CreateAccountCommand;
		public Command CreateAccountCommand
		{
			get
			{
				return _CreateAccountCommand ?? (_CreateAccountCommand = new Command(async () =>
				{
					//Pass to API:
					await LoginHandler.ForgetPassword(Email, RegisterSuccess, RegisterError);
				}));
			}
		}

		public async void RegisterSuccess(ForgetPasswordResponse response)
		{
			if (response.EmailSent)
			{
				await NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.PASSWORD_EMAIL_SENT, AppConstants.PASSWORD_RESET, AppConstants.OK_TEXT);

				await NavigationHandler.GlobalNavigator.Navigation.PopToRootAsync();
			}
			else
			{			
				await NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.PASSWORD_EMAIL_SENT, AppConstants.PASSWORD_EMAIL_SENT_FAILED, AppConstants.OK_TEXT);                
			}
		}

		public async void RegisterError(ErrorResponse response)
		{
			var errorMessage = string.Empty;
			switch (response.ErrorCode)
			{
				case ErrorCodes.Reset.E_SENT: errorMessage = ErrorMessages.Register.E_GRP; break;
				case ErrorCodes.Reset.E_EMAIL: errorMessage = ErrorMessages.Register.E_EMAIL; break;
				case ErrorCodes.Reset.E_VALIDATION:
					errorMessage = string.Format("{0}{1}",
												 ErrorMessages.Register.E_VALIDATION,
												 string.Join(Environment.NewLine, response.Errors.Select(s => s.Value.Message)));
					await NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.WARNING, errorMessage, AppConstants.OK_TEXT);
					break;
				case ErrorCodes.Register.E_ERROR: errorMessage = ErrorMessages.Register.E_ERROR; break;
			}
		}
	}
}

