﻿using Medibio.APIHandler;
using Medibio.Modules.Common;
using Medibio.Modules.Scan.Custom.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Medibio.Modules.Scan.ViewModel
{
    public class LoginViewModel : ViewModelBase
    {
		ForgotPasswordPage ForgotPasswordPage;
        public LoginViewModel(string groupId = "")
        {
            GroupId = groupId;
			ForgotPasswordPage = new ForgotPasswordPage();
            LoginCmd = new Command(() => Login());
            ForgotPasswordCmd = new Command(() => ForgotPassword());
            BackCmd = new Command(() => { NavigationHandler.GlobalNavigator.Navigation.PopAsync(); });
        }
        
        #region Properties

        private bool _IsBusy;
        public bool IsBusy
        {   
            get { return _IsBusy; }
            set { this.SetProperty(nameof(IsBusy), ref _IsBusy, value); }
        }

        private string _Email;
        public string Email
        {   
            get { return _Email; }
            set { this.SetProperty(nameof(Email), ref _Email, value); }
        }

        private string _Password;
        public string Password
        {   
            get { return _Password; }
            set { this.SetProperty(nameof(Password), ref _Password, value); }
        }

        private string _GroupId;
        public string GroupId
        {
            get { return _GroupId; }
            set { this.SetProperty(nameof(GroupId), ref _GroupId, value); }            
        }

        private bool _IsError;
        public bool IsError
        {
            get { return _IsError; }
            set { this.SetProperty(nameof(IsError), ref _IsError, value); }
        }

        private bool _IsErrorGroupId;
        public bool IsErrorGroupId
        {
            get { return _IsErrorGroupId; }
            set { this.SetProperty(nameof(IsErrorGroupId), ref _IsErrorGroupId, value); }
        }
        
        private bool _IsErrorEmail;
        public bool IsErrorEmail
        {
            get { return _IsErrorEmail; }
            set { this.SetProperty(nameof(IsErrorEmail), ref _IsErrorEmail, value); }            
        }

        private bool _IsErrorPassword;
        public bool IsErrorPassword
        {
            get { return _IsErrorPassword; }
            set { this.SetProperty(nameof(IsErrorPassword), ref _IsErrorPassword, value); }
        }

        private string _ErrorMessage;
        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { this.SetProperty(nameof(ErrorMessage), ref _ErrorMessage, value); }            
        }

        public ImageSource LogoImageSource { get { return ImageSource.FromFile("logo.png"); } }

        #endregion


        #region Commands

        public ICommand LoginCmd { get; private set; }
        public ICommand ForgotPasswordCmd { get; private set; }
        public ICommand BackCmd { get; private set; }       

        #endregion


        #region Methods

        public bool Validate()
        {
            if (string.IsNullOrWhiteSpace(GroupId))
                IsErrorGroupId = true;
            else
                IsErrorGroupId = false;

            if (string.IsNullOrWhiteSpace(Email))
                IsErrorEmail = true;
            else
                IsErrorEmail = false;
            
            //TODO - email format validation?

            if (string.IsNullOrWhiteSpace(Password))
                IsErrorPassword = true;
            else
                IsErrorPassword = false;

            //TODO - password rule validation?

            if (IsErrorGroupId || IsErrorEmail || IsErrorPassword)
            {
                IsError = true;
                return false;
            }
            else
            {
                IsError = false;
                return true;
            }
        }

        public async void Login()
        {
            //Validate credentials:
            if (!Validate())
                return;

            //Pass to API:
            await LoginHandler.Login(Email, Password, GroupId, LoginSuccess, LoginError);
        }

        public async void LoginSuccess(LoginResponse response)
        {
            if (!string.IsNullOrWhiteSpace(response.Token) && response.User != null)
            {
                App.Token = response.Token;
                App.UserId = response.User.Id;

                //TODO - log in the App Settings:
                //response.Token
                //response.User.Email
                //response.User.Id

                await NavigationHandler.GlobalNavigator.Navigation.PopToRootAsync();
            }
            else
            {
                //Generic error occured - API success but no valid user found:                
                await NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, ErrorMessages.Login.E_NOACCOUNT, AppConstants.OK_TEXT);                
            }
        }

        public async void LoginError(ErrorResponse response)
        {
            /*TODO
             * Perhaps some error codes should display as a Dialog/Alert versus others as inline-label error message?
             * For now they're all showing in the ErrorMessage label.
             * */

            switch (response.ErrorCode)
            {
                case ErrorCodes.Login.E_GRP: ErrorMessage = ErrorMessages.Login.E_GRP; break;
                case ErrorCodes.Login.E_NOACCOUNT: 
					ErrorMessage = ErrorMessages.Login.E_NOACCOUNT; 
					//TODO - Register
					break;
                case ErrorCodes.Login.E_INVALID: ErrorMessage = ErrorMessages.Login.E_INVALID; break;
                case ErrorCodes.Login.E_VALIDATION:
                    ErrorMessage = string.Format("{0}{1}", ErrorMessages.Login.E_VALIDATION, string.Join(Environment.NewLine, response.Errors.Select(s => s.Value.Message)));
                    break;
                case ErrorCodes.Login.E_ERROR: ErrorMessage = ErrorMessages.Login.E_ERROR; break;
            }
                        
            //await NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, "Login error", AppConstants.OK_TEXT);
        }

        public async void ForgotPassword()
        {
			//TODO - change to forgotten password page
			await NavigationHandler.GlobalNavigator.Navigation.PushAsync(ForgotPasswordPage);
        }
        #endregion
    }
}