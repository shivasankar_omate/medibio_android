﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.CustomView;
using Xamarin.Forms;

namespace Medibio.Modules.Scan.ViewModel
{
    class InstructionSlideViewModel : IWalkThroughData
    {
        public string Description
        {
            get; set;
        }

        public string Heading
        {
            get; set;
        }

        public ImageSource Source
        {
            get; set;
        }
    }
}
