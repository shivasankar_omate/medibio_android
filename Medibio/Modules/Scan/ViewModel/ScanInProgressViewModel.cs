﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Medibio.BluetoothLE.Model;
using Medibio.Modules.Common;
using Medibio.Modules.Model;
using Medibio.Modules.LogHandler;
using PCLStorage;
using Plugin.DeviceInfo;
using Xamarin.Forms;
using Medibio.Modules.Scan.View;
using Medibio.Extentions;
using Acr.Settings;
using Medibio.BluetoothLE;
using System.Threading;
using Medibio.Modules.Scan.Custom.ViewModel;

namespace Medibio.Modules.Scan.ViewModel
{
	class ScanInProgressViewModel : ViewModelBase
	{
		static string defaultBpm = "0";
		static string defaultDuration = "00:00:00";
		static string defaultBatteryLevel = "0";
		static string durationFormat = "{0:00}:{1:00}:{2:00}";

		ScanManager scanner = ScanManager.Manager;
		IFile _ScanDataFile;
		StringBuilder _ScanDataCapsule = new StringBuilder();
		const int MaxDataLimit = 30;
		int DataCount = 0;
		DateTime StartDateTime = DateTime.Now;
		DateTime ScaningStartTime;
		long SecondsCount;
		ScanSession scanSession = null;

		bool isHeaderWritten = false;

		private bool _IsBusy;
		public bool IsBusy
		{
			set
			{
				{ this.SetProperty(nameof(IsBusy), ref _IsBusy, value); }
			}
			get
			{
				return _IsBusy;
			}
		}

		private string _BatteryStatus;
		public string BatteryStatus
		{
			set
			{
				this.SetProperty(nameof(BatteryStatus), ref _BatteryStatus, value);
				/*
                if (Set("BatteryStatus", ref _BatteryStatus, value))
                {
                    RaisePropertyChanged("BatteryStatus");
                }
                */
			}
			get
			{
				return string.Format("{0}%", _BatteryStatus);
			}
		}

		private string _Bpm;
		public string Bpm
		{
			get
			{
				return _Bpm;
			}
			set
			{
				this.SetProperty(nameof(Bpm), ref _Bpm, value);
				/*
                if (Set("Bpm", ref _Bpm, value))
                {
                    RaisePropertyChanged("Bpm");
                }
                */
			}
		}

		private string _Duration;
		public string Duration
		{
			get
			{
				return _Duration;
			}
			set
			{
				if (!string.Equals(value, this._Duration))
				{
					this._Duration = value;
					Device.BeginInvokeOnMainThread(
						() =>
						{
							this.SetProperty(nameof(Duration), ref _Duration, value);
							//this.RaisePropertyChanged("Duration");
						});
				}
				/*
                if (Set("Duration", ref _Duration, value))
                {
                    RaisePropertyChanged("Duration");
                }
                */
			}
		}

		private bool _IsRunning;
		/*
        public bool IsRunning
        {
            get
            {
                return _IsRunning;
            }
            set
            {
                _IsRunning = value;
                if (IsRunning)
                {
                    
                    var tokenSource = new CancellationTokenSource();
                    var token = tokenSource.Token;
                    Task.Run(() =>
                    {
                        Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                        {
                            try
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    SecondsCount++;
                                    if (scanner.ScanDetail != null)
                                        scanner.ScanDetail.DurationInSeconds = SecondsCount;

                                    TimeSpan span = TimeSpan.FromSeconds(SecondsCount);
                                    Duration = string.Format(durationFormat, span.Hours, span.Minutes, span.Seconds);
                                });
                            }
                            catch
                            {
                                SecondsCount++;
                            }
                            

                            return IsRunning;
                        });
                    },
                    token
                    );
                    
                }
            }
        }*/

		public bool IsRunning
		{
			get
			{
				return _IsRunning;
			}
			set
			{
				if (value != this._IsRunning)
				{
					_IsRunning = value;
					Device.BeginInvokeOnMainThread(() =>
					{
						//this.SetProperty(nameof(Bpm), ref _Bpm, value);
						//this.RaisePropertyChanged("IsRunning");

					});
					this.UpdateTimer();
				}
			}
		}

		private void UpdateTimer()
		{
			if (this.IsRunning)
			{
				StartTimer(SecondsCount);
				/*
                Device.StartTimer(
                    TimeSpan.FromSeconds(1),
                    () =>
                    {
                        SecondsCount++;
                        if (scanner.ScanDetail != null)
                        {
                            scanner.ScanDetail.DurationInSeconds = SecondsCount;
                        }
                        TimeSpan span = TimeSpan.FromSeconds(SecondsCount);

                        Duration = string.Format(durationFormat, span.Hours, span.Minutes, span.Seconds);

                        return IsRunning;
                    });
                    */
			}
			else
			{

				this.cancellationTokenSource?.Cancel();
				// Stop Timer
				// Code here
			}
		}

		private Command _MenuCmd;
		public Command MenuCmd
		{
			get
			{
				return _MenuCmd ?? (_MenuCmd = new Command(() =>
				{
					//TODO: OPen or close slider menu
				}));
			}
		}

		private Command _CompleteScanCmd;
		public Command CompleteScanCmd
		{
			get
			{
				return _CompleteScanCmd ?? (_CompleteScanCmd = new Command(async () =>
				{
					bool answer = await NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.CONFIRM, AppConstants.WANNA_COMPLETE, AppConstants.YES, AppConstants.CANCEL_TEXT);
					if (answer)
						CompleteScan();
				}));
			}
		}


		public ScanInProgressViewModel()
		{
			BatteryStatus = defaultBatteryLevel;
			Duration = defaultDuration;
			Bpm = defaultBpm;
			IsBusy = true;
			if (scanner.HasScanDetails)
			{
				SecondsCount = scanner.ScanDetail.DurationInSeconds;
				IsRunning = true;
				isHeaderWritten = true;
			}
			GetLogFile().ContinueWith(t =>
			{
				scanner.OnDataReceived += Scanner_OnDataReceived;
				scanner.OnConnectionStateChanged += Scanner_OnDeviceStateChanged;
			});

		}

		private void Scanner_OnDataReceived(object sender, BLEDataReceivedEventArgs e)
		{
			if (!IsRunning)
			{
				if (defaultDuration.Equals(Duration))
				{
					ScaningStartTime = DateTime.Now;
					scanSession = ScanSession.Instance;
					WriteHeaderData();
					isHeaderWritten = true;
				}
				IsRunning = true;
			}

			if (IsBusy)
			{
				IsBusy = false;
			}
			HRData hrData = e.Data;
			Bpm = hrData.Bpm.ToString();
			BatteryStatus = hrData.BatteryPercent.ToString();
			try
			{
				if (_ScanDataFile != null && isHeaderWritten)
				{
					WriteScanData(e.Data);
				}
			}
			catch
			{
				Debug.WriteLine("exception on file");
			}

		}

		private void Scanner_OnDeviceStateChanged(Object sender, EventArgs args)
		{
			if (scanner.adapter.DeviceCurrentState == DeviceState.Connected)
			{
				IsRunning = true;
			}
			else if (scanner.adapter.DeviceCurrentState == DeviceState.Disconnected)
			{
				IsRunning = false;
			}
		}

		private async Task CompleteScan()
		{

			IsBusy = true;
			IsRunning = false;

			await scanner.CompleteScan();
			scanner.OnDataReceived -= Scanner_OnDataReceived;
			scanner.OnConnectionStateChanged -= Scanner_OnDeviceStateChanged;
			Settings.Local.Set<Model.Scan>(AppConstants.CURRENT_SCANNING, null);

			_ScanDataCapsule.Append(ScanDataFields.DATAEND + Environment.NewLine);

			await UpdateDataFile(_ScanDataCapsule.ToString());
			await NavigationHandler.GlobalNavigator.Navigation.PushAsync(new ScanCompletePage());
		}


		private async Task GetLogFile()
		{
			try
			{
				IFolder rootFolder = FileSystem.Current.LocalStorage;
				IFolder folder = await rootFolder.CreateFolderAsync("MedibioScanLogs",
					CreationCollisionOption.OpenIfExists);
				if (!scanner.HasScanDetails)
				{
					string timeStamp;
					timeStamp = StartDateTime.ToString(DateFormats.FilenameTimestampFormat);
					string deviceId;
					if (TargetPlatform.Android == Device.OS)
					{
						deviceId = CrossDeviceInfo.Current.Id;
					}
					else
					{
						deviceId = Logger.DefaultDeviceId;
					}
					Model.Scan scanDetail = new Model.Scan()
					{
						StartTime = StartDateTime.ToString(),
						ScanFileName = "MedibioScanData_" + deviceId + "_" + timeStamp + ".medibio_scan"
					};
					scanner.ScanDetail = scanDetail;
				}

				_ScanDataFile = await folder.CreateFileAsync(scanner.ScanDetail.ScanFileName,
					CreationCollisionOption.OpenIfExists);
			}
			catch (Exception e)
			{
				Debug.WriteLine("Exception at scan data :" + e.Message);
			}

		}

		private async void WriteHeaderData()
		{
			_ScanDataCapsule.Append(scanSession.WriteHeaderData());
		}

		private async void WriteScanData(HRData data)
		{
			_ScanDataCapsule.Append(string.Format(@"{0}{1}", data.ToString(), Environment.NewLine));
			DataCount++;

			if (MaxDataLimit == DataCount)
			{
				UpdateDataFile(_ScanDataCapsule.ToString());
			}
		}

		private async Task UpdateDataFile(string text)
		{
			_ScanDataFile.WriteAsync(text);
			_ScanDataCapsule.Clear();
			DataCount = 0;
		}


		private CancellationTokenSource cancellationTokenSource;
		private CancellationToken token;

		private async void StartTimer(long startTime)
		{
			// Do a check to make sure the timer isn't already going
			this.cancellationTokenSource = new CancellationTokenSource();
			this.token = cancellationTokenSource.Token;

			await Task.Run(async () =>
			{
				while (true)
				{

					//poll HW
					await Task.Delay(1000);
					if (token.IsCancellationRequested)
					{
						break;
					}
					startTime++;

					// Notify the updated time here....
					if (scanner.ScanDetail != null)
					{
						scanner.ScanDetail.DurationInSeconds = SecondsCount = startTime;
					}
					TimeSpan span = TimeSpan.FromSeconds(SecondsCount);

					Duration = string.Format(durationFormat, span.Hours, span.Minutes, span.Seconds);

				}
				//cleanup
			}, token).ConfigureAwait(false);

			// If Is reachable, stop video, stop polling and show message, then go back.

		}
	}
}
