﻿using Medibio.APIHandler;
using Medibio.Modules.Common;
using Medibio.Modules.Scan.Custom.ViewModel;
using Medibio.Modules.Scan.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Medibio.Modules.Scan.ViewModel
{
    public class ProgramCodeViewModel : ViewModelBase
    {
        public ProgramCodeViewModel()
        {
            NextCmd = new Command(() => ValidateProgramCode());            
        }

        #region Properties

        private bool _IsBusy;
        public bool IsBusy
        {
            get { return _IsBusy; }
            set { this.SetProperty(nameof(IsBusy), ref _IsBusy, value); }
        }

        private string _ProgramCode;
        public string ProgramCode
        {
            get { return _ProgramCode; }
            set { this.SetProperty(nameof(ProgramCode), ref _ProgramCode, value); }
        }

        private bool _IsError;
        public bool IsError
        {
            get { return _IsError; }
            set { this.SetProperty(nameof(IsError), ref _IsError, value); }
        }

        private bool _IsErrorProgramCode;
        public bool IsErrorProgramCode
        {
            get { return _IsErrorProgramCode; }
            set { this.SetProperty(nameof(IsErrorProgramCode), ref _IsErrorProgramCode, value); }
        }

        private string _ErrorMessage;
        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { this.SetProperty(nameof(ErrorMessage), ref _ErrorMessage, value); }
        }

        public ImageSource LogoImageSource { get { return ImageSource.FromFile("logo.png"); } }

        #endregion


        #region Commands

        public ICommand NextCmd { get; private set; }

        #endregion


        #region Methods

        public bool Validate()
        {
            if (string.IsNullOrWhiteSpace(ProgramCode))
                IsErrorProgramCode = true;
            else
                IsErrorProgramCode = false;

            if (IsErrorProgramCode)
                IsError = true;
            else
                IsError = false;

            return IsError == false;
        }

        public async void ValidateProgramCode()
        {
            IsBusy = true;

            //Validate credentials:
            if (!Validate())
                return;

            //Pass to API:
            await LoginHandler.CheckProgramCode(this.ProgramCode, null, ValidateProgramCodeSuccess, ValidateProgramCodeError);            
        }

        public async void ValidateProgramCodeSuccess(ProgramCodeResponse response)
        {
            IsBusy = false;

            if(response.IsValid && response.GroupInfo != null)
            {
                //var page = new LoginPage(response.GroupInfo.Id);
                //await NavigationHandler.GlobalNavigator.Navigation.PushAsync(page);
            }
            else
            {
                ErrorMessage = ErrorMessages.ProgramCode.E_INVALID;
                IsError = true;                
            }
        }

        public async void ValidateProgramCodeError(ErrorResponse response)
        {
            IsBusy = false;

            /*TODO
             * Perhaps some error codes should display as a Dialog/Alert versus others as inline-label error message?
             * For now they're all showing in the ErrorMessage label.
             * */

            switch (response.ErrorCode)
            {
                case ErrorCodes.ProgramCode.E_VALIDATION:
                    ErrorMessage = string.Format("{0}{1}", ErrorMessages.ProgramCode.E_VALIDATION, string.Join(Environment.NewLine, response.Errors.Select(s => s.Value.Message)));
                    break;
                default:
                    ErrorMessage = ErrorMessages.ProgramCode.E_ERROR;
                    break;
            }

            //await NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, "Login error", AppConstants.OK_TEXT);
        }

        #endregion
    }
}
