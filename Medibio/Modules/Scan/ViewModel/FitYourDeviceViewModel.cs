﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Medibio.Modules.Common;
using Medibio.Modules.Scan.Custom.ViewModel;
using Medibio.Modules.Scan.View;
using Xamarin.Forms;

namespace Medibio.Modules.Scan.ViewModel
{
	public class FitYourDeviceViewModel : ViewModelBase
	{
		bool IsRetest { get; set; }

		bool _IsBusy;
		public bool IsBusy
		{
			set
			{
				SetProperty("IsBusy", ref this._IsBusy, value);
			}
			get
			{
				return _IsBusy;
			}
		}

		private Command _BackCmd;
		public Command BackCmd
		{
			get
			{
				return _BackCmd ?? (_BackCmd = new Command(() =>
				{
					NavigationHandler.GlobalNavigator.Navigation.PopAsync();
				}));
			}
		}

		Command _TestConnectionCmd;
		public Command TestConnectionCmd
		{
			get
			{
				return _TestConnectionCmd ?? (_TestConnectionCmd = new Command(() =>
				{
					if (IsBusy)
						return;
					IsBusy = true;
					ScanManager.Manager.StartConnection(OnConnectionSuccess, OnConnectionError);
				}));
			}
		}

		Command _RetestCmd;
		public Command RetestCmd
		{
			get
			{
				return _RetestCmd ?? (_RetestCmd = new Command(() =>
				{
					if (IsBusy)
						return;
					IsBusy = true;
					IsRetest = true;
					ScanManager.Manager.StartConnection(OnConnectionSuccess, OnConnectionError);
				}));
			}
		}

		//This method will get called when Connection is successfuly made.
		private async void OnConnectionSuccess()
		{
			if (IsRetest)
			{
				try
				{
					Page currentPage = NavigationHandler.GlobalNavigator.CurrentPage;
					await LaunchDevcieFoundPage();
					NavigationHandler.GlobalNavigator.Navigation.RemovePage(currentPage);
				}
				catch (Exception e)
				{
					Debug.WriteLine(" Exception " + e.Message);
				}

			}
			else
			{
				await LaunchDevcieFoundPage();
			}
			IsRetest = false;
			IsBusy = false;

		}

		//Handle all the error codes here.
		private void OnConnectionError(string errorCode)
		{
			if (BluetoothErrorCode.NOT_ENABLED.Equals(errorCode))
			{
				NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, AppConstants.BLE_SETTINGS_ERROR, AppConstants.OK_TEXT);
				IsBusy = false;
				return;
			}
			else if (BluetoothErrorCode.DEVICE_NOT_FOUND.Equals(errorCode))
			{
				NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, AppConstants.CANNT_FIND_DEVICE, AppConstants.OK_TEXT);
			}

			LaunchDevcieNotFoundpage();
			IsBusy = false;
		}

		private async void LaunchDevcieNotFoundpage()
		{
			ContentPage page = new DeviceNotConnected();
			page.BindingContext = this;
			NavigationHandler.GlobalNavigator.Navigation.PushAsync(page);
		}

		private async Task LaunchDevcieFoundPage()
		{
			Device.BeginInvokeOnMainThread(() =>
			{
				ContentPage page = new DeviceConnectedPage();
				NavigationHandler.GlobalNavigator.Navigation.PushAsync(page);
				Debug.WriteLine("DeviceConnectedPage Navigation called");

			});

		}
	}
}
