namespace Medibio.Modules.Scan.Custom.ViewModel
{
	using System.Collections.ObjectModel;
	using System.ComponentModel;
	using System.Runtime.CompilerServices;

	//using Medibio.Annotations;
	using Medibio.CustomView;
	using Medibio.Modules.Result.ViewModel;

	public class ViewModelBase : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		//[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		protected void SetProperty(string propertyName, ref ObservableCollection<IWalkThroughData> referenceProperty, ObservableCollection<IWalkThroughData> newProperty)
		{
			if (newProperty != referenceProperty)
			{
				referenceProperty = newProperty;
			}
			this.OnPropertyChanged(propertyName);
		}

		protected void SetProperty(string propertyName, ref ObservableCollection<ScanResultItemViewModel> referenceProperty, ObservableCollection<ScanResultItemViewModel> newProperty)
		{
			if (newProperty != referenceProperty)
			{
				referenceProperty = newProperty;
			}
			this.OnPropertyChanged(propertyName);
		}

		protected void SetProperty(string propertyName, ref string referenceProperty, string newProperty)
		{
			if (!newProperty.Equals(referenceProperty))
			{
				referenceProperty = newProperty;
			}
			this.OnPropertyChanged(propertyName);
		}

		protected void SetProperty(string propertyName, ref IWalkThroughData referenceProperty, IWalkThroughData newProperty)
		{
			if (newProperty != referenceProperty)
			{
				referenceProperty = newProperty;
			}
			this.OnPropertyChanged(propertyName);
		}

		protected void SetProperty(string propertyName, ref bool referenceProperty, bool newProperty)
		{
			if (newProperty != referenceProperty)
			{
				referenceProperty = newProperty;
			}
			this.OnPropertyChanged(propertyName);
		}

	}
}