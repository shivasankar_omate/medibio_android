﻿using System;
using Medibio.Modules.Scan.View;
using Xamarin.Forms;
using System.Collections.Generic;
using Medibio.Modules.Scan.Custom.ViewModel;
using Medibio.Modules.About;

namespace Medibio.Modules.Scan.ViewModel
{
	public class HomeViewModel : ViewModelBase
	{
		public class MenuItem
		{
			private string _Title;
			public string Title
			{
				get { return _Title; }
				set { _Title = value; }
			}

			private Command _SelectedCommand;
			public Command SelectedCommand
			{
				get
				{
					return _SelectedCommand;
				}
				set
				{
					_SelectedCommand = value;
				}
			}
		}
		public List<MenuItem> MenuItems
		{
			get;
		}

		private bool _IsBusy;
		public bool IsBusy
		{
			set
			{
				this.SetProperty("IsBusy", ref _IsBusy, value);
			}
			get
			{
				return _IsBusy;
			}
		}

		private Command _GetStartedCmd;
		public Command GetStartedCmd
		{
			get
			{
				return _GetStartedCmd ?? (_GetStartedCmd = new Command(() =>
					{
						if (!IsBusy && NavigationHandler.GlobalNavigator.Navigation.NavigationStack.Count == 1)
						{
							IsBusy = true;
							GoToInstructionPage();
						}
					}));
			}
		}

		private Command _MenuCmd;
		public Command MenuCmd
		{
			get
			{
				return _MenuCmd ?? (_MenuCmd = new Command(() =>
				{
					//TODO: OPen or close slider menu
				}));
			}
		}

		private async void GoToInstructionPage()
		{
			//ContentPage NextPage = new InstructionPage();
			IsBusy = false;

			await NavigationHandler.GlobalNavigator.Navigation.PushAsync(NextPage);
		}

		public async void GoToAboutPage()
		{
			//ContentPage NextPage = new InstructionPage();
			IsBusy = false;
			await NavigationHandler.GlobalNavigator.Navigation.PushAsync(AboutPage);
		}

		private void GoToResultDetails()
		{
			//TODO Navigate to Scan results
		}

		ContentPage NextPage;

		//ILayout CurrentPage;

		AboutPage AboutPage;

		public HomeViewModel(HomePage bindingPage)
		{
			//todo:- refactor as this does not appear to be a good idea
			//CurrentPage = bindingPage;
			//bindingPage.Appearing += CurrentPage_Appearing;
			AboutPage = new AboutPage();
			NextPage = new InstructionPage();
			MenuItems = new List<MenuItem>{
				new MenuItem{Title = "Home",SelectedCommand = new Command(() =>
					{
					})},
				new MenuItem{Title = "Scan in progress",SelectedCommand = new Command(() =>
					{
					})},
				new MenuItem{Title = "My Results",SelectedCommand = new Command(() =>
					{
					})},
				new MenuItem{Title = "About Medibo and stress",SelectedCommand = new Command(() =>
					{
						if (!IsBusy && NavigationHandler.GlobalNavigator.Navigation.NavigationStack.Count == 1)
						{
							IsBusy = true;
							GoToAboutPage();
						}
					})},
				new MenuItem{Title = "Upload logs",SelectedCommand = new Command(() =>
					{
					})}
			};
		}

		//private void CurrentPage_Appearing(object sender, EventArgs e)
		//{
		//	NextPage = new InstructionPage();
		//}
	}
}

