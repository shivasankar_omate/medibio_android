﻿using System.Threading.Tasks;
using Medibio.Modules.Common;
using Medibio.Modules.LogHandler;
using Medibio.Modules.Scan.Custom.ViewModel;
using Plugin.Connectivity;

namespace Medibio.Modules.Scan.ViewModel
{
	public class ScanCompletedViewModel : ViewModelBase
	{
		private bool _IsBusy;
		public bool IsBusy
		{
			set
			{
				SetProperty("IsBusy", ref _IsBusy, value);
			}
			get
			{
				return _IsBusy;
			}
		}
		// This logic looks incorrect - if one file fails to upload then no files get deleted
		// so they will all be uploaded again next time ?!
		// Files should be uploaded and if successful deleted one at a time.
		//
		private async Task UploadScanFiles()
		{
			if (CrossConnectivity.Current.IsConnected)
			{
				await Logger.Handler.UploadLogFiles(async () =>
				{
					IsBusy = false;
					Logger.Handler.DeleteLogFiles();
					await NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, AppConstants.UPLOAD_SUCCESSFUL, AppConstants.OK_TEXT);
					//NavigationHandler.GlobalNavigator.Navigation.PopToRootAsync();
				}, async () =>
				{
					IsBusy = false;
					await NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, AppConstants.CANNT_UPLOAD_FILE, AppConstants.OK_TEXT);
					//NavigationHandler.GlobalNavigator.Navigation.PopToRootAsync();
				});
			}
			else
			{
				await NavigationHandler.GlobalNavigator.DisplayAlert(AppConstants.APP_NAME, AppConstants.NETWORK_ERROR, AppConstants.OK_TEXT);
				//NavigationHandler.GlobalNavigator.Navigation.PopToRootAsync();
			}
		}

		public ScanCompletedViewModel()
		{
			IsBusy = true;
			UploadScanFiles();
		}
	}
}
