﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Medibio.Modules.Scan.ViewModel;

namespace Medibio.Modules.Scan.View
{
    public partial class FitYourDevicePage : ContentPage
    {
        public FitYourDevicePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.BindingContext = new FitYourDeviceViewModel();
        }

        
    }
}
