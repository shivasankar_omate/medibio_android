﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.Effects;
using Medibio.Modules.Scan.ViewModel;
using Xamarin.Forms;

namespace Medibio.Modules.Scan.View
{
	public partial class DeviceConnectedPage : ContentPage
	{
		public DeviceConnectedPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			GradientBanner.Effects.Add(new GradientEffect()
			{
				BorderRadious = 0,
				Colors = new Color[] { Color.FromHex("#FFFFFF"), Color.FromHex("#FFFFFF"), Color.FromHex("#f3f3f3") },
				StartPoints = new float[] { 0f, 0.5f, 1f },
				Orientation = GradientOrientation.Vertical
			});

			this.BindingContext = new DeviceConnectedViewModel();
		}
	}
}
