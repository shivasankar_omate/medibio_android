﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Medibio
{
	public partial class ForgotPasswordPage : ContentPage
	{
		public ForgotPasswordPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			this.BindingContext = new ForgotPasswordViewModel();
		}
	}
}

