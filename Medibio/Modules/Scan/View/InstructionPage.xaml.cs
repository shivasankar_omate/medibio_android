﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Medibio.CustomView;
using Medibio.Modules.Scan.ViewModel;
using Xamarin.Forms;

namespace Medibio.Modules.Scan.View
{
    public partial class InstructionPage : ContentPage
    {
        public InstructionPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.BindingContext = new InstructionViewModel();
            
            //WalkThroughView.ItemTemplate = new DataTemplate(typeof(InstructionView));
            //WalkThroughView.SetBinding(WalkThroughCarouselView.ItemsSourceProperty, "Instructions");
			//WalkThroughView.SetBinding(WalkThroughCarouselView.SelectedItemProperty, "CurrentInstruction");
            PageIndicator.SetBinding(PagerIndicatorDots.ItemsSourceProperty, "Instructions");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            PageIndicator.SetBinding(PagerIndicatorDots.SelectedItemProperty, "CurrentInstruction", mode: BindingMode.OneWay);
        }

		void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
		{
			var viewModel = ((InstructionViewModel)this.BindingContext);
			if (viewModel != null)
			{
				var item = e.SelectedItem as IWalkThroughData;
				if (item == null)
					return;

				viewModel.CurrentInstruction = item;
			}
		}
	}
}
