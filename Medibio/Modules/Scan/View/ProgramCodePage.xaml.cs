﻿using Medibio.Modules.Scan.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Medibio.Modules.Scan.View
{
    public partial class ProgramCodePage : ContentPage
    {
        public ProgramCodePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new ProgramCodeViewModel();
        }
    }
}
