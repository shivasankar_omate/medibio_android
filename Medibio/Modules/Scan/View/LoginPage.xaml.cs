﻿using Medibio.APIHandler;
using Medibio.Modules.Scan.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Medibio.Modules.Scan.View
{
    public partial class LoginPage : ContentPage
    {
        public LoginViewModel ViewModel { get; private set; }

        public LoginPage(string groupId)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = ViewModel = new LoginViewModel(groupId);
        }
    }
}