﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Medibio.Modules.Scan.View
{
    public partial class InstructionView : ContentView
    {
        public InstructionView()
        {
            InitializeComponent();
        }
        public ImageSource Source
        {
            set
            {
                this.SampleImage.Source = value;
            }
            get
            {
                return this.SampleImage.Source;
            }
        }

        public string Heading
        {
            set
            {
                this.Header.Text = value;
            }
            get
            {
                return this.Header.Text;
            }
        }

        public string DescriptionText
        {
            set
            {
                this.Description.Text = value;
            }
            get
            {
                return this.Description.Text;
            }
        }
        
        public bool CanShowBulletIn
        {
            set
            {
                if (value)
                {
                    this.BulletsContainer.IsVisible = true;
                    this.Description.IsVisible = false;
                }else
                {
                    this.BulletsContainer.IsVisible = false;
                    this.Description.IsVisible = true;
                }
                
            }
        }
    }
}
