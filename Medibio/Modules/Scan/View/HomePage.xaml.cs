﻿using Medibio.Modules.Scan.ViewModel;
using Xamarin.Forms;

namespace Medibio.Modules.Scan.View
{
	public partial class HomePage : MasterDetailPage
	{
		public HomePage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			this.BindingContext = new HomeViewModel(this);

		}
	}
}
