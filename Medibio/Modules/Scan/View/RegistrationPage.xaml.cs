﻿using System;
using System.Collections.Generic;
using Medibio.Modules.Scan.ViewModel;
using Xamarin.Forms;

namespace Medibio
{
	public partial class RegistrationPage : ContentPage
	{
		public RegistrationPage(string groupId)
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			this.BindingContext = new RegistrationViewModel(groupId);
		}
	}
}

