﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Graphics.Drawables.Shapes;
using Medibio.Droid.Effects;
using Medibio.Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly:ResolutionGroupName("Medibio")]
[assembly:ExportEffect(typeof(NativeGradientEffect), "GradientEffect")]
namespace Medibio.Droid.Effects
{
    /**
     * 
     **/
    public class NativeGradientEffect : PlatformEffect
    { 
        /*
        public static int[] DefaultColors = new int[] {
                Android.Graphics.Color.ParseColor("#9D3467"),
                Android.Graphics.Color.ParseColor("#DC7844"),
                Android.Graphics.Color.ParseColor("#DC7844"),
                Android.Graphics.Color.ParseColor("#9D3467")
                };
        public static float[] DefaultStartPoints = new float[] {
                0, 0.52f, 0.53f, 1f };
        */
        
        protected override void OnAttached()
        {
            try
            {
                View view = (View)this.Element;
                var effects = new List<Effect>(Element.Effects); 
                var effect = (GradientEffect)effects.Find(e => e is GradientEffect);
                if (view != null)
                {
                    ShaderEffect sf = new ShaderEffect(effect);
                    PaintDrawable paint = new PaintDrawable();
                    paint.Shape = new RectShape();
                    paint.SetShaderFactory(sf);

                    //Setting border if it is given.
                    if (effect.BorderRadious != 0)
                    {
                        paint.SetCornerRadius(effect.BorderRadious);
                    }
                    //Check whether it is view or contianer.
                    if(Control == null && Container != null)
                    {
                        this.Container.SetBackground(paint);
                    }
                    else
                    {
                        this.Control.SetBackground(paint);
                    }
                   
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception in Gradient Effect android:>>" + e.Message);
            }
        }

        protected override void OnDetached()
        {
            //Nothing to do with it
        }

        public class ShaderEffect : ShapeDrawable.ShaderFactory
        {
            private GradientEffect _Effect;
            private int[] _Colors;
            private float[] _StartPoints;

            private float _Angle
            {
                get {
                    return _Effect.Angle;
                }
            }

            private GradientOrientation _Orientaion {
                get
                {
                    return _Effect.Orientation;
                }
            }

            public ShaderEffect(GradientEffect effect) {
                _Effect = effect;
                UpdateColors();
                UpdateStartPoints();
            }
            
            public override Shader Resize(int width, int height)
            {
                if (_Colors.Length == _StartPoints.Length)
                {
                    LinearGradient lg;
                    float X1,Y1;
                    if (_Orientaion == GradientOrientation.Horizontal)
                    {
                        X1 = width;
                        if (_Angle == 0)
                            Y1 = 0;
                        else
                            Y1 = (float) Math.Tan(_Angle)* X1;
                    }
                    else
                    {
                        Y1 = height;
                        if (_Angle == 0)
                            X1 = 0;
                        else
                            X1 = (float)Math.Tan(_Angle) * Y1;
                    }
                    lg = new LinearGradient(0, 0, X1,Y1,
                    _Colors,
                    _StartPoints,
                    Shader.TileMode.Clamp);
                    return lg;
                }
                else
                {
                    return null;
                }
            }

            public void UpdateColors()
            {
                int[] colors = new int[0];
                if ((_Effect != null) && (_Effect.Colors != null) && (_Effect.Colors.Length != 0))
                {
                    colors = new int[_Effect.Colors.Length];
                    for (int i = 0; i < _Effect.Colors.Length; i++)
                    {
                        colors[i] = _Effect.Colors[i].ToAndroid();
                    }
                    
                }

                _Colors = colors;
            }

            public void UpdateStartPoints()
            {
                if ((_Effect != null) && (_Effect.StartPoints != null) && (_Effect.StartPoints.Length != 0))
                {
                    _StartPoints = _Effect.StartPoints;
                }
            }
        }
    }

    
}
