﻿using Android.Graphics;
using Android.Widget;
using Medibio.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Label), typeof(ExtendedLabelRenderer))]
namespace Medibio.Droid.Renderers
{
    public class ExtendedLabelRenderer: LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            var label = (TextView)Control;
            Typeface font;
            if (e.NewElement.FontAttributes == FontAttributes.Bold)
                font = Typeface.CreateFromAsset(Forms.Context.Assets, "Roboto-Medium.ttf");
            else
                font = Typeface.CreateFromAsset(Forms.Context.Assets, "Roboto-Regular.ttf");
            label.Typeface = font;
        }
    }
}