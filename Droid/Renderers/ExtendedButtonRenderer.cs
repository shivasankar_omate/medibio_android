﻿using Android.Graphics;
using Medibio.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly:ExportRenderer(typeof(Button) , typeof(ExtendedButtonRenderer))]
namespace Medibio.Droid.Renderers
{
    public class ExtendedButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            var button = (Android.Widget.Button)Control;
            Typeface font;
            if (e.NewElement.FontAttributes == FontAttributes.Bold)
                font = Typeface.CreateFromAsset(Forms.Context.Assets, "Roboto-Medium.ttf");
            else
                font = Typeface.CreateFromAsset(Forms.Context.Assets, "Roboto-Regular.ttf");
            button.Typeface = font;
        }
    }
}