using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using Medibio.Droid.FileHelper;
using Medibio.FileHelper;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileHelperImplementation))]
namespace Medibio.Droid.FileHelper
{
    class FileHelperImplementation : IFileHelper
    {
        public async Task WriteAsync(string filePath, string text)
        {
            StreamWriter writer = new StreamWriter(filePath, true);
            writer.Write(text);
            writer.Close();
        }
    }
}