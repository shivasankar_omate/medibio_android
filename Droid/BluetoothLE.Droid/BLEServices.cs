﻿
namespace Medibio.Droid.BluetoothLE
{
    public class BLEServices
    {
        public static string CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

        public static string DeviceInfoService = "0000180a-0000-1000-8000-00805f9b34fb";
        public static string HRService = "0000180d-0000-1000-8000-00805f9b34fb";
        public static string BatteryService = "0000180f-0000-1000-8000-00805f9b34fb";

        public static string hrCharacteristicUuidString = "00002a37-0000-1000-8000-00805f9b34fb";
        public static string batteryCharacteristicUuidString = "00002a19-0000-1000-8000-00805f9b34fb";

        public static string kHxM2CustomServiceUuidString = "BEFDFF10-C979-11E1-9B21-0800200C9A66";
        public static string kHxM2ActivityCharacteristicUuidString = "BEFDFF11-C979-11E1-9B21-0800200C9A66";
        public static string kHxM2ECGCharacteristicUuidString = "BEFDFF13-C979-11E1-9B21-0800200C9A66";
        public static string kHxM2CombinedDataCharacteristicUuidString = "BEFDFF14-C979-11E1-9B21-0800200C9A66";

        public static string kDeviceInfoServiceUuidString = "0000180A-0000-1000-8000-00805f9b34fb";
        public static string kModelNumberCharacteristicUuidString = "00002A24-0000-1000-8000-00805f9b34fb";
        public static string kSerialNumberCharacteristicUuidString = "00002A25-0000-1000-8000-00805f9b34fb";
        public static string kFirmwareDateCharacteristicUuidString = "00002A26-0000-1000-8000-00805f9b34fb";
        public static string kHardwareRevisionCharacteristicUuidString = "00002A27-0000-1000-8000-00805f9b34fb";
        public static string kFirmwareVersionCharacteristicUuidString = "00002A28-0000-1000-8000-00805f9b34fb";
        public static string kManufacturerNameCharacteristicUuidString = "00002A29-0000-1000-8000-00805f9b34fb";

    }

    public class BLEDevices
    {
        public static string BioHarness3 = "BioHarness 3";
        public static string HXM2 = "HxM2";

    }
}