using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Medibio.Modules.Model;

namespace Medibio.Droid.BluetoothLE
{
    public class GattCB : BluetoothGattCallback
    {

        BluetoothAdapterImplementation _Adap = new BluetoothAdapterImplementation();

        bool isReading;
        int batteryCounter;
        int devInfoCount;
        bool hasFoundServices;
        List<BluetoothGattCharacteristic> devInfoCharacteristics = new List<BluetoothGattCharacteristic>();
        //Object readLock = new Object();
        BluetoothGattCharacteristic cdCharacteristic, hrCharacteristic, batteryCharacteristic, activityCharacteristic, ecgCharacteristic;
        List<BluetoothGattDescriptor> descriptorWriteQueue = new List<BluetoothGattDescriptor>();

        //Constructor
        public GattCB(BluetoothAdapterImplementation adap)
        {
            _Adap = adap;
            // _Adapter = new BluetoothAdapterImplementation();
        }



        public override void OnConnectionStateChange(BluetoothGatt gatt, GattStatus status, ProfileState newState)
        {
            base.OnConnectionStateChange(gatt, status, newState);
            //Connection established
            if (status == GattStatus.Success
                && newState == ProfileState.Connected)
            {
                //Discover services
                
                if (!hasFoundServices)
                {
                    hasFoundServices = gatt.DiscoverServices();
                    _Adap.DeviceConnected();
                }else
                {
                    hasFoundServices = gatt.DiscoverServices();
                    _Adap.DeviceReconnected();
                }
            }
            else if (status == GattStatus.Success
              && newState == ProfileState.Disconnected)
            {
                //Handle a disconnect event
                _Adap.DeviceDisconnected();
            }
            else if(newState == ProfileState.Disconnected)
            {
                //Handle a disconnect event
                _Adap.DeviceDisconnected();
            }
            else
            {
                Console.WriteLine("Gatt Status>>>" + status);
                Console.WriteLine("ProfileState>>>" + newState);
            }
            
            /*
            else if (newState == ProfileState.Disconnected)
            {
                _Adap.DeviceDisconnected();
            }
            */
        }

        public override void OnServicesDiscovered(BluetoothGatt gatt, GattStatus status)
        {
            base.OnServicesDiscovered(gatt, status);
            foreach (BluetoothGattService service in gatt.Services)
            {
                string uuid = service.Uuid.ToString().ToUpper();
                if (uuid.Equals(BLEServices.HRService.ToUpper()))
                {
                    _Adap.LogMessage("HRService discovered");
                    foreach (BluetoothGattCharacteristic characteristic in service.Characteristics)
                    {
                        string c_uuid = characteristic.Uuid.ToString().ToUpper();
                        _Adap.LogMessage(" HRCharacteristic: " + c_uuid);

                        if (c_uuid.Equals(BLEServices.hrCharacteristicUuidString.ToUpper()))
                        {
                            hrCharacteristic = characteristic;
                        }
                    }

                }
                else if (uuid.Equals(BLEServices.BatteryService.ToUpper()))
                {
                    _Adap.LogMessage("BatteryService discovered");
                    foreach (BluetoothGattCharacteristic characteristic in service.Characteristics)
                    {
                        string c_uuid = characteristic.Uuid.ToString().ToUpper();
                        _Adap.LogMessage(" BatteryService: " + c_uuid);

                        if (c_uuid.Equals(BLEServices.batteryCharacteristicUuidString.ToUpper()))
                        {
                            batteryCharacteristic = characteristic;
                            // Also do an initial read because it seems we may not get
                            // and initial value.
                            devInfoCharacteristics.Add(characteristic);
                        }
                    }
                }
                else if (uuid.Equals(BLEServices.DeviceInfoService.ToUpper()))
                {
                    _Adap.LogMessage("DeviceInfoService discovered");
                    foreach (BluetoothGattCharacteristic characteristic in service.Characteristics)
                    {
                        string c_uuid = characteristic.Uuid.ToString().ToUpper();

                        devInfoCharacteristics.Add(characteristic);

                    }


                }
                else if (uuid.Equals(BLEServices.kHxM2CustomServiceUuidString.ToUpper()))
                {
                    _Adap.LogMessage("HxM2CustomService discovered");
                    foreach (BluetoothGattCharacteristic characteristic in service.Characteristics)
                    {
                        string c_uuid = characteristic.Uuid.ToString().ToUpper();

                        if (c_uuid.Equals(BLEServices.kHxM2CombinedDataCharacteristicUuidString.ToUpper()))
                        {
                            cdCharacteristic = characteristic;
                        }
                        if (c_uuid.Equals(BLEServices.kHxM2ActivityCharacteristicUuidString.ToUpper()))
                        {
                            activityCharacteristic = characteristic;
                        }
                        if (c_uuid.Equals(BLEServices.kHxM2ECGCharacteristicUuidString.ToUpper()))
                        {
                            ecgCharacteristic = characteristic;
                        }

                    }
                }
                else
                {
                    _Adap.LogMessage("Unknown Service " + uuid + " discovered");
                }


            }

            // Now set the notifications up
            doReadDeviceInfo(gatt);

        }

        /// <summary>
        /// We start by reading the device info and then once that is done we enable CD notifications
        /// </summary>
        /// <param name="gatt">Gatt.</param>
        public void doReadDeviceInfo(BluetoothGatt gatt)
        {

            _Adap.LogMessage("Enable notifications");

            //Enable the required notifications
            //String useCh = _Adap.useCharacteristic.ToUpper();
            String useCh = BLEServices.kHxM2CombinedDataCharacteristicUuidString.ToUpper();
            // Seems we can't subscribe to 
            enableBatteryCharacteristic(gatt);
            
			if (cdCharacteristic != null) {
				_Adap.LogMessage("Combined Data is available so we are using it");
				enableCDCharacteristic (gatt);
			} else {
				_Adap.LogMessage("Combined Data is NOT available so we are using HR and Activity");
				enableHRCharacteristic(gatt);
				enableActivityCharacteristic(gatt);  // We need activity is we are using HR
			}

            // Now start the first one... this will also the characteristic reads
            //				if(descriptorWriteQueue.Count >= 1){   
            //					gatt.WriteDescriptor(descriptorWriteQueue.ElementAt(0));      
            //				}
            _Adap.LogMessage("Reading Device Info");
            if (devInfoCharacteristics.Count >= 1)
            {
                gatt.ReadCharacteristic(devInfoCharacteristics.ElementAt(0));
            }


        }
        public void doReadBattery(BluetoothGatt gatt)
        {
            // Do nothing here for now as we are testing notifications
            return;
            _Adap.LogMessage("Battery:  reading characteristic");

            isReading = gatt.ReadCharacteristic(batteryCharacteristic);

            // Wait for read to complete before continuing.
            while (isReading)
            {
                System.Threading.Thread.Sleep(50);

            }
            devInfoCount++;

            _Adap.LogMessage("Battery: Done reading battery");

        }
        public Java.Util.UUID UUID_DESCRIPTOR_NOTIFICATION_CFG = Java.Util.UUID.FromString("00002902-0000-1000-8000-00805f9b34fb");

        public void enableHRCharacteristic(BluetoothGatt gatt)
        {
            if (hrCharacteristic == null)
            {
                _Adap.LogMessage("Heart Rate characteristic is null, unable to enable notifications");
                return;
            }
            if (gatt.SetCharacteristicNotification(hrCharacteristic, true))
            {
                _Adap.LogMessage("Set heart rate characteristic notification");
            }
            else
            {
                _Adap.LogMessage("Failed to set heart rate characteristic notification");
            }
            System.Threading.Thread.Sleep(50);
            BluetoothGattDescriptor descriptor = hrCharacteristic.GetDescriptor(UUID_DESCRIPTOR_NOTIFICATION_CFG);
            descriptor.SetValue(BluetoothGattDescriptor.EnableNotificationValue.ToArray());
            descriptorWriteQueue.Add(descriptor);
            //gatt.WriteDescriptor (descriptor);
            //_Adap.StartTimer ();
        }
        public void enableCDCharacteristic(BluetoothGatt gatt)
        {
            if (cdCharacteristic == null)
            {
                _Adap.LogMessage("Combined Data characteristic is null, unable to enable notifications");
                return;
            }
            if (gatt.SetCharacteristicNotification(cdCharacteristic, true))
            {
                _Adap.LogMessage("Set combined characteristic notification");
            }
            else
            {
                _Adap.LogMessage("Failed to set combined characteristic notification");
            }
            System.Threading.Thread.Sleep(50);
            BluetoothGattDescriptor descriptor = cdCharacteristic.GetDescriptor(UUID_DESCRIPTOR_NOTIFICATION_CFG);
            descriptor.SetValue(BluetoothGattDescriptor.EnableNotificationValue.ToArray());
            descriptorWriteQueue.Add(descriptor);
            //gatt.WriteDescriptor (descriptor);
            // Start a timer to make sure that we can recover if we never receive any data from the device
            //_Adap.StartTimer ();
        }
        public void enableECGCharacteristic(BluetoothGatt gatt)
        {
            if (ecgCharacteristic == null)
            {
                _Adap.LogMessage("ECG characteristic is null, unable to enable notifications");
                return;
            }
            if (gatt.SetCharacteristicNotification(ecgCharacteristic, true))
            {
                _Adap.LogMessage("Set ECG characteristic notification");
            }
            else
            {
                _Adap.LogMessage("Failed to set ECG characteristic notification");
            }
            System.Threading.Thread.Sleep(100);
            BluetoothGattDescriptor descriptor = ecgCharacteristic.GetDescriptor(UUID_DESCRIPTOR_NOTIFICATION_CFG);
            descriptor.SetValue(BluetoothGattDescriptor.EnableNotificationValue.ToArray());
            descriptorWriteQueue.Add(descriptor);
            //gatt.WriteDescriptor (descriptor);
            // Start a timer to make sure that we can recover if we never receive any data from the device
            //_Adap.StartTimer ();
        }
        public void enableActivityCharacteristic(BluetoothGatt gatt)
        {
            if (activityCharacteristic == null)
            {
                _Adap.LogMessage("Activity characteristic is null, unable to enable notifications");
                return;
            }
            if (gatt.SetCharacteristicNotification(activityCharacteristic, true))
            {
                _Adap.LogMessage("Set Activity characteristic notification");
            }
            else
            {
                _Adap.LogMessage("Failed to set Activity characteristic notification");
            }
            System.Threading.Thread.Sleep(100);
            BluetoothGattDescriptor descriptor = activityCharacteristic.GetDescriptor(UUID_DESCRIPTOR_NOTIFICATION_CFG);
            descriptor.SetValue(BluetoothGattDescriptor.EnableNotificationValue.ToArray());
            descriptorWriteQueue.Add(descriptor);
            //gatt.WriteDescriptor (descriptor);
            // Start a timer to make sure that we can recover if we never receive any data from the device
            //_Adap.StartTimer ();
        }
        public void enableBatteryCharacteristic(BluetoothGatt gatt)
        {
            if (batteryCharacteristic == null)
            {
                _Adap.LogMessage("Battery characteristic is null, unable to enable notifications");
                return;
            }
            if (gatt.SetCharacteristicNotification(batteryCharacteristic, true))
            {
                _Adap.LogMessage("Set battery characteristic notification");
            }
            else
            {
                _Adap.LogMessage("Failed to set battery characteristic notification");
            }
            System.Threading.Thread.Sleep(100);
            BluetoothGattDescriptor descriptor = batteryCharacteristic.GetDescriptor(UUID_DESCRIPTOR_NOTIFICATION_CFG);
            if(descriptor != null)
            {
                descriptor.SetValue(BluetoothGattDescriptor.EnableNotificationValue.ToArray());
                descriptorWriteQueue.Add(descriptor);
            }
            //gatt.WriteDescriptor (descriptor);
        }
        public override void OnDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, GattStatus status)
        {
            if (status == GattStatus.Success)
            {
                _Adap.LogMessage("Callback: Wrote GATT Descriptor successfully.");
            }
            else
            {
                _Adap.LogMessage("Callback: Error writing GATT Descriptor: " + status);
            }
            descriptorWriteQueue.RemoveAt(0);  //pop the item that we just finishing writing
                                               //if there is more to write, do it!
            if (descriptorWriteQueue.Count > 0)
            {
                gatt.WriteDescriptor(descriptorWriteQueue.ElementAt(0));
            }
            else
            {
                // Start reading the characteristics
                if (devInfoCharacteristics.Count >= 1)
                {
                    gatt.ReadCharacteristic(devInfoCharacteristics.ElementAt(0));
                }
                else
                {
                    //start the scan
                }
            }

        }
        private String valueToString(byte[] value)
        {
            int i = 0;
            for (i = 0; i < value.Count() && value[i] != 0; i++) { }  // find the first null
            return System.Text.Encoding.UTF8.GetString(value, 0, i);
        }
        public override void OnCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, GattStatus status)
        {
            base.OnCharacteristicRead(gatt, characteristic, status);
            _Adap.LogMessage("  OnCharacteristicRead() called ");

            if (status == GattStatus.Success)
            {

                string c_uuid = characteristic.Uuid.ToString().ToUpper();
                if (c_uuid.Equals(BLEServices.kModelNumberCharacteristicUuidString.ToUpper()))
                {
                    byte[] data = characteristic.GetValue();
                    string result = valueToString(data);
                    _Adap.LogMessage("  kModelNumberCharacteristic: " + result);
                    MonitorDevice.Instance.modelNumber = result;
                }
                else if (c_uuid.Equals(BLEServices.kManufacturerNameCharacteristicUuidString.ToUpper()))
                {
                    byte[] data = characteristic.GetValue();
                    string result = valueToString(data);
                    _Adap.LogMessage("  kManufacturerNameCharacteristic: " + result);
                    MonitorDevice.Instance.manufacturer = result;
                }
                else if (c_uuid.Equals(BLEServices.kFirmwareDateCharacteristicUuidString.ToUpper()))
                {
                    byte[] data = characteristic.GetValue();
                    string result = valueToString(data);
                    _Adap.LogMessage("  kFirmwareDateCharacteristic: " + result);
                    MonitorDevice.Instance.firmwareDate = result;
                }
                else if (c_uuid.Equals(BLEServices.kFirmwareVersionCharacteristicUuidString.ToUpper()))
                {
                    byte[] data = characteristic.GetValue();
                    string result = valueToString(data);
                    _Adap.LogMessage("  kFirmwareVersionCharacteristic: " + result);
                    MonitorDevice.Instance.firmwareVersion = result;
                }
                else if (c_uuid.Equals(BLEServices.batteryCharacteristicUuidString.ToUpper()))
                {
                    byte[] data = characteristic.GetValue();
                    BLEHelper.ProcessBatteryData(data);
                }


            }
            // Remove the characteristic just read
            devInfoCharacteristics.RemoveAt(0);
            // Read the next one if there is one
            if (devInfoCharacteristics.Count >= 1)
            {
                gatt.ReadCharacteristic(devInfoCharacteristics.ElementAt(0));
            }
            else
            {
                // Let everyone know we have received device information
                _Adap.DeviceInfoReceivedFromService();

                // Now start the scan

                _Adap.LogMessage("Enabling Notifications");
                if (descriptorWriteQueue.Count >= 1)
                {
                    gatt.WriteDescriptor(descriptorWriteQueue.ElementAt(0));
                }
            }

        }

        public override void OnCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic)
        {
            base.OnCharacteristicChanged(gatt, characteristic);
            if (characteristic == null)
                return;

            string uuid = characteristic.Uuid.ToString().ToUpper();
            if (uuid.Equals(BLEServices.kHxM2CombinedDataCharacteristicUuidString.ToUpper()))
            {
                _Adap.LogMessage("kHxM2CombinedDataCharacteristic updated");
                byte[] data = characteristic.GetValue();
             
				_Adap.DataReceivedFromService (BLEHelper.ProcessCombinedData (data));
                batteryCounter++;

            }
            else if (uuid.Equals(BLEServices.hrCharacteristicUuidString.ToUpper()))
            {
                _Adap.LogMessage("hrCharacteristic updated");

                byte[] data = characteristic.GetValue();
        
                _Adap.DataReceivedFromService(BLEHelper.ProcessHRData(data));
                batteryCounter++;

            }
            else if (uuid.Equals(BLEServices.batteryCharacteristicUuidString.ToUpper()))
            {
                _Adap.LogMessage("batteryCharacteristic updated");
                byte[] data = characteristic.GetValue();
                BLEHelper.ProcessBatteryData(data);

            }
            else if (uuid.Equals(BLEServices.kHxM2ActivityCharacteristicUuidString.ToUpper()))
            {
                _Adap.LogMessage("HxM2ActivityCharacteristic updated");
                byte[] data = characteristic.GetValue();
                BLEHelper.ProcessActivityData(data);

            }
            else if (uuid.Equals(BLEServices.kHxM2ECGCharacteristicUuidString.ToUpper()))
            {
                _Adap.LogMessage("kHxM2ECGCharacteristic updated");
              

            }
            else
            {
                _Adap.LogMessage("Unknown Characteristic " + uuid + " updated");
            }

        }



    }
}