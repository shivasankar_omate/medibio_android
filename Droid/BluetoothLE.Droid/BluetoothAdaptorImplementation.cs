using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Medibio.BluetoothLE;
using Medibio.BluetoothLE.Interfaces;
using Medibio.BluetoothLE.Model;
using Medibio.Modules.Model;
using Android.Bluetooth;
using Android.Bluetooth.LE;
using Android.Preferences;
using Medibio.Droid.BluetoothLE;
using Xamarin.Forms;

[assembly: Dependency(typeof(BluetoothAdapterImplementation))]
namespace Medibio.Droid.BluetoothLE
{

    public class BluetoothAdapterImplementation : ScanCallback, IBluetoothAdapter //, BluetoothAdapter.ILeScanCallback
    {
        protected BluetoothManager _Manager;
        protected BluetoothAdapter _Adapter;
        protected BluetoothGatt _Gatt;

        public string Address
        {
            get
            {
                return _Adapter.Address;
            }
        }

        public IBluetoothDevice ConnectedDevice { get; set; }

        public List<IBluetoothDevice> DiscoveredDeviceList
        {
            get; set;
        }

        private bool _IsConnected;
        public bool IsConnected
        {
            get
            {
                return _IsConnected;
            }
            set
            {
                if (_IsConnected != value)
                {
                    _IsConnected = value;

                }
            }
        }

        private DeviceState _DeviceState;
        public DeviceState DeviceCurrentState
        {
            private set
            {
                if (ConnectedDevice != null && _DeviceState != value)
                {
                    _DeviceState = value;
                    if (this.OnDeviceStateChanged != null)
                    {
                        this.OnDeviceStateChanged.Invoke(this, EventArgs.Empty);
                    }
                }
            }
            get
            {
                return _DeviceState;
            }
        }

        public bool IsDiscoverable
        {
            get
            {
                if (_Adapter.ScanMode == Android.Bluetooth.ScanMode.ConnectableDiscoverable)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string hrCharacteristic
        {
            get
            {
                return BLEServices.hrCharacteristicUuidString;
            }
        }
        public string combinedDataCharacteristic
        {
            get
            {
                return BLEServices.kHxM2CombinedDataCharacteristicUuidString;
            }
        }
        public string activityCharacteristic
        {
            get
            {
                return BLEServices.kHxM2ActivityCharacteristicUuidString;
            }
        }
        public string ecgCharacteristic
        {
            get
            {
                return BLEServices.kHxM2ECGCharacteristicUuidString;
            }
        }
        private string _useCharacteristic = BLEServices.hrCharacteristicUuidString;
        public string useCharacteristic
        {
            get
            {
                _useCharacteristic = GetStringPreference("key_use_characteristic");
                return _useCharacteristic;
            }
            set
            {
                _useCharacteristic = value;
                SaveStringPreference("key_use_characteristic", value);
            }
        }
        public bool IsEnabled
        {
            get
            {
                return _Adapter.IsEnabled;
            }
        }

        public bool IsScanning
        {
            get; set;
        }

        public string Name
        {
            get
            {
                return _Adapter.Name;
            }
        }

        public BluetoothType TargetType
        {
            get; set;
        }
        // Wrapper for saving/fetching from user defaults
        public string Filename
        {
            get
            {
                return GetStringPreference("key_data_filename");
            }
            set
            {
                SaveStringPreference("key_data_filename", value);
            }
        }
        public bool IsRecording
        {
            get
            {
                return GetBooleanPreference("key_is_recording");
            }
            set
            {
                SaveBooleanPreference("key_is_recording", value);
            }
        }

        public event EventHandler<BluetoothDeviceFoundEventArgs> OnDeviceFound;
        public event EventHandler<BLEDataReceivedEventArgs> OnDataReceived;
        public event EventHandler<EventArgs> OnDeviceInfoReceived;
        //public event EventHandler<LEDataReceivedEventArgs> OnActivityDataReceived;
        public event EventHandler<BLEDeviceDisconnectedEventArgs> OnDeviceDisconnected;
        public event EventHandler<BLEDeviceConnectedEventArgs> OnDeviceConnected;
        public event EventHandler<EventArgs> OnDeviceStateChanged;
        public event EventHandler<EventArgs> OnScanFailedX;
        public event EventHandler<EventArgs> OnTimerExpired;

        System.Timers.Timer _timer;
        int timeToWait = 20;
        
        #region Constuctor
        public BluetoothAdapterImplementation()
        {
            OnCreate();
        }

        #endregion

        #region Methods

        private void OnCreate()
        {
            _Manager = (BluetoothManager)Android.App.Application.Context.GetSystemService("bluetooth");
            _Adapter = _Manager.Adapter;
            this.DiscoveredDeviceList = new List<IBluetoothDevice>();
        }
        public async Task StartScanDelayed()
        {
            LogMessage("StartScanDelayed() called");
            _timer = new System.Timers.Timer();
            //Trigger event every second
            _timer.Interval = 10000;
            _timer.Elapsed += OnTimedEvent;

            _timer.Enabled = true;

            return;
        }

        private void OnTimedEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            StartScanAsync(0);
            _timer.Stop();
        }
        public async Task StartScanAsync(int timeoutInMilliSeconds)
        {
            LogMessage("StartScanAsync() called");

            if (!IsScanning)
            {
                IsScanning = true;

                this.DiscoveredDeviceList.Clear();

                IList<ScanFilter> filters = new List<ScanFilter>();
                ScanFilter heartRateService = (new ScanFilter.Builder()).SetServiceUuid(ParcelUuid.FromString(BLEServices.HRService)).Build();
                filters.Add(heartRateService);
                ScanSettings settings = (new ScanSettings.Builder()).SetScanMode(Android.Bluetooth.LE.ScanMode.LowLatency).Build();
                _Adapter.BluetoothLeScanner.StartScan(filters, settings, this);
                LogMessage(" started bluetooth scan");
            }
            return;
        }
        public void LogMessage(string message)
        {
            //string msg = string.Format(@"{0}: {1}", DateTime.Now.ToString("yy-MMM-dd HH:mm:ss.FFF"), message);
            //Console.WriteLine(msg);
        }

        public async Task StopScan()
        {
            LogMessage("StopScan() called");
            if (IsScanning)
            {
                _Adapter.BluetoothLeScanner.StopScan(this);
                IsScanning = false;
            }
            return;
        }

        public async Task ConnectDevice(IBluetoothDevice device)
        {
            BluetoothDevice blDevice = ((BluetoothDeviceImplement)device).Device;
            if (blDevice.BondState == Bond.None)
            {
                // Make sure we have cleared any previous connections
                /*
                if (_Gatt != null)
                {
                    _Gatt.Disconnect();
                    _Gatt.Close();
                }
                */
                _Gatt = blDevice.ConnectGatt(Android.App.Application.Context, true, new GattCB(this));
            }
        }

        public async Task DisconnectDevice(IBluetoothDevice device)
        {
            BluetoothDevice blDevice = (device as BluetoothDeviceImplement).Device;
            if (this.ConnectedDevice == device)
            {
                _Gatt.Disconnect();
                _Gatt.Close();
                this.ConnectedDevice = null;
                this.DiscoveredDeviceList.Clear();
                IsConnected = false;
            }
        }

        #endregion

        public override void OnScanResult([GeneratedEnum] ScanCallbackType callbackType, Android.Bluetooth.LE.ScanResult result)
        {
            LogMessage("OnScanResult() called");

            base.OnScanResult(callbackType, result);

            LogMessage("result.Device.name:" + result.Device.Name);
            LogMessage("result.ScanRecord.DeviceName:" + result.ScanRecord.DeviceName);

            if (result.ScanRecord.DeviceName.ToUpper().Contains(BLEDevices.BioHarness3.ToUpper()) ||
                result.ScanRecord.DeviceName.ToUpper().Contains(BLEDevices.HXM2.ToUpper()))
            {
                LogMessage("Supported Device Found, connecting...");

                //Connect to the first device which was found
                if (this.DiscoveredDeviceList.Count < 1)
                {
                    this.DiscoveredDeviceList.Add(new BluetoothDeviceImplement(result.Device));
                    OnDeviceFound.Invoke(this, new BluetoothDeviceFoundEventArgs(new BluetoothDeviceImplement(result.Device)));
                    this.StopScan();
                }
            }
            LogMessage("Device Bond state:" + result.Device.BondState);
        }
        
        public override void OnScanFailed([GeneratedEnum] ScanFailure errorCode)
        {
            base.OnScanFailed(errorCode);
            LogMessage("Error callback:" + errorCode);
            // this.StopScan();
            this.OnScanFailedX.Invoke(this, EventArgs.Empty);
        }

        //For android call backs
        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        public void DeviceInfoReceivedFromService()
        {
            if (this.OnDeviceInfoReceived != null)
            {
                this.OnDeviceInfoReceived.Invoke(this, EventArgs.Empty);
            }
        }
        public void DataReceivedFromService(HRData data)
        {
            IsConnected = true;
            //<<<For reconnected notification.
            if (DeviceCurrentState == DeviceState.Disconnected)
            {
                DeviceCurrentState = DeviceState.Connected;
            }
            // >>>
            if (this.OnDataReceived != null)
            {
                this.OnDataReceived.Invoke(this, new BLEDataReceivedEventArgs(data));
            }
        }
        /*
		public void ActivityDataReceivedFromService(HRData data) {
			IsConnected = true;
			if (this.OnActivityDataReceived != null)
			{
				this.OnActivityDataReceived.Invoke(this, new LEDataReceivedEventArgs(data));
			}
		}
        */
        public void DeviceReconnected()
        {
            DeviceCurrentState = DeviceState.Connected;
        }
        public void DeviceDisconnected()
        {
            DeviceCurrentState = DeviceState.Disconnected;
            if (OnDeviceDisconnected != null)
            {
                OnDeviceDisconnected.Invoke(this, new BLEDeviceDisconnectedEventArgs(ConnectedDevice));
            }
        }

        public void DeviceConnected()
        {
            //if (!IsScanning)
            //{
                ConnectedDevice = DiscoveredDeviceList[0];//Its COmmented prior.
                DeviceCurrentState = DeviceState.Connected;
                if (OnDeviceConnected != null)
                {
                    OnDeviceConnected.Invoke(this, new BLEDeviceConnectedEventArgs(this.ConnectedDevice));
                }
            //}
            
        }

        public async Task EmailData(String path)
        {
            LogMessage("EmailData() called");
            //Activity1 Activity = new Activity1 ();

            //sendEmail (path);
        }
        //		private void sendEmail(String filePath){
        //			var email   = new Android.Content.Intent (Android.Content.Intent.ActionSendto);
        //			var file    = new Java.IO.File (filePath);
        //			var uri     =  Android.Net.Uri.FromFile (file);
        //
        //			file.SetReadable (true, true);
        //			email.PutExtra (Android.Content.Intent.ExtraEmail, new string[]{"duncan.groenewald@medibio.com.au"} );
        //			email.PutExtra (Android.Content.Intent.ExtraSubject, "Medibio Scan (Android)");
        //			email.PutExtra (Intent.ExtraStream, uri);
        //			email.SetType ("text/plain");
        //			LogMessage ("EmailData() called");
        //			MainApplication.Context.StartActivity(Intent.CreateChooser(email, "Send mail..."));
        //		}
        private void SaveStringPreference(string key, string value)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutString(key, value);
            editor.Apply();
        }
        private void SaveBooleanPreference(string key, Boolean value)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutBoolean(key, value);
            editor.Apply();
        }
        private void SaveIntPreference(string key, int value)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
            ISharedPreferencesEditor editor = prefs.Edit();
            editor.PutInt(key, value);
            editor.Apply();
        }
        private string GetStringPreference(string key)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
            //mBool = prefs.GetBoolean ("key_for_my_bool_value");
            //mInt = prefs.GetInt ("key_for_my_int_value");
            return prefs.GetString(key, "");
        }
        private bool GetBooleanPreference(string key)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
            return prefs.GetBoolean(key, false);
            //mInt = prefs.GetInt ("key_for_my_int_value");
            //return prefs.GetString (key);
        }
        private int GetIntPreference(string key)
        {
            ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
            return prefs.GetInt(key, 0);
        }
        public async Task StartTimer()
        {

            LogMessage("StartScanDelayed() called");
            System.Timers.Timer timer = new System.Timers.Timer();
            //Trigger event every second
            timer.Interval = timeToWait * 2000;
            timer.Elapsed += TimerExpired;

            timer.Enabled = true;

            return;
        }

        private void TimerExpired(object sender, System.Timers.ElapsedEventArgs e)
        {
            System.Timers.Timer timer = sender as System.Timers.Timer;
            OnTimerExpired.Invoke(this, EventArgs.Empty);
            timer.Stop();
        }
    }



}