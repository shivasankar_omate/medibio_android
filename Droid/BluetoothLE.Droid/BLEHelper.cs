using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Medibio.Modules.Model;

namespace Medibio.Droid.BluetoothLE
{
    public static class BLEHelper
    {
        //public static HRData DataPacket { get; } = new HRData();
        /// <summary>
        /// Process the raw data received from the device into application usable data, 
        /// according the the Bluetooth Heart Rate Profile.
        /// </summary>
        /// <param name="data">Raw data received from the heart rate monitor.</param>
        /// <returns>The heart rate measurement value.</returns>
        public static HRData ProcessHRData(byte[] data)
        {
            // Heart Rate profile defined flag values
            const byte HEART_RATE_VALUE_FORMAT = 0x01;
            const byte ENERGY_EXPANDED_STATUS = 0x08;
            const byte WORN_INDICATOR = 0x06;
            const byte RR_PRESENT = 0x10;

            byte currentOffset = 0;
            byte flags = data[currentOffset];
            bool isHeartRateValueSizeLong = ((flags & HEART_RATE_VALUE_FORMAT) != 0);
            bool hasEnergyExpended = ((flags & ENERGY_EXPANDED_STATUS) != 0);
            bool isRRPresent = ((flags & RR_PRESENT) != 0);

            ushort contactBits = (ushort)((flags & WORN_INDICATOR) >> 1);

			HRData hrData = new HRData ();


			hrData.Contact = contactBits;
            //BLEHelper.LogMessage(" Contact: " + BLEHelper.DataPacket.Contact);


            currentOffset++;

            ushort heartRateMeasurementValue = 0;

            if (isHeartRateValueSizeLong)
            {
                heartRateMeasurementValue = (ushort)((data[currentOffset + 1] << 8) + data[currentOffset]);
                currentOffset += 2;
            }
            else
            {
                heartRateMeasurementValue = data[currentOffset];
                currentOffset++;
            }
			hrData.Bpm = heartRateMeasurementValue;
            //BLEHelper.LogMessage(" bpm: " + BLEHelper.DataPacket.Bpm);

            ushort expendedEnergyValue = 0;

            if (hasEnergyExpended)
            {
                expendedEnergyValue = (ushort)((data[currentOffset + 1] << 8) + data[currentOffset]);
                currentOffset += 2;
            }
				
            ScanSession scanSession = ScanSession.Instance;

            if (isRRPresent)
            {
				// read from index to the end of data
                while (currentOffset < (data.Length - 1))
                {
                    ushort val = (ushort)((data[currentOffset + 1] << 8) + data[currentOffset]);
                    currentOffset += 2;

                    hrData.RrIntervals.Add(val);

                }

            }

            // The Heart Rate Bluetooth profile can also contain sensor Contact status information,
            // and R-Wave interval measurements, which can also be processed here. 
            // For the purpose of this sample, we don't need to interpret that data.

			return hrData;

        }

        public static void ProcessBatteryData(byte[] data)
        {

            sbyte percent = (sbyte)data[0];

			MonitorDevice.Instance.battery = (int)percent;

            
        }

        public static HRData ProcessCombinedData(byte[] bytes)
        {


            // The packet structure we use is based on version 1. Abort if it's any
            // other
            uint version = (uint)(bytes[0] & 0x0f);
            if (version != 0x01)
            {
                return null;
            }
            HRData hrData = new HRData ();

            sbyte contactBit = (sbyte)(bytes[0] >> 7);

            if (contactBit == 1)
            {
				hrData.Contact = 3;
            }
            else
            {
				hrData.Contact = 0;
            }
            //BLEHelper.LogMessage(" Contact: " + BLEHelper.DataPacket.Contact);

            // Byte 4 is heart rate. But we will continue to read it from 0x180D.
            // This will ensure compatibility with other devices.
            sbyte hr = (sbyte)bytes[4];
            //BLEHelper.LogMessage(" hr: " + hr);
			hrData.Bpm = (int)hr;

            // Activity is byte 5 and 4 LSB bits from byte 6
            Int16 activityUnscaled = (Int16)(((bytes[6] & 0x0f) << 8) | bytes[5]);
            double activity = activityUnscaled * 0.01;
			hrData.Activity = activity;

            // Posture is 4 HSB bits in byte 6 and all of byte 7
            Int16 posture = (Int16)((bytes[6] >> 4) | (bytes[7] << 4));
            // get Posture in 1 degree units
            posture = (Int16)((posture << 20) >> 20);
            // Sign-extend from 12-bit to 32-bit
			hrData.Posture = posture;

            int count = bytes.Count();

            ScanSession scanSession = ScanSession.Instance;

            // Decode up to 4 RR values. Values are stored 10-bit packed in 4ms resolution, e.g. 250 = 1000ms
            if (count > 9)
            {
                int val = (int)(((bytes[8] >> 0) | ((bytes[9] & 0x3) << 8)) * 4);
                //BLEHelper.LogMessage(" ibi: " + val);
				hrData.RrIntervals.Add(val);
                //scanSession.newRRInterval(val);
            }
            if (count > 10)
            {
                int val = (int)(((bytes[9] >> 2) | ((bytes[10] & 0xf) << 6)) * 4);
                //BLEHelper.LogMessage(" ibi: " + val);
				hrData.RrIntervals.Add(val);
                //scanSession.newRRInterval(val);
            }
            if (count > 11)
            {
                int val = (int)(((bytes[10] >> 4) | ((bytes[11] & 0x3f) << 4)) * 4);
                //BLEHelper.LogMessage(" ibi: " + val);
				hrData.RrIntervals.Add(val);
                //scanSession.newRRInterval(val);
            }
            if (count > 12)
            {
                int val = (int)(((bytes[11] >> 6) | (bytes[12] << 2)) * 4);
                //BLEHelper.LogMessage(" ibi: " + val);
				hrData.RrIntervals.Add(val);
                //scanSession.newRRInterval(val);
            }

            // Save data into monitor
            //LogMessage (" return");
			return hrData;

        }
        public static void ProcessActivityData(byte[] data)
        {

            ushort activityUnscaled = (ushort)((data[2] << 8) + data[1]);
            ushort peakActivity = (ushort)((data[4] << 8) + data[3]);

			MonitorDevice.Instance.activity = activityUnscaled * 0.01;
			MonitorDevice.Instance.peakActivity = peakActivity * 0.01;

            return ;

        }

        public static void LogMessage(string message)
        {
			string msg = string.Format(@"{0}: {1}", DateTime.Now.ToString(DateFormats.TimestampFormat), message);
            Console.WriteLine(msg);
        }
    }
}