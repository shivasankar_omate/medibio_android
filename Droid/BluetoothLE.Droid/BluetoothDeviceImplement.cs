using System;
using Android.Bluetooth;
using Medibio.BluetoothLE.Interfaces;
using Medibio.BluetoothLE.Model;
using Medibio.BluetoothLE;
using Android.App;
using System.Diagnostics;

namespace Medibio.Droid.BluetoothLE
{
	class BluetoothDeviceImplement : BluetoothGattCallback, IBluetoothDevice
	{
		public BluetoothDevice Device { private set; get; }

		public BluetoothDeviceImplement(BluetoothDevice device)
		{
			Device = device;
			if (Device != null)
			{
				//TODO:
			}
		}

		string IBluetoothDevice.Address
		{
			get
			{
				return this.Device.Address;
			}
		}

		string IBluetoothDevice.Name
		{
			get
			{
				return this.Device.Name;
			}
		}

		public event EventHandler<DeviceConnectedEventArgs> OnConnected;

		public event EventHandler<DeviceConnectedEventArgs> OnLost;

		DeviceState IBluetoothDevice.State
		{
			get
			{
				if (Device.BondState == Bond.Bonded)
				{
					return DeviceState.Connected;
				}
				else if (Device.BondState == Bond.Bonding)
				{
					return DeviceState.Connecting;
				}
				else {
					return DeviceState.Disconnected;
				}
			}
		}

		public DeviceState State
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		bool IBluetoothDevice.CreateSocket(Action action)
		{
			// todo:- check this
			Debugger.Break();
			//			BluetoothGatt bGatt = Device.ConnectGatt (Application.Context, true, this, BluetoothTransports.Le);
			//			bool con = bGatt.Connect ();

			//			Device.SetPin (Encoding.ASCII.GetBytes("1234"));
			//			Device.SetPairingConfirmation (true);
			//			Console.WriteLine (Device.BondState);
			//			var res = Device.FetchUuidsWithSdp ();
			//			ParcelUuid[] services = this.Device.GetUuids ();
			//			for (int i = 0; i < services.Count (); i++) {
			//				Console.WriteLine ("Service:"+services[i] );
			//			}
			//			BluetoothGatt bGatt = Device.ConnectGatt (Android.App.Application.Context, true, this);
			//			bool con = bGatt.Connect ();
			//			if (con) {
			//				foreach (BluetoothGattService service in bGatt.Services) {
			//					Console.Write ("Service::" + service.Uuid.ToString () + " Type::" + service.Type.ToString ());
			//				}
			//			}
			//
			//			return con;

			return false;
		}

		bool IBluetoothDevice.ReadData(Action action)
		{
			throw new NotImplementedException();
		}

		public bool Connect()
		{
			if (Device != null)
			{
				return Device.CreateBond();
			}
			else {
				return false;
			}
		}

		public override void OnCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic)
		{
			base.OnCharacteristicChanged(gatt, characteristic);
			Console.WriteLine("OnCharacteristicChanged" + characteristic.GetStringValue(2));
		}

		public override void OnCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, GattStatus status)
		{
			base.OnCharacteristicRead(gatt, characteristic, status);
			Console.WriteLine("OnCharacteristicRead" + status.ToString());
		}

		public override void OnDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, GattStatus status)
		{
			base.OnDescriptorRead(gatt, descriptor, status);
			Console.WriteLine("OnDescriptorRead" + descriptor.Uuid.ToString());
		}

		public override void OnReadRemoteRssi(BluetoothGatt gatt, int rssi, GattStatus status)
		{
			base.OnReadRemoteRssi(gatt, rssi, status);
			Console.WriteLine("OnReadRemoteRssi" + rssi + status.ToString());
		}


	}



}