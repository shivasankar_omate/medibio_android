﻿using System;
using Android.Bluetooth;
using Android.Bluetooth.LE;

namespace Medibio.Droid.BluetoothLE
{
	public class GattStatusCodes
	{
		
			public GattStatusCodes ()
			{
			}
			public static string getDescription(GattStatus status){

				switch(status) {

				case GattStatus.ConnectionCongested:
					return "GattStatus.ConnectionCongested";
				case GattStatus.Failure:
					return "GattStatus.Failure";
				case GattStatus.InsufficientAuthentication:
					return "GattStatus.InsufficientAuthentication";
				case GattStatus.InsufficientEncryption:
					return "GattStatus.InsufficientEncryption";
				case GattStatus.InvalidAttributeLength:
					return "GattStatus.InvalidAttributeLength";
				case GattStatus.InvalidOffset:
					return "GattStatus.InvalidOffset";
				case GattStatus.ReadNotPermitted:
					return "GattStatus.ReadNotPermitted";
				case GattStatus.RequestNotSupported:
					return "GattStatus.RequestNotSupported";
				case GattStatus.Success:
					return "GattStatus.Success";
				case GattStatus.WriteNotPermitted:
					return "GattStatus.WriteNotPermitted";

				default:
					return "Unknown";
				}
			}
	
	}
}

